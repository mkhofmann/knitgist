from unittest import TestCase

from KnitGIST.KnitGraph_Construction.DoubleKnitGraph import DoubleKnitGraph
from KnitGIST.KnitGraph_Construction.common_knitgraphs import *
from KnitGIST.KnitOutGenerator.InstructionGraph import KnitOutInstructionGraph
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import Needle
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_instructions import KnitInstruction
from KnitGIST.KnitOutGenerator.graph_to_knitout import Bed
from KnitGIST.KnitGraph_Construction.Iterator import Iterator
from KnitGIST.KnitGraph_Construction.BitmapParser import BitmapParser


class TestKnitOutInstructionGraph(TestCase):
    def test_top_sort_instructions(self):
        w = 50
        h = 50
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"doubleStSt_{w}x{h}.k")

        knitGraph = buildRib1(w, h)
        double = DoubleKnitGraph(knitGraph)
        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"doubleRib1_{w}x{h}.k")

    def test_withColor(self):
        w = 5
        h = 5
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        for rowIndex in double.rows:
            if rowIndex > 0 and rowIndex % 2 == 1:
                row = knitGraph.rows[rowIndex]
                firstLoop = row[0]
                lastLoop = row[-1]
                double.swapStitchColor(firstLoop.loopId)
                double.swapStitchColor(lastLoop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"StSt_ColorSwap_{w}x{h}.k")

    def test_vert(self):
        w = 25
        h = 25
        s = 5
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        for rowIndex in double.rows:
            if rowIndex >= 0:
                row = knitGraph.rows[rowIndex]
                for index, loop in enumerate(row):
                    if rowIndex % 2 == 1:
                        col = index+1
                    else:
                        col = len(row) - index
                    stripe = int(col / s)
                    if stripe % 2 == 0:  # first stripe swaps color
                        double.swapStitchColor(loop.loopId)
        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"vertStrip_{w}x{h}.k")

    def test_colorStripes(self):
        w = 50
        h = 50
        s = w / 50
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        for rowIndex in knitGraph.rows:

            if rowIndex > 0 and rowIndex % 2 == 1:
                row = knitGraph.rows[rowIndex]
                firstLoop = row[0]
                lastLoop = row[-1]
                double.swapStitchColor(firstLoop.loopId)
                double.swapStitchColor(lastLoop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"vertStripe_{w}x{h}.k")

    def test_withColor_stst_checkerboard(self):
        w = 25
        h = 25
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        for rowIndex in knitGraph.rows:
            if rowIndex >= 0:
                row = knitGraph.rows[rowIndex]

                for i, loop in enumerate(row):
                    colIndex = -1
                    if rowIndex % 2 == 0:
                        colIndex = i
                    else:
                        colIndex = len(row) - i - 1

                    # Calculate which tile in the checkerboard we are in
                    rowIndexGroup = int(rowIndex / 5)
                    colIndexGroup = int(colIndex / 5)
                    if ((rowIndexGroup + colIndexGroup) % 2 == 0):
                        double.swapStitchColor(loop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Checker{w}x{h}.k")

    def test_withColor_rib1_checkerboard(self):
        w = 25
        h = 25
        knitGraph = buildRib1(w, h)
        double = DoubleKnitGraph(knitGraph)
        for rowIndex in knitGraph.rows:
            if rowIndex >= 0:
                row = knitGraph.rows[rowIndex]

                for i, loop in enumerate(row):
                    colIndex = -1
                    if rowIndex % 2 == 0:
                        colIndex = i
                    else:
                        colIndex = len(row) - i - 1

                    # Calculate which tile in the checkerboard we are in
                    rowIndexGroup = int(rowIndex / 5)
                    colIndexGroup = int(colIndex / 5)
                    if ((rowIndexGroup + colIndexGroup) % 2 == 0):
                        double.swapStitchColor(loop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Checker{w}x{h}.k")

    def test_withColor_stst_checkerboard_2d_arr(self):
        w = 25
        h = 25
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        itr = Iterator()
        row_arr = itr.reverse_2D_arr_dict_individual_rows(knitGraph.rows)
        #print("knitGraph.rows len: ", len(knitGraph.rows))
        for rowIndex in row_arr:
            #print("rowIndex: ", rowIndex)
            if rowIndex >= 0:
                row = row_arr[rowIndex]
                for colIndex, loop in enumerate(row):
                    # Calculate which tile in the checkerboard we are in
                    rowIndexGroup = int(rowIndex / 5)
                    colIndexGroup = int(colIndex / 5)
                    if ((rowIndexGroup + colIndexGroup) % 2 == 0):
                        double.swapStitchColor(loop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Checker{w}x{h}.k")

    def test_withColor_rib1_checkerboard_2d_arr(self):
        w = 25
        h = 25
        knitGraph = buildRib1(w, h)
        double = DoubleKnitGraph(knitGraph)
        itr = Iterator()
        row_arr = itr.reverse_2D_arr_dict_individual_rows(knitGraph.rows)
        for rowIndex in row_arr:
            if rowIndex >= 0:
                row = row_arr[rowIndex]
                for colIndex, loop in enumerate(row):
                    # Calculate which tile in the checkerboard we are in
                    rowIndexGroup = int(rowIndex / 5)
                    colIndexGroup = int(colIndex / 5)
                    if ((rowIndexGroup + colIndexGroup) % 2 == 0):
                        double.swapStitchColor(loop.loopId)

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Checker{w}x{h}.k")

    def test_withColor_G_letter(self):
        w = 25
        h = 25
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        itr = Iterator()
        row_arr = itr.reverse_2D_arr_dict_individual_rows(knitGraph.rows)

        # Letter G

        # from [3][1] to [3][6]
        # from [4][1] to [4][6]
        for i in range(3,5):
            for j in range(1,7):
                double.swapStitchColor(row_arr[i][j].loopId)

        # from [5][1] and [5][2]
        # from [6][1] and [6][2]
        # ...
        # from [14][1] and [14][2]
        for i in range(5,15):
            for j in range(1,3):
                double.swapStitchColor(row_arr[i][j].loopId)

        # from [15][1] to [15][6] (excluding [15][3])
        # from [16][1] to [16][6] (excluding [16][3])
        for i in range(15, 17):
            for j in range(1, 7):
                if (j != 3):
                    double.swapStitchColor(row_arr[i][j].loopId)

        # from [17][1] to [17][6] (excluding [17][3] and [17][4])
        # from [18][1] to [18][6] (excluding [18][3] and [18][4])
        # from [19][1] to [19][6] (excluding [19][3] and [19][4])
        for i in range(17, 20):
            for j in range(1, 7):
                if (j != 3 and j != 4):
                    double.swapStitchColor(row_arr[i][j].loopId)

        # from [20][1] to [20][6]
        # from [21][1] to [21][6]
        for i in range(20, 22):
            for j in range(1, 7):
                double.swapStitchColor(row_arr[i][j].loopId)

        # Letter I


        # Letter S


        # Letter T


        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"G_Letter{w}x{h}.k")

    def test_bindOff(self):
        w = 10
        h = 10
        knitGraph = buildStSt(w, h)
        double = DoubleKnitGraph(knitGraph)
        graph = KnitOutInstructionGraph(double, True)
        graph.writeInstructions(f"boStSt_{w}x{h}.k")

    def test_sortKnitInstructions(self):
        instructions = [KnitInstruction("-", Needle(needle, Bed.Front), 3) for needle in range(1, 5)]
        print(sorted(instructions))
        instructions = [KnitInstruction("+", Needle(needle, Bed.Front), 3) for needle in range(5, 0, -1)]
        print(sorted(instructions))

    def test_bitmapSwapColors_oddHeight_StSt(self):
        bmp = BitmapParser()
        double = bmp.bitmapSwapColors('bitmap1.txt', ststPattern())

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Bitmap_swap_oddStSt.k")

    def test_bitmapSwapColors_evenHeight_Rib(self):
        bmp = BitmapParser()
        double = bmp.bitmapSwapColors('bitmap2.txt', ribPattern())

        graph = KnitOutInstructionGraph(double)
        graph.writeInstructions(f"Bitmap_swap_evenRib.k")

