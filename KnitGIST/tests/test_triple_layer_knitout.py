from typing import Tuple, List
from unittest import TestCase

from KnitGIST.ColorWork.LayerPosition import allOneColorLayer, Layer_Position, Layer_Coloring
from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitGraph_Construction.common_knitgraphs import *
from KnitGIST.KnitOutGenerator.triple_layer_knitout import Layer_KnitOut_Generator


def writeLayerFilesHorizontalSeam(width: int, height: int, rowIndex: int, file_postFix: str) -> Tuple[str, str, str]:
    posOrder: List[Layer_Position] = [Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot]

    def nextPos(index, increment) -> Layer_Position:
        return posOrder[(index + increment) % len(posOrder)]

    fileNames = []

    for i, pos in enumerate(posOrder):
        filename = f"{pos}_{file_postFix}"
        with open(filename, "w") as layer:
            lines = []
            for row in range(0, height):
                line = ""
                if row == rowIndex:
                    order = [pos, nextPos(i, 1), nextPos(i, 2)]
                    for col in range(0, width):
                        line += f"{order[col % len(posOrder)]} "
                else:
                    for _ in range(0, width):
                        line += f"{pos} "
                line += "\n"
                lines.append(line)
            layer.writelines(lines)
            fileNames.append(filename)
    assert len(fileNames) == 3
    return fileNames[0], fileNames[1], fileNames[2]


def writeBraidStripes(stripes: int, stripe_size: int, height: int, file_postFix: str) -> Tuple[str, str, str]:
    posOrder: List[Layer_Position] = [Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot]

    def nextPos(index, increment) -> Layer_Position:
        return posOrder[(index + increment) % len(posOrder)]

    width = stripes * stripe_size

    fileNames = []
    for i, pos in enumerate(posOrder):
        filename = f"{pos}_{file_postFix}"
        with open(filename, "w") as layer:
            lines = []
            for row in range(0, height):
                line = ""
                for c in range(0, width):
                    curStripe = int(c / stripe_size)
                    posIndex = curStripe % len(posOrder)
                    pos = nextPos(posIndex, i)
                    line += f"{pos} "
                line += "\n"
                lines.append(line)
            layer.writelines(lines)
            fileNames.append(filename)

    assert len(fileNames) == 3
    return fileNames[0], fileNames[1], fileNames[2]


def plainStst(width=2, height=2) -> LayeredKnit:
    stst0 = buildStSt(width, height)
    stst1 = buildStSt(width, height)
    stst2 = buildStSt(width, height)
    topLayer = allOneColorLayer(Layer_Position.Top, width, height + 1, "top.lyr")
    midLayer = allOneColorLayer(Layer_Position.Mid, width, height + 1, "mid.lyr")
    botLayer = allOneColorLayer(Layer_Position.Bot, width, height + 1, "bot.lyr")
    layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, midLayer, botLayer])
    return layeredKnit


class TestLayer_KnitOut_Generator(TestCase):

    def test__cast_on_bound(self):
        # layeredKnit = self.plainStst(2, 2)
        layeredKnit = plainStst(10, 10)
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("bound-CO.k")

    def test__rib1_rStst(self):
        width = 10
        height = 10
        stst0 = buildStSt(width, height)
        stst1 = buildRib1(width, height)
        stst2 = buildReverseStSt(width, height)
        topLayer = allOneColorLayer(Layer_Position.Top, width, height + 1, "top.lyr")
        midLayer = allOneColorLayer(Layer_Position.Mid, width, height + 1, "mid.lyr")
        botLayer = allOneColorLayer(Layer_Position.Bot, width, height + 1, "bot.lyr")
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, midLayer, botLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("layered_textures.k")

    def test_braidTextures(self):
        stripe_size = 4
        stripes = 4
        width = stripe_size * stripes
        height = 10
        stst0 = buildStSt(width, height)
        stst1 = buildRib1(width, height)
        stst2 = buildReverseStSt(width, height)
        topName, midName, botName = writeBraidStripes(stripes, stripe_size, height + 1, f"braid.lyr")
        top = Layer_Coloring(width, height + 1, topName)
        mid = Layer_Coloring(width, height + 1, midName)
        bot = Layer_Coloring(width, height + 1, botName)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [top, mid, bot])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=False, ststBaseLayers=4, ststTopLayers=4, bindOff=False)
        generator.writeInstructions("braid-texture.k")

    def test_pocketTextures(self):
        border_size = 4
        pocket_size = 10
        width = border_size + pocket_size + border_size
        height = 10
        stst0 = buildStSt(width, height)
        stst1 = buildRib1(width, height)
        stst2 = buildReverseStSt(width, height)
        topName, midName, botName = self.writeFrontPocket(border_size, pocket_size, height + 1)
        top = Layer_Coloring(width, height + 1, topName)
        mid = Layer_Coloring(width, height + 1, midName)
        bot = Layer_Coloring(width, height + 1, botName)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [top, mid, bot])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0, ststTopLayers=4, bindOff=False)
        generator.writeInstructions("pocket-texture.k")

    def test__cast_on_unbound(self):
        width = 10
        height = 10
        stst0 = buildStSt(width, height)
        stst1 = buildStSt(width, height)
        stst2 = buildStSt(width, height)
        seam = height - 1
        topName, midName, botName = writeLayerFilesHorizontalSeam(width, height + 1, seam, f"seam.lyr")
        top = Layer_Coloring(width, height + 1, topName)
        mid = Layer_Coloring(width, height + 1, midName)
        bot = Layer_Coloring(width, height + 1, botName)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [top, mid, bot])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=False, ststBaseLayers=4, ststTopLayers=4, bindOff=False)
        generator.writeInstructions("unbound-CO-midSeam.k")

    def test__bind_off_unbound(self):
        layeredKnit = plainStst(10, 10)
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, bindOff=True)
        generator.writeInstructions("BoundOff.k")

    def test_braidStripes(self):
        stripe_size = 1
        stripes = 3
        width = stripe_size * stripes
        height = 2
        stst0 = buildStSt(width, height)
        stst1 = buildStSt(width, height)
        stst2 = buildStSt(width, height)
        topName, midName, botName = writeBraidStripes(stripes, stripe_size, height + 1, f"braid.lyr")
        top = Layer_Coloring(width, height + 1, topName)
        mid = Layer_Coloring(width, height + 1, midName)
        bot = Layer_Coloring(width, height + 1, botName)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [top, mid, bot])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0, ststTopLayers=0, bindOff=False)
        generator.writeInstructions("braid.k")

    @staticmethod
    def writeFrontPocket(border_size: int,
                         pocket_size: int,
                         height: int,
                         postFix: str = "pocket.lyr") -> Tuple[str, str, str]:

        top_fileName = f"top_{postFix}"
        mid_fileName = f"mid_{postFix}"
        bot_fileName = f"bot_{postFix}"

        with open(top_fileName, "w") as top:
            with open(mid_fileName, "w") as mid:
                with open(bot_fileName, "w") as bot:
                    top_lines = []
                    mid_lines = []
                    bot_lines = []
                    for row in range(0, height):
                        top_line = ""
                        mid_line = ""
                        bot_line = ""
                        for _ in range(0, border_size):
                            top_line += f"{Layer_Position.Mid} "
                            mid_line += f"{Layer_Position.Top} "
                            bot_line += f"{Layer_Position.Bot} "
                        for _ in range(0, pocket_size):
                            top_line += f"{Layer_Position.Top} "
                            mid_line += f"{Layer_Position.Mid} "
                            bot_line += f"{Layer_Position.Bot} "
                        for _ in range(0, border_size):
                            top_line += f"{Layer_Position.Mid} "
                            mid_line += f"{Layer_Position.Top} "
                            bot_line += f"{Layer_Position.Bot} "
                        top_line += "\n"
                        mid_line += "\n"
                        bot_line += "\n"
                        top_lines.append(top_line)
                        mid_lines.append(mid_line)
                        bot_lines.append(bot_line)
                    top.writelines(top_lines)
                    mid.writelines(mid_lines)
                    bot.writelines(bot_lines)
        return top_fileName, mid_fileName, bot_fileName

    def test_frontPocket(self):
        border_size = 4
        pocket_size = 4
        width = border_size + pocket_size + border_size
        height = 10
        stst0 = buildStSt(width, height)
        stst1 = buildStSt(width, height)
        stst2 = buildStSt(width, height)
        topName, midName, botName = self.writeFrontPocket(border_size, pocket_size, height + 1)
        top = Layer_Coloring(width, height + 1, topName)
        mid = Layer_Coloring(width, height + 1, midName)
        bot = Layer_Coloring(width, height + 1, botName)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [top, mid, bot])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0, ststTopLayers=4, bindOff=False)
        generator.writeInstructions("pocket.k")

    def test_stretchy_bound_bind_off(self):
        layeredKnit = plainStst(10, 10)
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=False, bindOff=True, boundBindOff=True)
        generator.writeInstructions("Stretch_BoundOff.k")


