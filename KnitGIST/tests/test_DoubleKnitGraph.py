from unittest import TestCase

from KnitGIST.KnitGraph_Construction.DoubleKnitGraph import DoubleKnitGraph
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildStSt


class TestDoubleKnitGraph(TestCase):
    def test_generate_double_graph(self):
        knitGraph = buildStSt(2, 2)
        double = DoubleKnitGraph(knitGraph)
        pass
