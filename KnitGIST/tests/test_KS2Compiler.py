from unittest import TestCase

from KnitGIST2.KnitGraph_Construction.KnitGraphPlotter import plotKnitGraph
from KnitGIST2.KnitGraph_Construction.ks_compiler.KSCompiler import KSCompiler


class TestKS2Compiler(TestCase):
    def test_build_graph(self):
        compiler = KSCompiler(r'''1st and 3rd rows k 3, [k,p] to last st, p.
                                    2nd row p.''')
        knitGraph = buildGraphFromInstructions(width=(20, 10), height=4)
        plotKnitGraph(knitGraph, "Ks2_test.png")

    def test_build_lace(self):
        compiler = KSCompiler(r'''1st row k, [yo, sk2po] to last st, k.
                                    from ws 2nd to 10th rows [p] to end.
                                    3rd row k, [yo, sk2po] to end.
                                    5th row k, [yo, skpo] to end.
                                    7th row k, [skpo] to end.
                                    9th row [skpo] to end.
                                    ''')
        try:
            knitGraph = buildGraphFromInstructions(width=(8, 8))
            assert False
        except:
            pass

    def test_draw_pattern(self):
        compiler = KSCompiler(r'''1st row k,p, [k] to last 3 sts, k,p 2. 
                                    2nd row k 2, p, [p] to last 2 sts, k, p.''')
        grid = compiler.stitchGrid(8, 6)
        print(grid)

    def test_balance_loop_counts_in_block(self):
        pattern = "1st row skpo, s2kpo, k2tog."
        compiler = KSCompiler(pattern, balance=True)
        print(compiler._courseIdsToInstructions)
        pattern = "1st row yo, [yo, yo] 2, yo 2."
        compiler = KSCompiler(pattern, balance=True)
        print(compiler._courseIdsToInstructions)
