from unittest import TestCase

from KnitGIST.ColorWork.LayerPosition import Layer_Coloring
from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildStSt


class TestLayer_Position(TestCase):
    def test_compatible(self):
        layer_0 = Layer_Coloring(3, 3, "test_layer0.lyr")
        layer_1 = Layer_Coloring(3, 3, "test_layer1.lyr")
        layer_2 = Layer_Coloring(3, 3, "test_layer2.lyr")
        assert layer_0.checkCompatibility(layer_1, layer_2)

    def test_Layer(self):
        coloring = Layer_Coloring(3, 3, "test_layer0.lyr")

    def test_implied_layer(self):
        layer_0 = Layer_Coloring(3, 3, "test_layer0.lyr")
        layer_1 = Layer_Coloring(3, 3, "test_layer1.lyr")
        layer_2 = Layer_Coloring(3, 3, "test_layer2.lyr")
        impliedLayer = layer_0.impliedLayer(layer_1)
        assert impliedLayer == layer_2
