from unittest import TestCase

from KnitGIST.ColorWork.LayerPosition import allOneColorLayer, Layer_Position
from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildStSt
from KnitGIST.KnitOutGenerator.triple_layer_knitout import Layer_KnitOut_Generator


class Test_TripleSTST(TestCase):

    def test_012(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, midLayer, botLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_012.k")

    def test__120(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [midLayer, botLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_120.k")

    def test__201(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, topLayer, midLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_201.k")

    def test__102(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [midLayer, topLayer, botLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_102.k")

    def test__021(self):
        width = 2
        height = 2
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, botLayer, midLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_021.k")

    def test__210(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("stst_210.k")


def getLayers(height, width):
    topLayer = allOneColorLayer(Layer_Position.Top, width, height + 1, "top.lyr")
    midLayer = allOneColorLayer(Layer_Position.Mid, width, height + 1, "mid.lyr")
    botLayer = allOneColorLayer(Layer_Position.Bot, width, height + 1, "bot.lyr")
    return botLayer, midLayer, topLayer


def getTextures(height, width, textureFunc=buildStSt):
    texture0 = textureFunc(width, height)
    texture1 = textureFunc(width, height)
    texture2 = textureFunc(width, height)
    return texture0, texture1, texture2