from unittest import TestCase

from KnitGraph_Construction.KnitGraphPlotter import plotKnitGraph
from KnitGraph_Construction.common_knitgraphs import buildStSt


class Test(TestCase):

    def test_build_st_st(self):
        knitGraph = buildStSt(5, 5)
        print("stst")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature > 0
        assert horizontalCompression == 0
        assert verticalCompression == 0
        assert wideStretch == 0
        assert verticalStretch == 0
        plotKnitGraph(knitGraph, "stst4x4.png")

    def test_build_rib1(self):
        knitGraph = buildRib1(4, 4)
        print("rib1")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature == 0
        assert horizontalCompression > 0
        assert verticalCompression == 0
        assert wideStretch > 0
        assert verticalStretch == 0
        plotKnitGraph(knitGraph, "rib1_4x4.png")

    def test_build_rib2(self):
        knitGraph = buildRib2(4, 4)
        print("rib2")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature == 0
        assert horizontalCompression > 0
        assert verticalCompression == 0
        assert wideStretch > 0
        assert verticalStretch == 0
        plotKnitGraph(knitGraph, "rib2_4x4.png")

    def test_build_seed(self):
        knitGraph = buildSeed(4, 4)
        print("seed")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        plotKnitGraph(knitGraph, "seed4x4.png")
        assert curvature == 0
        assert horizontalCompression > 0
        assert verticalCompression > 0
        assert wideStretch < .5
        assert verticalStretch < .5

    def test_build_moss(self):
        knitGraph = buildMoss(4, 4)
        print("moss")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature == 0
        assert horizontalCompression > 0
        assert verticalCompression > 0
        assert wideStretch < .5
        assert verticalStretch < .5

    def test_build_reverse_st_st(self):
        knitGraph = buildReverseStSt(4, 4)
        print("reverseStSt")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature < 0
        assert horizontalCompression == 0
        assert verticalCompression == 0
        assert wideStretch == 0
        assert verticalStretch == 0
        plotKnitGraph(knitGraph, "rStSt4x4.png")

    def test_build_garter(self):
        knitGraph = buildGarter(4, 4)
        print("grt")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature == 0
        assert horizontalCompression == 0
        assert verticalCompression > 0
        assert wideStretch == 0
        assert verticalStretch > 0
        plotKnitGraph(knitGraph, "garter4x4.png")

    def test_build_welt2(self):
        knitGraph = buildWelt2(4, 4)
        print("welt2")
        curvature = knitGraph.getBaseCurvature()
        print("curvature == {}".format(curvature))
        horizontalCompression = knitGraph.horizontalCompression()
        print("wide_comp == {}".format(horizontalCompression))
        verticalCompression = knitGraph.waleWiseCompression()
        print("vert_comp == {}".format(verticalCompression))
        wideStretch = knitGraph.widthStretch()
        print("wide_stretch == {}".format(wideStretch))
        verticalStretch = knitGraph.heightStretch()
        print("vert_stretch == {}".format(verticalStretch))
        assert curvature == 0
        assert horizontalCompression == 0
        assert verticalCompression > 0
        assert wideStretch == 0
        assert verticalStretch > 0
        plotKnitGraph(knitGraph, "welt2_4x4.png")

    def test_build_basic_lace(self):
        knitGraph = buildBasicLace(8, 12)
        plotKnitGraph(knitGraph, "lace_6x6.png")
        assert knitGraph.getBaseCurvature() > 0
        assert knitGraph.horizontalCompression() == 0
        assert knitGraph.waleWiseCompression() == 0
        assert knitGraph.widthStretch() == 0
        assert knitGraph.heightStretch() == 0

    def test_build_basic_cable(self):
        knitGraph = buildBasicCable(5, 6)
        plotKnitGraph(knitGraph, "cable_5x6.png")
        assert knitGraph.getBaseCurvature() > 0
        compression = knitGraph.horizontalCompression()
        assert compression > 0
        assert knitGraph.waleWiseCompression() > 0
        assert knitGraph.widthStretch() == 0
        assert knitGraph.heightStretch() < 0.5
