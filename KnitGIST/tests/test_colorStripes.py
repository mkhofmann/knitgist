from typing import List, Tuple
from unittest import TestCase

from KnitGIST.ColorWork.LayerPosition import Layer_Position, Layer_Coloring
from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitOutGenerator.triple_layer_knitout import Layer_KnitOut_Generator
from KnitGIST.tests.test_tripleStSt import getTextures
from KnitGIST.tests.test_tripleTexture import getUniqueTextures


def getStripes(firstStripe: List[Layer_Position], secondStripe: List[Layer_Position], stripeWidth: int, height: int, file_postFix: str) -> \
        Tuple[Layer_Coloring, Layer_Coloring, Layer_Coloring]:
    width = stripeWidth * 2

    fileNames = []
    textureId = 0
    for first, second in zip(firstStripe, secondStripe):
        filename = f"{textureId}_{file_postFix}"
        with open(filename, "w") as layer:
            lines = []
            for row in range(0, height + 1):
                line = ""
                for c in range(0, stripeWidth):
                    line += f"{first} "
                for c in range(stripeWidth, width):
                    line += f"{second} "
                line += "\n"
                lines.append(line)
            layer.writelines(lines)
            fileNames.append(filename)
        textureId += 1

    assert len(fileNames) == 3
    layer_0 = Layer_Coloring(width, height + 1, fileNames[0])
    layer_1 = Layer_Coloring(width, height + 1, fileNames[1])
    layer_2 = Layer_Coloring(width, height + 1, fileNames[2])
    return layer_0, layer_1, layer_2


def getQuads(firstStripe: List[Layer_Position], secondStripe: List[Layer_Position], stripeWidth: int, height: int, file_postFix: str) -> \
        Tuple[Layer_Coloring, Layer_Coloring, Layer_Coloring]:
    width = stripeWidth * 2

    fileNames = []
    textureId = 0
    for first, second in zip(firstStripe, secondStripe):
        filename = f"{textureId}_{file_postFix}"
        with open(filename, "w") as layer:
            lines = []
            for row in range(0, int(height / 2)):
                line = ""
                for c in range(0, stripeWidth):
                    line += f"{first} "
                for c in range(stripeWidth, width):
                    line += f"{second} "
                line += "\n"
                lines.append(line)
            for row in range(int(height / 2), height + 1):
                line = ""
                for c in range(0, stripeWidth):
                    line += f"{second} "
                for c in range(stripeWidth, width):
                    line += f"{first} "
                line += "\n"
                lines.append(line)
            layer.writelines(lines)
            fileNames.append(filename)
        textureId += 1

    assert len(fileNames) == 3
    layer_0 = Layer_Coloring(width, height + 1, fileNames[0])
    layer_1 = Layer_Coloring(width, height + 1, fileNames[1])
    layer_2 = Layer_Coloring(width, height + 1, fileNames[2])
    return layer_0, layer_1, layer_2


class Test_ColorStripes(TestCase):

    def test_012To021_pocket(self):
        width = 10
        height = 10
        filename = "012_to_102"
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Mid, Layer_Position.Top, Layer_Position.Bot],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_012To210_swap(self):
        width = 10
        height = 10
        filename = "012_to_210"
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Bot, Layer_Position.Mid, Layer_Position.Top],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_texture_012To210_swap(self):
        width = 10
        height = 10
        filename = "texture_012_to_210"
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Bot, Layer_Position.Mid, Layer_Position.Top],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_012To120_braid(self):
        width = 10
        height = 10
        filename = "012To120"
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Mid, Layer_Position.Bot, Layer_Position.Top],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_texture_012To021_pocket(self):
        width = 10
        height = 10
        filename = "texture_012_to_102"
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Mid, Layer_Position.Top, Layer_Position.Bot],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_texture_012To120_braid(self):
        width = 10
        height = 10
        filename = "texture_012To120"
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getStripes([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                  [Layer_Position.Mid, Layer_Position.Bot, Layer_Position.Top],
                                                  int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_012To021_quad(self):
        width = 10
        height = 10
        filename = "quad_012_to_102"
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getQuads([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                [Layer_Position.Mid, Layer_Position.Top, Layer_Position.Bot],
                                                int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_012To120_quad(self):
        width = 10
        height = 10
        filename = "quad_012To120"
        stst0, stst1, stst2 = getTextures(height, width)
        botLayer, midLayer, topLayer = getQuads([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                [Layer_Position.Mid, Layer_Position.Bot, Layer_Position.Top],
                                                int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_texture_quad_012To021(self):
        width = 10
        height = 10
        filename = "text_quad_012_to_102"
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getQuads([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                [Layer_Position.Mid, Layer_Position.Top, Layer_Position.Bot],
                                                int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")

    def test_texture_quad_012To120(self):
        width = 10
        height = 10
        filename = "texture_quad_012To120"
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getQuads([Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot],
                                                [Layer_Position.Mid, Layer_Position.Bot, Layer_Position.Top],
                                                int(width / 2), height, filename)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions(f"{filename}.k")
