from unittest import TestCase

from KnitGIST.KnitGraph_Construction.DoubleKnitGraph import DoubleKnitGraph
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildStSt
from KnitGIST.KnitGraph_Construction.ks_compiler.KSCompiler import KSCompiler
from KnitGIST.KnitOutGenerator.graph_to_knitout import KnitOutGenerator


class Test(TestCase):

    def test_write_instructions(self):
        compiler = KSCompiler(r'''1st row k.
                                    2nd row p.''')
        knitGraph = compiler.buildGraph(2, 2)
        generator = KnitOutGenerator(knitGraph)
        generator.writeInstructions("stst2x2.k")

        # compiler = KSCompiler(r'''1st and 2nd rows k,p.''')
        # knitGraph = compiler.buildGraph(4, 2)
        # generator = KnitOutGenerator(knitGraph)
        # generator.writeInstructions("rib4x2.k")
        #
        # knitGraph = buildStSt(2, 2)
        # double = DoubleKnitGraph(knitGraph)
        # generator = KnitOutGenerator(double)
        # generator.writeInstructions("double.k")

