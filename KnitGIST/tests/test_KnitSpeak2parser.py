from unittest import TestCase

from KnitGIST2.KnitGraph_Construction.ks_compiler.ksParser.KnitSpeak2parser import KnitSpeak2Parser


class TestKnitSpeak2Parser(TestCase):

    def test_flipped(self):
        parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False)
        result = parser.parse("flipped 1st row k 3, [k,p] to last st, p.")
        print(result)

    def test_courseIds(self):
        parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False)
        # result = parser.parse("from 3rd to 8th rows k.")
        # print(result)
        result=parser.parse("from 3rd to 8th rows k.")
        print(result)
        result = parser.parse("from ws 3rd to 8th rows k.")
        print(result)
        result = parser.parse("from rs 3rd to 8th rows k.")
        print(result)
        result = parser.parse("from rs 3rd to cat=10 rows k.")
        print(result)
        result = parser.parse("from 3rd to cat rows k.")
        print(result)
        result = parser.parse("from 3rd to cat, and cat=4 rows k.")
        print(result)
        result = parser.parse("1st row [k 1, p 1] to last 2 sts, k 2.")
        print(result)
        result = parser.parse("1st row [k 1, p 1] to last st, k.")
        print(result)
        result = parser.parse("1st row [k 1, p 1] to end.")
        print(result)
        result = parser.parse("1st row k 1, [k,p] to last st, k.")
        print(result)

    def test_stitchStatement(self):
        parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False)
        result = parser.parse(" 1st row skpo 2.")[0]
        print(result)
        result = parser.parse("1st row k cat=4.")[0]
        print(result)
        result = parser.parse("1st row k2tog, k 2.")
        print(result)

        try:
            result = parser.parse(" 1st row [k, [k2,p2] to last 4 sts] 2.")
            assert False
        except:
            pass

    def expressions(self, parser):
        result1 = parser.parse("1")
        assert result1 == 1
        resutl2 = parser.parse("cat=10")
        assert resutl2 == 10
        result3 = parser.parse("1+1")  # 1+2 == 2
        assert result3 == 2
        result4 = parser.parse("cat+1")  # 10+1 == 11
        assert result4 == 11
        result5 = parser.parse("cat+2*2")  # 10+ (2*2) = 14
        assert result5 == 14
        resutl6 = parser.parse("cat*2+2")  # (10*2)+2 == 22
        assert resutl6 == 22
        result7 = parser.parse("1/3*2")  # 1/ (3*2) == 1/6 = 1.6666
        assert result7 == 1.0 / 6.0
