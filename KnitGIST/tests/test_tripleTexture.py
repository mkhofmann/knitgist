from unittest import TestCase

from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildStSt, buildRib1, buildGarter
from KnitGIST.KnitOutGenerator.triple_layer_knitout import Layer_KnitOut_Generator
from KnitGIST.tests.test_tripleStSt import getLayers


class Test_TripleTextures(TestCase):

    def test__012(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, midLayer, botLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_012.k")

    def test__120(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [midLayer, botLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_120.k")

    def test__201(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, topLayer, midLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_201.k")

    def test__102(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [midLayer, topLayer, botLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_102.k")

    def test__021(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [topLayer, botLayer, midLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_021.k")

    def test__210(self):
        width = 10
        height = 10
        stst0, stst1, stst2 = getUniqueTextures(height, width)
        botLayer, midLayer, topLayer = getLayers(height, width)
        layeredKnit = LayeredKnit([stst0, stst1, stst2], [botLayer, midLayer, topLayer])
        generator = Layer_KnitOut_Generator(layeredKnit)
        generator.runInstructions(bindBottom=True, ststBaseLayers=0)
        generator.writeInstructions("texture_210.k")


def getUniqueTextures(height, width):
    texture0 = buildStSt(width, height)
    texture1 = buildRib1(width, height)
    texture2 = buildGarter(width, height)
    return texture0, texture1, texture2