import random
from unittest import TestCase

from KnitGIST2.synthesis.Row_synthesis import generateRandomKorP, generateStitchBlock, unWindBlock


class Test(TestCase):
    def test_generate_stitch_block(self):
        random.seed(1)
        for i in range(0, 10):
            block = generateStitchBlock(generateRandomKorP, 5)
            print(block)

    def test_un_wind_block(self):
        random.seed(1)
        for i in range(0, 10):
            block, out = generateStitchBlock(generateRandomKorP, 5)
            block = unWindBlock(block)
            print(block)
