from unittest import TestCase

from KnitGIST2.KnitGraph_Construction.ks_compiler.ksParser.KnitSpeak2parser import KnitSpeak2Parser, knitDefinition
from KnitGIST2.KnitGraph_Construction.ks_compiler.RepWidthSolver import repWidthSolver, convertOpsToConditional


class Test(TestCase):
    def test_rep_width_solver(self):
        parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False)
        result = parser.parse("1st row k, [k, p] to last 10 sts, [k 2, p] to last st, k.")
        operations = result[0]["stitch-operations"]
        print(operations)
        finalWidth = repWidthSolver(operations)
        assert finalWidth == 13
        finalWidth = repWidthSolver(operations, 16, 13)
        assert finalWidth == 13
        finalWidth = repWidthSolver(operations, 16, 16)
        assert finalWidth == -1

    def test_convert_ops_to_conditional(self):
        parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False)
        result = parser.parse("1st row k.")
        operations = result[0]["stitch-operations"]
        print(operations)
        print(convertOpsToConditional(operations))

        result = parser.parse("1st row k, [k,p] to end.")
        operations = result[0]["stitch-operations"]
        print(operations)
        print(convertOpsToConditional(operations, [(knitDefinition(), 2)]))

