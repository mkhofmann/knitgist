from unittest import TestCase


def compareStitchLists(list1, list2):
    same = len(list1) == len(list2)
    if not same:
        return False
    if "cable" in list1:
        list1 = list1["cable"]
    elif "decrease" in list1:
        list1 = list1["decrease"]
    for op1, op2 in zip(list1, list2):
        if op1 != op2:
            return False
    return True


class Test(TestCase):
    def test_graphs(self):
        compiler = KSCompiler(r'''1st, 5th rows  k 3.
                              flipped from ws 2nd to 6th rows k 3.
                              3rd row k, t2l = cable[hold 1 front then k then k from front].''')
        knitGraph = buildGraphFromInstructions(width=3, height=6)
        plotKnitGraph(knitGraph, "test_t2l.png")
        compiler = KSCompiler(r'''1st, 5th rows  k 3.
                                      flipped from ws 2nd to 6th rows k 3.
                                      3rd row c3b = cable[hold 2 back then k then k, k from back].''')
        knitGraph = buildGraphFromInstructions(width=3, height=6)
        plotKnitGraph(knitGraph, "test_c3b.png")
        compiler = KSCompiler(r'''1st, 5th rows  k 3.
                                      flipped from ws 2nd to 6th rows k 3.
                                      3rd row yo, s2kpo = decrease[s-2, k], yo.''')
        knitGraph = buildGraphFromInstructions(width=3, height=6)
        plotKnitGraph(knitGraph, "test_s2kpo.png")
        compiler = KSCompiler(r'''1st, 5th rows  k 3.
                                      flipped from ws 2nd to 6th rows k 3.
                                      3rd row yo, s2kpo = decrease[s-2, k], yo.''')
        knitGraph = buildGraphFromInstructions(width=3, height=6)
        plotKnitGraph(knitGraph, "test_s2kpo.png")
        compiler = KSCompiler(r'''1st, 5th rows  k 3.
                                      flipped from ws 2nd to 6th rows k 3.
                                      3rd row yo, skpo = decrease[s, k], k.''')
        knitGraph = buildGraphFromInstructions(width=3, height=6)
        plotKnitGraph(knitGraph, "test_skpo.png")
