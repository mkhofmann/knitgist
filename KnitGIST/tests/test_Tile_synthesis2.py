import random
from unittest import TestCase

from KnitGIST2.KnitGraph_Construction.KnitGraphPlotter import plotKnitGraph
from KnitGIST2.KnitGraph_Construction.ks_compiler.KSCompiler import buildGraph
from KnitGIST2.synthesis.Row_synthesis import generateRandomKorP
from KnitGIST2.synthesis.Tile_synthesis import generateTile, unWindTile


class Test(TestCase):
    def test_generate_tile(self):
        random.seed(1)
        tile = generateTile(5, 5, generateRandomKorP)
        print(tile)
        knitgraph = buildGraph(tile, 5, 6)
        plotKnitGraph(knitgraph, "Ks2Tile_test.png")

    def test_un_wind_tile(self):
        tile = generateTile(5, 5, generateRandomKorP)
        tile = unWindTile(tile)
        print(tile)
        knitgraph = buildGraph(tile, 5, 6)
        plotKnitGraph(knitgraph, "Ks2Tile_test.png")
