from typing import Optional

from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import Needle
from KnitGIST.KnitOutGenerator.graph_to_knitout import Bed, MachineDirection


class StitchInstruction:
    def __init__(self, direction: MachineDirection, needle: Needle, carrier: int):
        self.direction: MachineDirection = direction
        self.needle: Needle = needle
        self.carrier: int = carrier

    def samePass(self, other) -> bool:
        assert isinstance(other, StitchInstruction)
        return self.direction == other.direction and self.carrier == other.carrier

    def __str__(self):
        return f"knit {self.direction} {self.needle} {self.carrier}\n"

    def __repr__(self):
        return str(self)

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return isinstance(other, KnitInstruction) \
               and self.direction == other.direction and self.needle == other.needle and self.carrier == other.carrier

    def __gt__(self, other):
        assert self.samePass(other)
        if self.direction == MachineDirection.R2L:
            return self.needle > other.needle
        else:
            return self.needle < other.needle

    def __lt__(self, other):
        assert self.samePass(other)
        if self.direction == MachineDirection.L2R:
            return self.needle < other.needle
        else:
            return self.needle > other.needle


class TuckInstruction(StitchInstruction):
    def __init__(self, direction: MachineDirection, needle: Needle, carrier: Optional[int]):
        self.noCarrier = carrier is None
        super().__init__(direction, needle, carrier)

    def __str__(self):
        if self.noCarrier:
            return f"tuck {self.direction} {self.needle}\n"
        else:
            return f"tuck {self.direction} {self.needle} {self.carrier}\n"


class KnitInstruction(StitchInstruction):
    def __init__(self, direction: MachineDirection, needle: Needle, carrier: int):
        assert needle.bed == Bed.Front, "Knit only on front Needles"
        super().__init__(direction, needle, carrier)


class PurlInstruction(StitchInstruction):
    def __init__(self, direction: MachineDirection, needle: Needle, carrier: int):
        assert needle.bed == Bed.Back, "Purl only on back Needles"
        super().__init__(direction, needle, carrier)


class MissInstruction(StitchInstruction):
    def __init__(self, direction: MachineDirection, needle: Needle, carrier: int):
        super().__init__(direction, needle, carrier)

    def __str__(self):
        return f"miss {self.direction} {self.needle} {self.carrier}\n"

class RackInstruction:
    def __init__(self, curNeedle, targetNeedle):
        assert curNeedle.bed != targetNeedle.bed, "Cannot align needles on same bed"
        if curNeedle.bed == Bed.Front:
            self.frontNeedle = curNeedle
            self.backNeedle = targetNeedle
        else:
            self.frontNeedle = targetNeedle
            self.backNeedle = curNeedle
        self.rack = self.frontNeedle.loc - self.backNeedle.loc

    def __str__(self):
        return f"rack {self.rack}\n"

    def __repr__(self):
        return str(self)


class XferInstruction:
    def __init__(self, curNeedle: Needle, targetNeedle: Needle):
        self.curNeedle = curNeedle
        self.targetNeedle = targetNeedle
        if self.curNeedle == self.targetNeedle:
            self.isNoOp: bool = True
            self.rack: RackInstruction = RackInstruction(self.curNeedle, self.curNeedle.opposite())
        else:
            assert self.curNeedle.bed != self.targetNeedle.bed, "Xfer instructions must be to opposite bed"
            self.isNoOp: bool = False
            self.rack: RackInstruction = RackInstruction(self.curNeedle, self.targetNeedle)

    def reverseInstruction(self):
        return XferInstruction(self.targetNeedle, self.curNeedle)

    def reversed(self, other) -> bool:
        """
        :param other: the xfer instruction being compared to
        :return: True if the xfer instruction has the same needles, but in the opposite order
        """
        assert isinstance(other, XferInstruction)
        if self.curNeedle == other.targetNeedle:
            assert self.targetNeedle == other.curNeedle
            return True
        return False

    def __str__(self):
        if self.isNoOp:
            return ""  # return an empty string since this operation is not productive

        return f"{self.rack}xfer {self.curNeedle} {self.targetNeedle}\n"

    def __repr__(self):
        if self.isNoOp:
            return "No transfer"
        return str(self)

    def __lt__(self, other):
        """
        Using this in a sort will sort the xfers from right to left (a '-' pass)
        :param other: The other xfer instruction to consider
        :return: True if other needle is to the left of curNeedle (other <- this)
        """
        assert isinstance(other, XferInstruction)
        return self.curNeedle.loc < other.curNeedle.loc
