from enum import Enum
from typing import Optional

from KnitGIST.KnitOutGenerator.graph_to_knitout import Bed


class MachineDirection(Enum):
    R2L = "+"
    L2R = "-"

    def __str__(self):
        return self.value

    def opposite(self):
        if self.value == MachineDirection.R2L.value:
            return MachineDirection.L2R
        else:
            return MachineDirection.R2L

    def resultingSide(self):
        if self == MachineDirection.R2L:
            return MachineSide.Left
        else:
            return MachineSide.Right


class MachineSide(Enum):
    Right = "right"
    Left = "left"

    def __str__(self):
        return self.value

    def opposite(self):
        if self == MachineSide.Right:
            return MachineSide.Left
        else:
            return MachineSide.Right

    def nextDirection(self) -> MachineDirection:
        if self == MachineSide.Right:
            return MachineDirection.R2L
        else:
            return MachineDirection.L2R

    def __lt__(self, other):
        """
        :param other: the other machine side to consider
        :return: True if this is right side (0 needle position) and other is left (higher needle positions)
        """
        assert isinstance(other, MachineSide)
        return self.value == MachineSide.Right and other.value == MachineSide.Left

    def __eq__(self, other):
        assert isinstance(other, MachineSide)
        return self.value == other.value


class Needle:
    FabricBuffer = 10

    def __init__(self, loc: int, bed: Bed):
        self._loc = loc
        self._bed = bed
        self.curLoopId: Optional[str] = None

    @property
    def bed(self):
        return self._bed

    @property
    def loc(self):
        return self._loc

    def addLoop(self, loopId: str, check: bool = True):
        if check:
            assert self.curLoopId is None, f"Loop {self.curLoopId} already occupies {self}"
        self.curLoopId = loopId

    def toFront(self):
        return Needle(self.loc, Bed.Front)

    def toBack(self):
        return Needle(self.loc, Bed.Back)

    def opposite(self):
        return Needle(self.loc, self.bed.opposite())

    def __str__(self):
        return f"{self._bed}{self._loc + Needle.FabricBuffer}"

    def __repr__(self):
        return f"{self._bed}{self._loc}"

    def __hash__(self):
        return self._loc

    def __eq__(self, other):
        return isinstance(other, Needle) and self.loc == other.loc and self.bed == other.bed

    def __gt__(self, other):
        assert isinstance(other, Needle)
        return self.loc > other.loc

    def __lt__(self, other):
        assert isinstance(other, Needle)
        return self.loc < other.loc
