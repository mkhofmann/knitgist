from typing import Optional, Dict, List

from KnitGIST.ColorWork.LayerPosition import Layer_Position
from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph
from KnitGIST.KnitGraph_Construction.Loop import Loop
from KnitGIST.KnitOutGenerator.KnitOutUtilities.MachinePass import Xfer_MachinePass, Stitch_MachinePass
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_instructions import StitchInstruction, XferInstruction, \
    TuckInstruction
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import Needle, MachineSide
from KnitGIST.KnitOutGenerator.graph_to_knitout import Bed, MachineDirection


class MachineState:
    def __init__(self, needleCount: int, layer_count: int = 3):
        self.layer_count: int = layer_count
        # the side each yarn carrier was left on the machine
        self.yarnIdsToSides: Dict[int, MachineSide] = {y: MachineSide.Left for y in range(0, self.layer_count)}
        self.carriageSide: MachineSide = MachineSide.Left
        self.frontBed: Dict[int, Optional[Loop]] = {n: None for n in range(1, needleCount + 1)}
        self.backBed: Dict[int, Optional[Loop]] = {n: None for n in range(1, needleCount + 1)}
        self.loopsToNeedles: Dict[Loop, Optional[Needle]] = {}
        self.loopToCreationBed: Dict[Loop, Bed] = {}  # used to determine if a stitch that has been moved is a knit or a purl
        self.rack = 0

    def getHoldingNeedle(self, loop: Loop) -> Optional[Needle]:
        if loop not in self.loopsToNeedles:
            return None
        else:
            return self.loopsToNeedles[loop]

    def needleToYarnId(self, needleLoc: int) -> int:
        mod = (needleLoc - 1) % self.layer_count
        return mod

    def needleMatchesDefaultYarnOfLayer(self, needleLoc: int, position: Layer_Position) -> bool:
        yarn_id = self.needleToYarnId(needleLoc)
        defaultLayer = Layer_Position.getPosition(yarn_id)
        return defaultLayer == position

    @property
    def needleCount(self):
        return len(self.frontBed)

    def expectTextureIndex(self, needleLoc: int) -> int:
        """
        :param needleLoc: the location of the needle being compared against yarn colors
        :return: the index to compare to a layered knitgraph for the target texture knitgraph
        """
        mod = needleLoc % self.layer_count
        return mod

    def standardTextureLayer(self, needleLoc: int) -> Layer_Position:
        textureIndex = self.expectTextureIndex(needleLoc)
        if textureIndex == 0:
            return Layer_Position.Top
        elif textureIndex == 1:
            return Layer_Position.Mid
        else:
            return Layer_Position.Bot

    def __getitem__(self, needle: Needle) -> Optional[Loop]:
        # assert isinstance(item, Needle), f"{item} of type {type(Needle)} is not a Needle"
        # Make a null needle for this location on both beds
        if needle.loc not in self.frontBed:
            self.frontBed[needle.loc] = None
        if needle.loc not in self.backBed:
            self.backBed[needle.loc] = None
        # get the current loop or none
        if needle.bed == Bed.Front:
            return self.frontBed[needle.loc]
        else:
            return self.backBed[needle.loc]

    def __setitem__(self, needle: Needle, loop: Optional[Loop]) -> None:
        if needle.bed == Bed.Front:
            self.frontBed[needle.loc] = loop
        else:
            self.backBed[needle.loc] = loop
        if loop is not None:
            self.loopsToNeedles[loop] = needle
            self.loopToCreationBed[loop] = needle.bed  # used to determine if a stitch that has been moved is a knit or a purl

    def removeLoopFromNeedle(self, needle: Needle) -> Optional[Loop]:
        loop = self[needle]
        self[needle] = None
        return loop

    def updateStateByStitch(self, instruction: StitchInstruction, newLoop: Loop) -> None:
        if not isinstance(instruction, TuckInstruction):
            assert self[instruction.needle] is not None, f"No loop to knit on {instruction.needle}"
        curLoop = self[instruction.needle]
        if curLoop is not None:
            self.loopsToNeedles[curLoop] = None
        self[instruction.needle] = newLoop

    def updateStateByXfer(self, instruction: XferInstruction) -> None:
        loop = self.removeLoopFromNeedle(instruction.curNeedle)
        self[instruction.targetNeedle] = loop

    def needlesLocsByYarn(self, yarnId: int) -> List[int]:
        """
        :param yarnId: the id of the yarn should correspond to first position
        :return: the needle locations relevant to this yarn: (e.g., 0-> [0,3,6,...] 1->[1,4,7...] for 3 layers)
        """
        return [needleLoc for needleLoc in range(yarnId, self.needleCount, self.layer_count)]

    def needleByParent(self, child_loop: Loop, knitgraph: KnitGraph) -> Optional[Needle]:
        inEdges = knitgraph.getInEdgesByParents(child_loop.loopId)
        assert len(inEdges) == 1, "Only supporting knit/purl patterns"
        for parentId in inEdges:
            parentLoop = knitgraph.getLoop(parentId)
            if parentLoop not in self.loopsToNeedles:
                return None
            needle = self.loopsToNeedles[parentLoop]
            if needle is None:  # this has been stitched over, its child should probably be on a needle
                needle = self.loopsToNeedles[child_loop]
                assert needle is not None
            return needle

    def xferPass(self, xfer_instructions: List[XferInstruction]) -> Optional[Xfer_MachinePass]:
        if len(xfer_instructions) == 0:
            return None
        direction = self.carriageSide.nextDirection()
        self.carriageSide = self.carriageSide.opposite()
        for xfer_instruction in xfer_instructions:
            self.updateStateByXfer(xfer_instruction)
        machinePass = Xfer_MachinePass(direction=direction, instructions=xfer_instructions)
        return machinePass

    def stitchPass(self, instructionsToLoops: Dict[StitchInstruction, Optional[Loop]], yarnId: int, missPosition: int) -> Stitch_MachinePass:
        yarnSide = self.yarnIdsToSides[yarnId]
        direction = yarnSide.nextDirection()
        for instruction, parentLoop in instructionsToLoops.items():
            self.updateStateByStitch(instruction, parentLoop)
        self.carriageSide = yarnSide.opposite()
        self.yarnIdsToSides[yarnId] = yarnSide.opposite()
        rightMostNeedleLoc = min(*self.frontBed.keys())
        leftMostNeedleLoc = max(*self.frontBed.keys())
        machinePass = Stitch_MachinePass(direction=direction, instructions=[*instructionsToLoops.keys()], leftMostNeedle_loc=leftMostNeedleLoc,
                                         rightMostNeedle_loc=rightMostNeedleLoc, missPosition=missPosition)
        return machinePass

    def needleLocIter(self, direction: MachineDirection) -> List[int]:
        needles = [*self.frontBed.keys()]
        if direction == MachineDirection.L2R:
            needles.reverse()
        return needles
