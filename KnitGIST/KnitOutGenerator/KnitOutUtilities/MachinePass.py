from typing import List, Optional, Dict

from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_instructions import XferInstruction, StitchInstruction, \
    MissInstruction, TuckInstruction
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import Needle
from KnitGIST.KnitOutGenerator.graph_to_knitout import MachineDirection, Bed


class MachinePass:
    def __init__(self, direction: MachineDirection):
        self._direction: MachineDirection = direction

    @property
    def direction(self) -> MachineDirection:
        return self._direction

    @property
    def instructions(self) -> list:
        return []

    def writtenInstructions(self) -> List[str]:
        return [str(instruction) for instruction in self.instructions]

    def __str__(self):
        string = "rack 0\n"  # reset racking for consistency
        for instruction in self.instructions:
            string += str(instruction)
        return string

    def __len__(self):
        return len(self.instructions)


class Xfer_MachinePass(MachinePass):
    def __init__(self, direction: MachineDirection, instructions: List[XferInstruction]):
        super().__init__(direction)
        self._instructions: List[XferInstruction] = instructions
        self.xfersByNeedleLoc: Dict[int, XferInstruction] = {xfer.curNeedle.loc: xfer for xfer in instructions}
        self._sortInstructions()

    def _sortInstructions(self) -> None:
        op_instructions = []
        for instruction in self.instructions:
            if not instruction.isNoOp:
                op_instructions.append(instruction)
        self._instructions = op_instructions
        if self._direction == MachineDirection.R2L:
            self._instructions.sort()
        else:
            self._instructions.sort(reverse=True)

    @property
    def instructions(self) -> List[XferInstruction]:
        return self._instructions

    def hasTransferFromNeedleLoc(self, location) -> bool:
        return location in self.xfersByNeedleLoc

    def undoLastPass(self, lastXfers):
        keptInstructions = []
        for instruction in self.instructions:
            if lastXfers.hasTransferFromNeeldeLoc(instruction.curNeedle.loc):
                otherXFer = lastXfers.xfersByNeedleLoc[instruction.curNeedle.loc]
                if not instruction.reversed(otherXFer):
                    keptInstructions.append(instruction)
        return Xfer_MachinePass(self.direction, keptInstructions)



class Stitch_MachinePass(MachinePass):
    MISS_BUFFER = 4

    def __init__(self, direction: MachineDirection, instructions: List[StitchInstruction], leftMostNeedle_loc: Optional[int] = None, rightMostNeedle_loc: int = 1, missPosition:Optional[int]=None):
        super().__init__(direction)
        self.carrier: int = -1
        foundCarrier = False
        for instruction in instructions:
            if foundCarrier:
                assert self.carrier == instruction.carrier, "Cannot have multiple Yarns in stitch pass"
            else:
                self.carrier = instruction.carrier
                foundCarrier = True
            assert instruction.direction == self.direction, "Stitching in wrong pass, we do not support twisted loops"
        self._instructions: List[StitchInstruction] = instructions
        # add an empty tuck on a needle that holds nothing at the end to move the carriage past the fabric's edge
        # if direction == MachineDirection.L2R:
        #     tuckNeedle_loc = rightMostNeedle_loc - 1
        # else:
        #     tuckNeedle_loc = leftMostNeedle_loc + 1
        # tuckNeedle = Needle(tuckNeedle_loc, Bed.Front)  # note, bed does not matter for this tuck of no loops
        # self._instructions.append(TuckInstruction(self.direction, tuckNeedle, None))

        # # add a miss to override kickback behavior
        if missPosition is None:
            missPosition = self.carrier
        if direction == MachineDirection.L2R:  # end on right side, go to lower needles
            missNeedle_loc = rightMostNeedle_loc - missPosition
        else:
            missNeedle_loc = leftMostNeedle_loc + missPosition  # end on left side, go to higher needles
        missNeedle = Needle(missNeedle_loc, Bed.Front)  # note, bed does not matter for misses
        self._instructions.append(MissInstruction(self.direction, missNeedle, self.carrier))
        # self._instructions.append(TuckInstruction(self.direction, tuckNeedle, None))  # re-tuck to reset pass type to avoid errors in knitout-dat

    @property
    def instructions(self) -> List[StitchInstruction]:
        return self._instructions
