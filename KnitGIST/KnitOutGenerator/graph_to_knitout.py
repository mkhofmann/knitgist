from enum import Enum
from typing import Dict, Tuple, List, Union, Optional, Set

from sortedcontainers import SortedDict

from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph


class Bed(Enum):
    Front = "f"
    Back = "b"

    def __str__(self):
        return self.value

    def opposite(self):
        if self.value == Bed.Front.value:
            return Bed.Back
        else:
            return Bed.Front


class MachineDirection(Enum):
    R2L = "+"  # todo how were these wrong?
    L2R = "-"

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.name

    def opposite(self):
        if self.value == MachineDirection.R2L.value:
            return MachineDirection.L2R
        else:
            return MachineDirection.R2L

    def __eq__(self, other):
        return self.value == other.value


class PullDirection(Enum):
    BTF = "btf"
    FTB = "ftb"

    def opposite(self):
        if self.value == PullDirection.BTF.value:
            return PullDirection.FTB
        else:
            return PullDirection.BTF

    def __eq__(self, other):
        if isinstance(other, PullDirection):
            return self.value == other.value
        else:
            return self.value == other


class KnitOutGenerator:
    def __init__(self, knitGraph: KnitGraph, carrier: int = 3, bindOff: bool = False):
        self._carrier: int = carrier
        self._knitGraph: KnitGraph = knitGraph
        self._loopIdToLocations: Dict[str: Union[
            Tuple[int, Bed], str]] = {}  # loopId's corresponds to node IDs, points to tuple (needleIndex, bed)
        self._needlesToLoopStacks: Dict[Bed, Dict[int, List[str]]] = {Bed.Front: {},
                                                                      Bed.Back: {}}
        self._instructionsByRow: Dict[int, List[str]] = {}
        self._instructions: list = []
        self._rackPosition: int = 0
        self._frontBedTargetAlignment: dict = {}  # actualNeedle -> target needle for loop Stack
        self._castOn()
        for row in range(1, self._knitGraph.maxRow):
            self.addRow(row)
        if bindOff:
            self.bindOff()
        else:
            self.dropRow()

    def bindOff(self, chain: int = 10):
        """
        Adds bind off instructions to end
        :param chain: the length of the chain that preserves yarn for pulling through final slip stitch
        """
        bindOffInstructions = []
        start = max(*self._needlesToLoopStacks[Bed.Front])
        for n in range(start, 1, -1):
            self.transferAcrossBeds(bindOffInstructions, n, Bed.Front)
            self.align(bindOffInstructions, 1, 2)
            self.transferAcrossBeds(bindOffInstructions, n, Bed.Back, allowOverlap=True, xferNeedle=n - 1)
            self.align(bindOffInstructions)
            self.knit(bindOffInstructions, MachineDirection.R2L, n - 1, newLoopId=f"bo{n}")
        for i in range(0, chain):
            if i % 2 == 0:
                self.knit(bindOffInstructions, MachineDirection.R2L, 1, f"chain{i}")
            else:
                self.knit(bindOffInstructions, MachineDirection.L2R, 1, f"chain{i}")

        bindOffInstructions.append("outhook {}\n".format(self._carrier))
        bindOffInstructions.append("drop {}{}\n".format(Bed.Front, 1))
        self._instructionsByRow[max(*self._instructionsByRow.keys()) + 1] = bindOffInstructions

    def _getCarrierByLoop(self, loopId: str) -> int:
        loop = self._knitGraph.getLoop(loopId)
        yarnId = loop.yarnId
        return self._getCarrier(yarnId)

    def _getCarrier(self, yarnId: int) -> int:
        return yarnId + self._carrier

    def _castOn(self):
        """
        create cast on instructions and set up loop stacks
        """
        rowInstructions: List[str] = [";!knitout-2\n",
                                      ";;Carriers: 1 2 3 4 5 6 7 8 9 10\n"]
        for yarnId in self._knitGraph.yarns:
            rowInstructions.append(f"inhook {self._getCarrier(yarnId)}\n")
        width = len(self._knitGraph.rows[0])
        for n in range(1, width + 1):
            self._needlesToLoopStacks[Bed.Front][n] = []  # LIFO stacks of loops
            self._needlesToLoopStacks[Bed.Front][n] = []
            self._needlesToLoopStacks[Bed.Back][n] = []
            self._needlesToLoopStacks[Bed.Back][n] = []
        endPass1 = int(width % 2 == 0)  # 1 if even width, 0 if odd width
        startPass1 = 2 - endPass1  # 2 if odd width, 1 if even width
        for i in range(width, endPass1, -2):  # todo even width?
            actual_needle = i
            rowInstructions.append(f"tuck - f{actual_needle} {self._carrier}\n")
        for j in range(startPass1, width, 2):
            actual_needle = j
            rowInstructions.append(f"tuck + f{actual_needle} {self._carrier}\n")
        rowInstructions.append(f"releasehook {self._carrier}\n")
        maxNeedle = max(*self._needlesToLoopStacks[Bed.Front].keys())
        for n in range(maxNeedle, 0, -1):
            rowInstructions.append(f"knit - f{n} 3\n")
        for n in range(1, maxNeedle + 1):
            rowInstructions.append(f"knit + f{n} 3\n")
        for i, loop in enumerate(reversed(self._knitGraph.rows[0])):
            needle = i + 1
            self._recordLoopLocation(loop.loopId, needle, Bed.Front)

        self._instructionsByRow[0] = rowInstructions

    # todo: if code working, delete
    # def retargetNeedle(self, curNeedle, targetNeedle):
    #     self._frontBedTargetAlignment[curNeedle] = targetNeedle

    def _getLoopStack(self, bed: Bed, needle: int):
        return self._needlesToLoopStacks[bed][needle]

    def _recordLoopLocation(self, loopId: str, needle: int, bed: Bed):
        """
        records the location of loop by loop ID on a needle and bed
        :param loopId: id of loop being recorded
        :param needle: the needle position being applied
        :param bed: the bed being applied
        """
        self._loopIdToLocations[loopId] = (needle, bed)
        self._needlesToLoopStacks[bed][needle].append(loopId)  # LIFO stacks of loops

    def clearLoopStack(self, needle: int, bed: Bed, dropped: bool = True):
        """
        Removes all the loops on a needle
        :param needle: the needle position being cleared
        :param bed: the bed position being cleared
        :param dropped: if true, sets teh value to be "dropped"
        """
        if dropped:
            for loopId in self._getLoopStack(bed, needle):
                self._loopIdToLocations[loopId] = "dropped"
        self._needlesToLoopStacks[bed][needle] = []

    def addRow(self, rowIndex: int):
        rowInstructions = []
        if rowIndex % 2 == 1:
            passDirection = MachineDirection.R2L
        else:
            passDirection = MachineDirection.L2R
        self.prepareRow(rowInstructions, passDirection)
        width = len(self._knitGraph.rows[rowIndex])
        sortedLoopIds = []
        loopIdToTargetNeedle: Dict[str, int] = {}
        for i, loop in enumerate(self._knitGraph.rows[rowIndex]):
            targetNeedle = self.getTargetNeedleByLoopIndex(i, passDirection, width)
            sortedLoopIds.append(loop.loopId)
            loopIdToTargetNeedle[loop.loopId] = targetNeedle
        # sortedLoopIds = sorted(sortedLoopIds, reverse=True)
        missAlignedNeedlesByDepth = SortedDict()
        for i, loopId in enumerate(sortedLoopIds):
            curNeedle = self.getTargetNeedleByLoopIndex(i, passDirection, width)
            targetNeedle = loopIdToTargetNeedle[loopId]
            self.addStitch(rowInstructions, loopId, curNeedle, passDirection)
            if targetNeedle != curNeedle:
                curNeedleLoopStack = self._getLoopStack(Bed.Front, curNeedle)
                assert len(
                    curNeedleLoopStack) == 1  # should have a loop and everything should be unstacked at this step
                edge = self._knitGraph.graph.pred[curNeedleLoopStack[0]]
                assert len(edge) == 1, "MissAligned needles should be cables with one edge per needle"
                edgeDepth = None
                for edgeParent in edge:
                    edgeDepth = edge[edgeParent]["depth"]
                if edgeDepth not in missAlignedNeedlesByDepth:
                    missAlignedNeedlesByDepth[edgeDepth] = []
                missAlignedNeedlesByDepth[edgeDepth].append((curNeedle, targetNeedle))
        for depth in missAlignedNeedlesByDepth:
            for missAlignedNeedle, targetNeedle in missAlignedNeedlesByDepth[depth]:
                self.transferAcrossBeds(rowInstructions, missAlignedNeedle, Bed.Front,
                                        False)  # transfer all misaligned needles back at once
        for depth in reversed(missAlignedNeedlesByDepth):  # iterate from highest to least depth values
            for missAlignedNeedle, targetNeedle in missAlignedNeedlesByDepth[depth]:
                self.align(rowInstructions, targetNeedle, missAlignedNeedle)
                self.transferAcrossBeds(rowInstructions, missAlignedNeedle, Bed.Back, False, targetNeedle)
        self.align(rowInstructions)
        self._instructionsByRow[rowIndex] = rowInstructions

    @staticmethod
    def getTargetNeedleByLoopIndex(loopIndex, passDirection, width):
        if passDirection == MachineDirection.R2L:
            targetNeedle = width - loopIndex
        else:
            targetNeedle = loopIndex + 1
        return targetNeedle

    def prepareRow(self, rowInstructions: List[str], passDirection: MachineDirection):
        """
        Put all loops on the front bed between row creation
        :param rowInstructions: The rowInstruction list being added to
        :param passDirection: the direction the needle carrier will pass
        """
        self.align(rowInstructions)  # reset racking to 0
        targetSidesByNeedle: Dict[int, Bed] = {}  # target needle to target bed
        targetNeedles: Dict[int, Tuple[int, int]] = {}  # current needles to (target needle, parent order)
        needleToCurrentBed: Dict[int, Bed] = {}  # current needle to current bed
        needleInDec: Set[int] = set()
        for needle in self._needlesToLoopStacks[Bed.Front]:
            frontStack: List[str] = self._getLoopStack(Bed.Front, needle)
            backStack: List[str] = self._getLoopStack(Bed.Back, needle)
            assert len(frontStack) + len(backStack) == 1, "Cannot have loops on back and front bed at position"
            if len(frontStack) == 1:
                loopId: str = frontStack[0]
                bed = Bed.Front
            else:
                loopId: str = backStack[0]
                bed = Bed.Back
            needleToCurrentBed[needle] = bed
            successors = self._knitGraph.getOutEdgesByChildren(parentId=loopId)
            assert len(successors) == 1, "Loop cannot have multiple children, note increases are YO"
            childId = [*successors.keys()][0]
            pullDirection = successors[childId]["direction"]
            childPredessors = self._knitGraph.getInEdgesByParents(childId=childId)
            if len(childPredessors) > 1:  # decrease
                needleInDec.add(needle)
            if pullDirection == "btf":
                targetBed = Bed.Front
            else:
                targetBed = Bed.Back
            childLoop = self._knitGraph.getLoop(childId)
            # assert childLoop.row == rowIndex, "Look ahead one row"  # todo, slips?
            indexInNextRow = self._knitGraph.rows[childLoop.row].index(childLoop)
            targetNeedle = self.getTargetNeedleByLoopIndex(indexInNextRow, passDirection,
                                                           len(self._knitGraph.rows[childLoop.row]))
            targetNeedles[needle] = targetNeedle, successors[childId]["parentOrder"]
            assert targetNeedle not in targetSidesByNeedle or targetBed == targetSidesByNeedle[
                targetNeedle], "cannot have different pull directions on same needle"
            targetSidesByNeedle[targetNeedle] = targetBed

        transfersByParentOrder: Dict[
            int, List[Tuple[int, int]]] = SortedDict()  # parentOrder to list of (current needle, target needle)
        for needle in targetNeedles:  # transfers everything to back bed
            targetNeedle, parentOrder = targetNeedles[needle]
            if needle != targetNeedle or needle in needleInDec:  # needs transfers
                if parentOrder not in transfersByParentOrder:
                    transfersByParentOrder[parentOrder] = []
                transfersByParentOrder[parentOrder].append((needle, targetNeedle))
                if Bed.Front == needleToCurrentBed[needle]:
                    self.transferAcrossBeds(rowInstructions, needle,
                                            Bed.Front)  # transfer everything that will move to back bed
                    needleToCurrentBed[needle] = Bed.Back

        for parentOrder in transfersByParentOrder:
            for needle, targetNeedle in transfersByParentOrder[
                parentOrder]:  # note: needle is on back, target is always front
                self.align(rowInstructions, targetNeedle, needle)
                self.transferAcrossBeds(rowInstructions, needle, Bed.Back, allowOverlap=True, xferNeedle=targetNeedle)
                needleToCurrentBed[targetNeedle] = Bed.Front
        self.align(rowInstructions)
        for needle in targetSidesByNeedle:
            curBed = needleToCurrentBed[needle]
            targetBed = targetSidesByNeedle[needle]
            if curBed != targetBed:
                self.transferAcrossBeds(rowInstructions, needle, curBed)

    def transferAcrossBeds(self, rowInstructions: List[str], curNeedle: int, curBed: Bed, allowOverlap: bool = False,
                           xferNeedle: Optional[int] = None):
        """
        write a transfer instruction from needle and bed position to opposite bed. Reset the needles stacks accordingly
        :param rowInstructions: the set of instructions being added to
        :param xferNeedle:the needle to transfer over to, defaults to same as current needle
        :param curNeedle: need to switch between
        :param curBed: the bed that loops are being passed from
        :param allowOverlap: if true, loops may be on the transfer bed
        """
        if xferNeedle is None:
            xferNeedle = curNeedle
        xferBed = curBed.opposite()
        curLoopStack = self._getLoopStack(curBed, curNeedle)  # loops currently held on that needle
        assert len(curLoopStack) > 0, f"Nothing to transfer from {curBed}{curNeedle}"
        if not allowOverlap:
            xferStack = self._getLoopStack(xferBed, xferNeedle)
            assert len(xferStack) == 0, "Not allowing transfer on top of existing Loops"
        for loopId in curLoopStack:
            self._recordLoopLocation(loopId, xferNeedle, xferBed)
        self.clearLoopStack(curNeedle, curBed, dropped=False)
        rowInstructions.append(f"xfer {curBed}{curNeedle} {xferBed}{xferNeedle}\n")

    def knit(self, rowInstructions: List[str], machineDirection: MachineDirection, needle: int, newLoopId: str):
        """
        creates a knit instruction at location and updates loop locations
        :param rowInstructions: the set of instructions being added to
        :param machineDirection: the direction the carrier is moving
        :param needle: the needle being knit
        :param newLoopId: the id of the new loop created by the knit
        """
        self.clearLoopStack(needle, Bed.Front)  # drop anything on this needle
        self._recordLoopLocation(newLoopId, needle, Bed.Front)  # record the new loop placed on this needle
        rowInstructions.append(f"knit {machineDirection} f{needle} {self._getCarrierByLoop(newLoopId)}\n")

    def purl(self, rowInstructions: List[str], machineDirection: MachineDirection, needle: int, newLoopId: str):
        """
        creates a purl instruction at location and updates loop locations
        :param rowInstructions: the set of instructions being added to
        :param machineDirection: the direction the carrier is moving
        :param needle: the needle being knit
        :param newLoopId: the id of the new loop created by the knit
        """
        self.clearLoopStack(needle, Bed.Back)  # drop anything on this needle
        self._recordLoopLocation(newLoopId, needle, Bed.Back)  # record the new loop placed on this needle
        rowInstructions.append(f"knit {machineDirection} b{needle} {self._getCarrierByLoop(newLoopId)}\n")

    def addStitch(self, rowInstructions: List[str], loopId: str, needle: int, passDirection: MachineDirection):
        """
        Determines the type of stitch and adds the approriate instructions
        :param loopId: the id of the new loop created by the knit
        :param rowInstructions: the set of instructions being added to
        :param passDirection: the direction the carrier is moving
        :param needle: the needle being knit
        """
        parents = self._knitGraph.graph.pred[loopId]
        if len(parents) > 0:
            for parent in parents:
                pullDirection = parents[parent]["direction"]
                if pullDirection == "btf":
                    self.knit(rowInstructions, passDirection, needle, loopId)
                else:
                    self.purl(rowInstructions, passDirection, needle, loopId)
                break
        else:  # yarn-over
            self.yarnOver(rowInstructions, loopId, passDirection, needle)
            self._frontBedTargetAlignment[needle] = needle

    def yarnOver(self, rowInstructions: List[str], loopId: str, passDirection: MachineDirection, needle: int):
        """
        Creates a yarn over instruction at the location and updates loop locations
        :param loopId: the id of the new loop created by the knit
        :param rowInstructions: the set of instructions being added to
        :param passDirection: the direction the carrier is moving
        :param needle: the needle being knit
        """
        targetNeedleStack = self._getLoopStack(Bed.Front, needle)
        if len(targetNeedleStack) > 0:
            self.transferAcrossBeds(rowInstructions, needle, Bed.Front, False)
        self._recordLoopLocation(loopId, needle, Bed.Front)
        rowInstructions.append(f"tuck {passDirection} f{needle} {self._getCarrierByLoop(loopId)}\n")

    def align(self, rowInstructions: List[str], frontNeedle: int = 1, backNeedle: int = 1):
        """
        Aligns the two beds to support a transfer
        :param rowInstructions:
        :param frontNeedle:
        :param backNeedle:
        """

        def getGlobalRackPosition(fn: int, bn: int):
            return fn - bn

        newRack = getGlobalRackPosition(frontNeedle, backNeedle)
        assert newRack <= 8, f"Cannot rack {newRack} > 8: f{frontNeedle} to b{backNeedle}"
        if newRack != self._rackPosition:
            self._rackPosition = newRack
            rowInstructions.append(f"rack {self._rackPosition}\n")

    def writeInstructions(self, knitOutFileName: str):
        """
        Writes the generated instructions to file
        :param knitOutFileName: file holding instructions
        """
        with open(knitOutFileName, "w") as f:
            for row in self._instructionsByRow:
                for instruction in self._instructionsByRow[row]:
                    f.write(instruction)
            f.close()

    def dropRow(self, rowIndex: Optional[int] = None):
        """
        adds instructions at row that drops all needles
        :param rowIndex: the row being dropped
        """
        if rowIndex is None:
            rowIndex = max(self._instructionsByRow.keys()) + 1
        rowInstructions = []
        self.align(rowInstructions)
        rowInstructions.append(f"outhook {self._carrier}\n")
        for bed in self._needlesToLoopStacks:
            for needle in self._needlesToLoopStacks[bed]:
                rowInstructions.append(f"drop {bed}{needle}\n")
        self._instructionsByRow[rowIndex] = rowInstructions
