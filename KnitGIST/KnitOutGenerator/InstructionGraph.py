from enum import Enum
from typing import Dict, List, Optional, Union

from networkx import DiGraph, topological_sort

from KnitGIST.KnitGraph_Construction.DoubleKnitGraph import DoubleKnitGraph
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import MachineDirection, Needle
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_instructions import KnitInstruction, XferInstruction
from KnitGIST.KnitOutGenerator.graph_to_knitout import Bed


class Face(Enum):
    Front = "front"
    Back = "back"

    def __str__(self):
        return str(self.value)

    def opposite(self):
        if self.value == Face.Front.value:
            return Face.Back
        else:
            return Face.Front


def getFace(needle_loc: int) -> Face:
    if needle_loc % 2 == 1:
        return Face.Front
    else:
        return Face.Back


class KnitOutInstructionGraph:
    def __init__(self, knitGraph: DoubleKnitGraph, bindOff: bool = False):
        self._knitGraph: DoubleKnitGraph = knitGraph
        self._graph: DiGraph = DiGraph()
        self._nextNode: int = 0
        self._lastNode: int = -1
        self._preambleNode: int = -1
        self._carrier: int = 3
        self._rackPosition: int = 0
        self._loopsIdsToNodes: Dict[str, int] = {}
        self._loopIdsToNeedle: Dict[str, Needle] = {}
        self._needlesToCurLoopIds: Dict[Bed.Front: Dict[int, Optional[str]]] = {Bed.Front: {}, Bed.Back: {}}
        self._rowToNodes: Dict[int, List[int]] = {}
        self._yarnIDToBed: Dict[int, Bed] = {0: Bed.Front, 1: Bed.Back}
        self._faceToNeedleLocs: Dict[str, List[int]] = {"front": [], "back": []}
        self._preamble()
        self._yarnsToNodes: Dict[int, int] = {}
        self._castOn()
        for row in self._knitGraph.rows:
            if row > 0:
                self._addRow(row)
        if bindOff:
            self.bindOff()
        self._outYarns()

    def _outYarns(self):
        for yarnId in self._yarnsToNodes:
            outNode = self._addNode(f"outhook {self._getCarrier(yarnId)}\n")
            self._addEdge(outNode, self._yarnsToNodes[yarnId])

    def _addNode(self, instruction: Union[str, KnitInstruction, XferInstruction], edgeToLastNode=True) -> int:
        curVal = self._nextNode
        self._graph.add_node(self._nextNode, instruction=instruction)
        if edgeToLastNode:
            self._addEdge(curVal)
        self._nextNode += 1
        self._lastNode += 1
        return curVal

    def _addEdge(self, toNode: int, priorNode: Optional[int] = None):
        if priorNode is None:
            priorNode = self._lastNode
        assert 0 <= priorNode < toNode
        self._graph.add_edge(priorNode, toNode)

    def _preamble(self):
        self._preambleNode = self._addNode(instruction=r""";!knitout-2
;;Machine: SWG091N2
;;Gauge: 5
;;Carriers: 1 2 3 4 5 6 7 8 9 10
;;Position: Right
""", edgeToLastNode=False)

    def _getCarrier(self, yarnId: int) -> int:
        return yarnId + self._carrier

    def _getCarrierByLoop(self, loopId: str) -> int:
        loop = self._knitGraph.getLoop(loopId)
        yarnId = loop.yarnId
        return self._getCarrier(yarnId)

    def _mapLoopToYarn(self, loopId: str, loopNode: int):
        loop = self._knitGraph.getLoop(loopId)
        yarnId = loop.yarnId
        yarnNode = self._yarnsToNodes[yarnId]
        self._addEdge(loopNode, yarnNode)

    def topSortInstructions(self) -> List[Union[str, KnitInstruction]]:
        return [self._graph.nodes[node]["instruction"] for node in topological_sort(self._graph)]

    def carriageReturnOptimizedInstructions(self) -> List[str]:
        basicInstructions = self.topSortInstructions()
        castOnInstructions = []
        segmentInstructions: List[List[Union[str, KnitInstruction]]] = []
        lastInstruction = None
        startingInstruction = 1
        for i, instruction in enumerate(basicInstructions):
            if isinstance(instruction, KnitInstruction):
                castOnInstructions.append(str(instruction))
            elif "releasehook 4" not in instruction:
                castOnInstructions.append(instruction)
            else:
                castOnInstructions.append(instruction)
                lastInstruction = basicInstructions[i + 1]
                startingInstruction = i + 2
                break

        currentSegment = [lastInstruction]
        for nextInstruction in basicInstructions[startingInstruction:]:
            if isinstance(lastInstruction, XferInstruction) and isinstance(nextInstruction, XferInstruction):
                currentSegment.append(nextInstruction)
            elif isinstance(lastInstruction, KnitInstruction) and isinstance(nextInstruction, KnitInstruction) \
                    and lastInstruction.samePass(nextInstruction):  # same pass of stitching
                currentSegment.append(nextInstruction)
            else:
                segmentInstructions.append(currentSegment)
                currentSegment = [nextInstruction]
            lastInstruction = nextInstruction
        segmentInstructions.append(currentSegment)
        optimizedInstructions = castOnInstructions
        lastSegmentDir = None
        for segment in segmentInstructions:
            firstInstruction = segment[0]
            if isinstance(firstInstruction, KnitInstruction):
                lastSegmentDir = firstInstruction.direction
                sortedKnits = sorted(segment)
                sortedKnitStrings = [str(knit) for knit in sortedKnits]
                optimizedInstructions.extend(sortedKnitStrings)
            elif isinstance(firstInstruction, XferInstruction):
                sortedXfers = sorted(segment)
                keptXfers = []
                i = 0
                while i < len(sortedXfers) - 1:
                    firstXfer = sortedXfers[i]
                    secondXfer = sortedXfers[i + 1]
                    assert isinstance(firstXfer, XferInstruction)
                    if not firstXfer.reversed(secondXfer):
                        keptXfers.append(firstXfer)
                        i += 1  # keep first and try second vs third
                    else:
                        i += 2  # drop both first and second and move onto third and fourth
                if i == len(sortedXfers) - 1:
                    keptXfers.append(sortedXfers[i])
                if len(keptXfers) > 0:
                    sortedXfersStrings = [str(xfer) for xfer in keptXfers]
                    if lastSegmentDir == "-":  # last carriage pass was left to right
                        optimizedInstructions.extend(sortedXfersStrings)
                        lastSegmentDir = "+"
                    else:
                        lastSegmentDir = "-"
                        reversedXfers = reversed(sortedXfersStrings)
                        optimizedInstructions.extend(reversedXfers)
            else:
                optimizedInstructions.extend(segment)

        return optimizedInstructions

    def writeInstructions(self, knitOutFileName: str):
        """
        Writes the generated instructions to file
        :param knitOutFileName: file holding instructions
        """
        with open(knitOutFileName, "w") as f:
            f.writelines(self.carriageReturnOptimizedInstructions())
            f.close()

    def _addStitch(self, loopId: str, needle: Needle, passDirection: MachineDirection, parentId=None) -> int:
        parentIds = self._knitGraph.getInEdgesByParents(loopId)
        if parentId is None:
            if len(parentIds) == 1:
                parentId = list(parentIds.keys())[0]
            else:
                assert False, "Only knit purl currently supported"
        pullDirection = parentIds[parentId]["direction"]
        if pullDirection == "btf":
            newNode = self._knit(passDirection, needle, loopId)
        else:
            newNode = self._purl(passDirection, needle, loopId)
        if parentId in self._loopsIdsToNodes:
            self._addEdge(newNode, self._loopsIdsToNodes[parentId])  # create an edge for parent loop to this loop creation
        self._mapLoopToYarn(loopId, newNode)
        self._loopsIdsToNodes[loopId] = newNode
        self._loopIdsToNeedle[loopId] = needle
        self._needlesToCurLoopIds[needle.bed][needle.loc] = loopId
        return newNode

    def _knit(self, passDirection: MachineDirection, needle: Needle, childId: str):
        assert needle.bed == Bed.Front
        return self._addNode(KnitInstruction(str(passDirection), needle, self._getCarrierByLoop(childId)))

    def _purl(self, passDirection: MachineDirection, needle: Needle, childId: str):
        assert needle.bed == Bed.Back
        return self._addNode(KnitInstruction(str(passDirection), needle, self._getCarrierByLoop(childId)))

    def _align(self, frontNeedle: int = 1, backNeedle: int = 1) -> Optional[int]:
        """
        Aligns the two beds to support a transfer
        :param frontNeedle: the needle location to align with back needle
        :param backNeedle: the needle location to align with front needle.
        """
        newRack = frontNeedle - backNeedle
        return self._rack(newRack)

    def _rack(self, racking: int = 0) -> Optional[int]:
        """
        Creates a racking node that realigns the bed
        :param racking: the racking alignment B+R = F
        :return: the node of this racking
        """
        if racking != self._rackPosition:
            self._rackPosition = racking
            return self._addNode(f"rack {self._rackPosition}\n")
        return None

    def _castOn(self):
        self._preAssignCastOnLocations()
        yarnNode = self._addNode(f"inhook {self._getCarrier(0)}\n")  # bring in first yarn
        self._yarnsToNodes[0] = yarnNode

        # Cast On First yarn
        self._castOnYarn(0, self.castOnWidth)

        # Switch out yarn
        self._addNode(f"releasehook {self._getCarrier(0)}\n")  # unhook first yarn
        yarnNode = self._addNode(f"inhook {self._getCarrier(1)}\n")  # bring in second yarn
        self._yarnsToNodes[1] = yarnNode

        # Cast On Second Yarn
        self._castOnYarn(1, self.castOnWidth)
        self._addNode(f"releasehook {self._getCarrier(1)}\n")  # unhook first yarn

        # Add 2 STST rows of colors
        frontYarnNode = self._yarnsToNodes[0]
        backYarnNode = self._yarnsToNodes[1]
        for f in range(self.castOnWidth - 1, 0, -2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(f, Bed.Front), self._getCarrier(0)))
            self._addEdge(newNode, frontYarnNode)
        for b in range(self.castOnWidth, 1, -2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(b, Bed.Back), self._getCarrier(1)))
            self._addEdge(newNode, backYarnNode)
        for f in range(1, self.castOnWidth, 2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.R2L), Needle(f, Bed.Front), self._getCarrier(0)))
            self._addEdge(newNode, frontYarnNode)
        for b in range(2, self.castOnWidth + 1, 2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.R2L), Needle(b, Bed.Back), self._getCarrier(1)))
            self._addEdge(newNode, backYarnNode)
        for f in range(self.castOnWidth - 1, 0, -2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(f, Bed.Front), self._getCarrier(0)))
            self._addEdge(newNode, frontYarnNode)
        for b in range(self.castOnWidth, 1, -2):
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(b, Bed.Back), self._getCarrier(1)))
            self._addEdge(newNode, backYarnNode)

    def _castOnYarn(self, yarnId: int, castOnWidth: int):
        self._align(frontNeedle=2, backNeedle=4)
        yarnNode = self._yarnsToNodes[yarnId]
        for b in range(castOnWidth, 0, -2):
            f = b - 1
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(f, Bed.Front), self._getCarrier(yarnId)))
            self._addEdge(newNode, yarnNode)
            newNode = self._addNode(KnitInstruction(str(MachineDirection.L2R), Needle(b, Bed.Back), self._getCarrier(yarnId)))
            self._addEdge(newNode, yarnNode)
        self._align(frontNeedle=4, backNeedle=4)
        for f in range(1, castOnWidth, 2):
            b = f + 1
            newNode = self._addNode(KnitInstruction(str(MachineDirection.R2L), Needle(f, Bed.Front), self._getCarrier(yarnId)))
            self._addEdge(newNode, yarnNode)
            newNode = self._addNode(KnitInstruction(str(MachineDirection.R2L), Needle(b, Bed.Back), self._getCarrier(yarnId)))
            self._addEdge(newNode, yarnNode)
        self._rack(0)

    def _preAssignCastOnLocations(self):
        self.castOnWidth = len(self._knitGraph.rows[0])
        assert self.castOnWidth % 2 == 0, "DoubleKnitGraph must have even stitch count"
        # assign loop locations after cast ont
        for i, loop in enumerate(self._knitGraph.rows[0]):
            if i % 2 == 0:
                side = Bed.Back
                self._faceToNeedleLocs["back"].append(i)
            else:
                side = Bed.Front
                self._faceToNeedleLocs["front"].append(i)
            needle = Needle(self.castOnWidth - i, side)
            self._loopIdsToNeedle[loop.loopId] = needle
            self._needlesToCurLoopIds[side][needle.loc] = loop.loopId
            self._needlesToCurLoopIds[side.opposite()][needle.loc] = None

    def _addRow(self, row_index: int):
        if row_index % 2 == 1:  # RS row:
            direction = MachineDirection.R2L
        else:
            direction = MachineDirection.L2R

        self._transferForStitching(0, row_index)
        self._knitRow(0, row_index, direction)
        self._putParentsOnMainBeds(0)
        self._transferForStitching(1, row_index)
        self._knitRow(1, row_index, direction)
        self._putParentsOnMainBeds(1)

    def _getHeldParent(self, loop) -> str:
        foundParent = False
        for parentId in self._knitGraph.getInEdgesByParents(loop.loopId):
            if parentId in self._loopIdsToNeedle:
                needle = self._loopIdsToNeedle[parentId]
                if self._needlesToCurLoopIds[needle.bed][needle.loc] == parentId:
                    return parentId
        assert foundParent, f"No parent of {loop} is currently held"

    def _xfer(self, curNeedle: Needle, targetNeedle: Needle):
        assert curNeedle.bed != targetNeedle.bed, f"Cannot transfer over single bed {curNeedle} -> {targetNeedle}"
        if curNeedle.bed == Bed.Front:
            frontNeedle = curNeedle
            backNeedle = targetNeedle
        else:
            frontNeedle = targetNeedle
            backNeedle = curNeedle
        priorRack = self._rackPosition
        self._align(frontNeedle.loc, backNeedle.loc)
        self._addNode(XferInstruction(curNeedle, targetNeedle))
        self._rack(priorRack)

    def _xferLoop(self, loopId, targetNeedle: Needle):
        curNeedle = self._loopIdsToNeedle[loopId]
        assert self._needlesToCurLoopIds[curNeedle.bed][curNeedle.loc] == loopId, f"{loopId} not currently on f{curNeedle}"
        self._xfer(curNeedle, targetNeedle)
        self._loopIdsToNeedle[loopId] = targetNeedle
        self._needlesToCurLoopIds[curNeedle.bed][curNeedle.loc] = None
        self._needlesToCurLoopIds[targetNeedle.bed][targetNeedle.loc] = loopId

    def _putParentsOnMainBeds(self, face: Face):
        """
        Moves loops to appropriate bed based on their face
        All front face loops (odd needles) go to the front bed
        All back face loops (even needles) go to the back bed
        :param face: the face being transferred
        """
        for needle_loc in self._needlesToCurLoopIds[Bed.Front]:
            if getFace(needle_loc) == face:
                if face == Face.Front and self._needlesToCurLoopIds[Bed.Front][needle_loc] is None:  # nothing on front bed for front face
                    loopId = self._needlesToCurLoopIds[Bed.Back][needle_loc]
                    assert loopId is not None, f"No loop found on either bed at N{needle_loc}"
                    self._xferLoop(loopId, Needle(needle_loc, Bed.Front))
                elif self._needlesToCurLoopIds[Bed.Back][needle_loc] is None:  # nothing on back bed for back face
                    loopId = self._needlesToCurLoopIds[Bed.Front][needle_loc]
                    assert loopId is not None, f"No loop found on either bed at N{needle_loc}"
                    self._xfer(loopId, Needle(needle_loc, Bed.Back))

    def _transferForStitching(self, stitchingYarn: int, rowIndex: int):
        """
        Transfers every loop in row with specific yarn to the bed needed to make the correct stitch type
        ie (purls go to back bed and knits got to front bed
        :param stitchingYarn: the yarn used in the next pass
        :param rowIndex:  the index of the row being prepped for
        """
        row = self._knitGraph.rows[rowIndex]
        for loop in row:
            if loop.yarnId == stitchingYarn:
                parentId = self._getHeldParent(loop)
                parentNeedle = self._loopIdsToNeedle[parentId]
                edgeData = self._knitGraph.getInEdgesByParents(loop.loopId)[parentId]
                if (edgeData["direction"] == "ftb" and parentNeedle.bed != Bed.Back) or (edgeData["direction"] == "btf" and parentNeedle.bed != Bed.Front):
                    self._xferLoop(parentId, parentNeedle.opposite())

    def _knitRow(self, stitchingYarn: int, rowIndex: int, passDirection: MachineDirection):
        """
        Stitch all of the loops in row using the stitching yarn from a specific direction
        :param stitchingYarn: the yarn being used to make stitches
        :param rowIndex: the index of the row being stitched
        :param passDirection: the direction the carrier is expected to pass
        """
        row = self._knitGraph.rows[rowIndex]
        for loop in row:
            if loop.yarnId == stitchingYarn:
                parentId = self._getHeldParent(loop)
                parentNeedle = self._loopIdsToNeedle[parentId]
                self._addStitch(loopId=loop.loopId, needle=parentNeedle, passDirection=passDirection, parentId=parentId)

    def bindOff(self, tagHeight=4, tagWidth=4):
        self._rack(0)
        for f in range(1, self.castOnWidth, 2):
            b = f + 1
            frontFaceNeedle = Needle(f, Bed.Front)
            backFaceNeedle = Needle(b, Bed.Back)
            self._addNode(KnitInstruction("+", frontFaceNeedle, self._getCarrier(0)))  # knit front
            self._addNode(KnitInstruction("+", backFaceNeedle, self._getCarrier(1)))  # knit back
            nextFrontNeedle_loc = f + 2
            nextBackNeedle_loc = b + 2
            if nextFrontNeedle_loc < self.castOnWidth:
                assert nextBackNeedle_loc <= self.castOnWidth
                back_frontFace_Needle = frontFaceNeedle.opposite()
                front_backFace_Needle = backFaceNeedle.opposite()
                self._xfer(frontFaceNeedle, back_frontFace_Needle)  # swap sides
                self._xfer(backFaceNeedle, front_backFace_Needle)
                self._addNode(KnitInstruction("-", back_frontFace_Needle, self._getCarrier(0)))  # knit on back
                self._addNode(KnitInstruction("-", front_backFace_Needle, self._getCarrier(1)))
                self._xfer(back_frontFace_Needle, Needle(nextFrontNeedle_loc, Bed.Front))  # transfer over to next stitch
                self._xfer(front_backFace_Needle, Needle(nextBackNeedle_loc, Bed.Back))

        # make tag to store extra yarn length and prevent pre-slip unravelling
        for i in range(0, tagHeight):
            if i % 2 == 0:
                for w in range(0, tagWidth):
                    self._addNode(KnitInstruction("-", Needle(self.castOnWidth - 1 - w, Bed.Front), self._getCarrier(0)))
                    self._addNode(KnitInstruction("-", Needle(self.castOnWidth - 1 - w, Bed.Front), self._getCarrier(1)))
            else:
                for w in range(tagWidth - 1, -1, -1):
                    self._addNode(KnitInstruction("+", Needle(self.castOnWidth - 1 - w, Bed.Front), self._getCarrier(0)))
                    self._addNode(KnitInstruction("+", Needle(self.castOnWidth - 1 - w, Bed.Front), self._getCarrier(1)))
