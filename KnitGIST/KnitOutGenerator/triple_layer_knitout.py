from typing import List, Dict, Set, Optional

from KnitGIST.ColorWork.LayerPosition import Layer_Position
from KnitGIST.ColorWork.LayeredKnit import LayeredKnit
from KnitGIST.KnitGraph_Construction.Loop import Loop
from KnitGIST.KnitOutGenerator.KnitOutUtilities.MachinePass import MachinePass, Stitch_MachinePass, Xfer_MachinePass
from KnitGIST.KnitOutGenerator.KnitOutUtilities.MachineState import MachineState
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_instructions import XferInstruction, StitchInstruction, \
    KnitInstruction, PurlInstruction, TuckInstruction
from KnitGIST.KnitOutGenerator.KnitOutUtilities.knitout_structures import Needle, MachineSide
from KnitGIST.KnitOutGenerator.graph_to_knitout import MachineDirection, Bed


class Layer_KnitOut_Generator:
    def __init__(self, layeredKnit: LayeredKnit):
        self.layeredKnit: LayeredKnit = layeredKnit
        self.machineState: MachineState = MachineState(self.layeredKnit.needleWidth,
                                                       layer_count=self.layeredKnit.layer_count)
        self.machinePasses: List[MachinePass] = []
        self.lastDirection: MachineDirection = MachineDirection.L2R  # todo: check where cast on starts
        self.lastYarnDirection: MachineDirection = MachineDirection.L2R  # The side the yarn is on,

    def writeInstructions(self, filename):
        with open(filename, "w") as file:
            # preamble
            file.writelines([";!knitout-2\n",
                             ";;Machine: SWG091N2\n",
                             ";;Gauge: 5\n",
                             ";;Carriers: 1 2 3 4 5 6 7 8 9 10\n",
                             ";;Position: Right\n"])
            foundCarriers: Set[int] = set()  # if a carrier is not found, we need to inhook
            hookedCarriers: Set[int] = set()  # if a carrier is hooked we need to release it after this pass
            for machinePass in self.machinePasses:
                if isinstance(machinePass, Stitch_MachinePass) and machinePass.carrier not in foundCarriers:
                    file.writelines([f"inhook {machinePass.carrier}\n"])
                    instructions = machinePass.writtenInstructions()
                    file.writelines(instructions)
                    if isinstance(machinePass.instructions[0], TuckInstruction):
                        hookedCarriers.add(machinePass.carrier)
                    else:  # need an extra pass for rows
                        file.writelines([f"releasehook {machinePass.carrier}\n"])
                    foundCarriers.add(machinePass.carrier)
                elif isinstance(machinePass, Stitch_MachinePass) and machinePass.carrier in hookedCarriers:
                    file.writelines(machinePass.writtenInstructions())
                    file.writelines([f"releasehook {machinePass.carrier}\n"])
                    hookedCarriers.remove(machinePass.carrier)
                else:
                    file.writelines(machinePass.writtenInstructions())
            # out hooks
            file.writelines([f"outhook {self._carrier(yarnId)}\n" for yarnId in self.layeredKnit.yarns])

    def runInstructions(self, bindBottom=False, ststBaseLayers=4, ststTopLayers=4, bindOff=False, boundBindOff=False):
        if bindBottom:
            self._cast_on_bound(ststBorderRows=ststBaseLayers)
        else:
            self._cast_on_unbound(ststBorderRows=ststBaseLayers)
        self.addTexturePasses()
        for _ in range(0, ststTopLayers):
            self.ststPass()
        if bindOff:
            if boundBindOff:
                self._stretchy_BoundBindOff(tag=True)
            else:
                self._stretchy_UnBoundBindOff(tag=True)
                # self._bindOff_unbound(ststTopLayers)

    @staticmethod
    def _carrier(yarnId: int) -> int:
        """
        :param yarnId: the yarnId to convert to a carrier
        :return: carriers by convention start with carrier 3 and go to carrier 4 and 5 for 3 layer knit
        """
        return yarnId + 3

    def _cast_on_bound(self, castOnYarnId: int = 0, ststBorderRows: int = 4):
        """
        Creates a single yarn cast on across all needles. This will close the base layers together
        :param castOnYarnId: the yarn to set the base cast on with
        """
        # tuck on even needles
        # reverse direction and tuck on odd needles
        evenTucks: Dict[TuckInstruction:Loop] = {}
        evenDir = MachineDirection.L2R
        oddDir = evenDir.opposite()
        carrier = self._carrier(castOnYarnId)
        castOnLoops = self.layeredKnit.rows[0]  # ordered from left to right
        oddLoops = {}
        for i, loop in enumerate(castOnLoops):
            needleLoc = self.machineState.needleCount - i  # e.g. 10, 9, 8, 7 ... 1
            if needleLoc % 2 == 0:
                evenTucks[TuckInstruction(evenDir, Needle(needleLoc, Bed.Front), carrier)] = loop
            else:
                oddLoops[loop] = needleLoc

        stitchPass = self.machineState.stitchPass(evenTucks, castOnYarnId, 1)
        self.addPass(stitchPass)
        oddTucks = {}
        for oddLoop in reversed(oddLoops):
            needleLoc = oddLoops[oddLoop]
            oddTucks[TuckInstruction(oddDir, Needle(needleLoc, Bed.Front), carrier)] = oddLoop
        stitchPass = self.machineState.stitchPass(oddTucks, castOnYarnId, 1)
        self.addPass(stitchPass)

        # add 3 lines of knits in binding color
        for direction in [oddDir.opposite(), oddDir,
                          oddDir.opposite()]:  # go the opposite of the last tuck pass, then reverse again
            knits: Dict[KnitInstruction, Loop] = {}
            needles = self.machineState.frontBed.keys()
            if direction == MachineDirection.L2R:
                needles = reversed(needles)
            for needleLoc in needles:
                frontNeedle = Needle(needleLoc, Bed.Front)
                loop = self.machineState.frontBed[needleLoc]
                knit = KnitInstruction(direction, frontNeedle, carrier)
                knits[knit] = loop
            self.addPass(self.machineState.stitchPass(knits, castOnYarnId, 1))

        for yarnId in self.machineState.yarnIdsToSides:
            if yarnId == castOnYarnId:
                assert self.machineState.yarnIdsToSides[yarnId] == MachineSide.Right
            else:
                self.machineState.yarnIdsToSides[yarnId] = MachineSide.Right  # set others to starts on canonical right

        for _ in range(0, ststBorderRows):
            self.ststPass()

    def _cast_on_unbound(self, ststBorderRows: int = 4):
        evenDir = MachineDirection.L2R
        oddDir = evenDir.opposite()
        castOnLoops = self.layeredKnit.rows[0]  # ordered from left to right
        for yarnId in self.getSortedYarns():
            evenTucks: Dict[TuckInstruction:Loop] = {}
            oddLoops: Dict[Loop: int] = {}
            carrier = self._carrier(yarnId)
            isEven_CO = self.layeredKnit.layerWidth % 2 == 0
            for i, loop in enumerate(castOnLoops):
                needleLoc = self.machineState.needleCount - i  # e.g. 10, 9, 8, 7 ... 1
                if self.machineState.needleToYarnId(needleLoc) == yarnId:  # needle that needs current yarn cast on
                    if isEven_CO:
                        evenTucks[TuckInstruction(evenDir, Needle(needleLoc, Bed.Front), carrier)] = loop
                    else:
                        oddLoops[loop] = needleLoc
                    isEven_CO = not isEven_CO
            self.addPass(self.machineState.stitchPass(evenTucks, yarnId, 1))
            oddTucks = {}
            for oddLoop in reversed(oddLoops.keys()):
                needleLoc = oddLoops[oddLoop]
                oddTucks[TuckInstruction(oddDir, Needle(needleLoc, Bed.Front), carrier)] = oddLoop
            self.addPass(self.machineState.stitchPass(oddTucks, yarnId, 1))

            # do 2 stst rows of color to  stabilize loops
            for direction in [oddDir.opposite(), oddDir,
                              oddDir.opposite()]:  # go the opposite of the last tuck pass, then reverse again, then again
                knits: Dict[KnitInstruction, Loop] = {}
                needles = self.machineState.frontBed.keys()
                if direction == MachineDirection.L2R:
                    needles = reversed(needles)
                for needleLoc in needles:
                    if self.machineState.needleToYarnId(needleLoc) == yarnId:
                        frontNeedle = Needle(needleLoc, Bed.Front)
                        loop = self.machineState.frontBed[needleLoc]
                        knit = KnitInstruction(direction, frontNeedle, carrier)
                        knits[knit] = loop
                self.addPass(self.machineState.stitchPass(knits, yarnId, 1))

        for _ in range(0, ststBorderRows):
            self.ststPass()

    def _bindOff_unbound(self, ststBorderRows: int = 0):
        for _ in range(0, ststBorderRows):
            self.ststPass()
        # put all yarns on Left side for left to right bind off
        addReverseRow = False
        for yarnId in self.layeredKnit.yarns:
            if self.machineState.yarnIdsToSides[yarnId] == MachineSide.Right:
                addReverseRow = True
        if addReverseRow:
            self.ststPass()
        # # move everything to front
        front_xfers = []
        for needleLoc in range(1, self.machineState.needleCount + 1):
            front = Needle(needleLoc, Bed.Front)
            if self.machineState[front] is None:
                back = front.opposite()
                front_xfers.append(XferInstruction(back, front))
        self.addPass(self.machineState.xferPass(front_xfers))

        # working from back to front layers bind off from left side
        for yarnId in reversed(self.getSortedYarns()):  # yarns in reverse of layering (starting with back layer yarn)
            # move all loops of this yarn to front
            # move all other loops to back
            # xfers = []
            # for needleLoc in range(1, self.machineState.needleCount + 1):
            #     front = Needle(needleLoc, Bed.Front)
            #     back = front.opposite()
            #     if self.machineState.needleToYarnId(needleLoc) == yarnId:  # put on front
            #         if self.machineState[front] is None:
            #             xfers.append(XferInstruction(back, front))
            #     else:  # put on back
            #         if self.machineState[back] is None:
            #             xfers.append(XferInstruction(front, back))
            # self.addPass(self.machineState.xferPass(xfers))
            carrier = self._carrier(yarnId)
            for needleLoc in range(self.machineState.needleCount, 1, -1):
                if self.machineState.needleToYarnId(needleLoc) == yarnId:  # needle position corresponds to this yarn
                    frontNeedle = Needle(needleLoc, Bed.Front)
                    backNeedle = frontNeedle.opposite()
                    instruction = KnitInstruction(MachineDirection.L2R, frontNeedle, carrier)
                    knit = {instruction: self.machineState.frontBed[needleLoc]}
                    self.addPass(self.machineState.stitchPass(knit, yarnId, 1))  # knit
                    backXfer = XferInstruction(frontNeedle, backNeedle)  # transfer to back
                    self.addPass(self.machineState.xferPass([backXfer]))
                    nextNeedleLoc = needleLoc - self.layeredKnit.layer_count
                    if nextNeedleLoc > 0:
                        frontXfer = XferInstruction(backNeedle, Needle(nextNeedleLoc, Bed.Front))
                        self.addPass(self.machineState.xferPass([frontXfer]))  # xfer to next front stitch of this yarn

    def _stretchy_UnBoundBindOff(self, tag: bool = True):
        boundYarns = set()
        for yarnId in self.getSortedYarns():
            xfers = []
            for needleLoc in self.machineState.frontBed:
                frontNeedle = Needle(needleLoc, Bed.Front)
                backNeedle = frontNeedle.toBack()
                needle_yarnId = self.machineState.needleToYarnId(needleLoc)
                if needle_yarnId == yarnId:  # move all loops of this yarn to front
                    if self.machineState.backBed[needleLoc] is not None:
                        xfers.append(XferInstruction(backNeedle, frontNeedle))
                elif needle_yarnId not in boundYarns:  # move all other unbound yarns to back
                    if self.machineState.frontBed[needleLoc] is not None:
                        xfers.append(XferInstruction(frontNeedle, backNeedle))
            self.addPass(self.machineState.xferPass(xfers))
            startingSide = self.machineState.yarnIdsToSides[yarnId]
            direction = startingSide.nextDirection()
            needleLocs = self.machineState.needleLocIter(direction)

            for i, firstLoc in enumerate(needleLocs):
                if self.machineState.needleToYarnId(firstLoc) == yarnId:
                    isLastStitch = True
                    for secondLoc in needleLocs[i + 1:]:  # jump ahead until we find the next relevant needle
                        loc_yarn = self.machineState.needleToYarnId(secondLoc)
                        if loc_yarn == yarnId:  # found it, bind off and go to next
                            self.bindOfLoops(yarnId, direction, firstLoc, secondLoc)
                            isLastStitch = False
                            break
                    if isLastStitch:
                        # add last stitch with this yarn
                        lastNeedleLoc = firstLoc
                        self.addPass(self.machineState.stitchPass({KnitInstruction(direction.opposite(),
                                                                                   Needle(lastNeedleLoc, Bed.Front),
                                                                                   self._carrier(yarnId)): None}, yarnId, 1))

            if tag:  # catch each yarn on opposite side of knitting to leave a tail for easy pull through
                firstNeedleLoc = needleLocs[0]
                self.addPass(self.machineState.stitchPass({TuckInstruction(direction.opposite(),
                                                                           Needle(firstNeedleLoc + yarnId, Bed.Front),
                                                                           self._carrier(yarnId)): None}, yarnId, 1))

    def _stretchy_BoundBindOff(self, bindOffYarnId: int = 0, tag: bool = True):
        # BO procedure
        # Stitch (keeping to knit purl of prior row) first stitch
        # for remaining needles:
        #   stitch next stitch
        #   transfer prior stitch onto new stitch
        #   stitch through to decrease, knit/purl based on prior stitch status
        # repeat until last stitch then tag stitch for slip knot yarn
        startingSide = self.machineState.yarnIdsToSides[bindOffYarnId]
        direction = startingSide.nextDirection()
        needleLocs = self.machineState.needleLocIter(direction)
        for firstLoc, secondLoc in zip(needleLocs[:-1], needleLocs[1:]):  # iterate over pairs of needle locations
            self.bindOfLoops(bindOffYarnId, direction, firstLoc, secondLoc)

        # add last stitch
        lastNeedleLoc = needleLocs[-1]
        self.addPass(self.machineState.stitchPass({KnitInstruction(direction.opposite(),
                                                                   Needle(lastNeedleLoc, Bed.Front),
                                                                   self._carrier(bindOffYarnId)): None}, bindOffYarnId, 1))

        if tag:  # catch each yarn on opposite side of knitting to leave a tail for easy pull through
            firstNeedleLoc = needleLocs[0]
            for yarnId in self.layeredKnit.yarns:
                self.addPass(self.machineState.stitchPass({TuckInstruction(direction.opposite(),
                                                                           Needle(firstNeedleLoc + yarnId, Bed.Front),
                                                                           self._carrier(yarnId)): None}, yarnId, 1))

    def bindOfLoops(self, yarnId, direction, firstLoc, secondLoc):
        carrier = self._carrier(yarnId)
        firstLoop = self.machineState.frontBed[firstLoc]
        firstNeedle = Needle(firstLoc, Bed.Front)
        if firstLoop is None:
            firstLoop = self.machineState.backBed[firstLoc]
            firstNeedle = Needle(firstLoc, Bed.Back)
        assert firstLoop is not None, f"No loop at {firstLoc}"
        secondLoop = self.machineState.frontBed[secondLoc]
        secondNeedle = Needle(secondLoc, Bed.Front)
        if secondLoop is None:
            secondLoop = self.machineState.backBed[secondLoc]
            secondNeedle = Needle(secondLoc, Bed.Back)
        assert secondLoop is not None, f"No loop at {secondLoc}"
        first_stitchBed = self.machineState.loopToCreationBed[firstLoop]
        second_stitchBed = self.machineState.loopToCreationBed[secondLoop]
        # xfer for knit vs purl
        stitchSide_xfers = []
        if first_stitchBed != firstNeedle.bed:
            stitchSide_xfers.append(XferInstruction(firstNeedle, firstNeedle.opposite()))
        if second_stitchBed != secondNeedle.bed:
            stitchSide_xfers.append(XferInstruction(secondNeedle, secondNeedle.opposite()))
        self.addPass(self.machineState.xferPass(stitchSide_xfers))
        # stitch on first two needles
        firstOnBack = False
        secondOnFront = False
        stitches = {}
        if first_stitchBed == Bed.Front:  # knit first stitch
            stitches[KnitInstruction(direction, firstNeedle.toFront(), carrier)] = firstLoop
        else:  # purl first stitch
            stitches[PurlInstruction(direction, firstNeedle.toBack(), carrier)] = firstLoop
            firstOnBack = True
        if second_stitchBed == Bed.Front:  # knit second stitch
            stitches[KnitInstruction(direction, secondNeedle.toFront(), carrier)] = secondLoop
            secondOnFront = True
        else:  # purl second stitch
            stitches[PurlInstruction(direction, secondNeedle.toBack(), carrier)] = secondLoop
        self.addPass(self.machineState.stitchPass(stitches, yarnId, 1))
        # transfer first stitch onto second stitch
        side_xfers = []
        if not firstOnBack:  # stitch wasn't purled so we need to transfer to back
            side_xfers.append(XferInstruction(firstNeedle.toFront(), firstNeedle.toBack()))
        if not secondOnFront:
            side_xfers.append(XferInstruction(secondNeedle.toBack(), secondNeedle.toFront()))
        self.addPass(self.machineState.xferPass(side_xfers))
        reverse_xfer = XferInstruction(firstNeedle.toBack(), secondNeedle.toFront())
        self.addPass(self.machineState.xferPass([reverse_xfer]))  # put first loop on second loop

    def ststPass(self):
        layer_xfers = []
        for needleLoc in range(1, self.machineState.needleCount + 1):
            frontNeedle = Needle(needleLoc, Bed.Front)
            backNeedle = frontNeedle.opposite()
            if self.machineState.needleMatchesDefaultYarnOfLayer(needleLoc, Layer_Position.Top):  # front layer needle
                if self.machineState.backBed[needleLoc] is not None:  # move to front for knitting
                    layer_xfers.append(XferInstruction(backNeedle, frontNeedle))
            else:
                if self.machineState.frontBed[needleLoc] is not None:  # Transfer out of first layer to back
                    layer_xfers.append(XferInstruction(frontNeedle, backNeedle))
        self.addPass(self.machineState.xferPass(layer_xfers))
        sorted_yarns = self.getSortedYarns()
        for yarnId in sorted_yarns:
            # transfer loops to front
            xfers = []
            for needleLoc in range(1, self.machineState.needleCount + 1):
                needleYarnID = self.machineState.needleToYarnId(needleLoc)
                if needleYarnID == yarnId and self.machineState.frontBed[needleLoc] is None:
                    xfers.append(XferInstruction(Needle(needleLoc, Bed.Back), Needle(needleLoc, Bed.Front)))
            xfer_pass = self.machineState.xferPass(xfers)
            self.addPass(xfer_pass)
            curYarnSide = self.machineState.yarnIdsToSides[yarnId]
            direction = curYarnSide.nextDirection()
            carrier = self._carrier(yarnId)
            knits = {}
            if direction == MachineDirection.R2L:
                needleIter = range(1, self.machineState.needleCount + 1)
            else:
                needleIter = range(self.machineState.needleCount, 0, -1)
            for needleLoc in needleIter:
                if self.machineState.needleToYarnId(needleLoc) == yarnId:
                    loop = self.machineState.frontBed[needleLoc]
                    assert loop is not None, f"No loop at f{needleLoc}"
                    knits[KnitInstruction(direction, Needle(needleLoc, Bed.Front), carrier)] = loop
            stitch_pass = self.machineState.stitchPass(knits, yarnId, 1)
            self.addPass(stitch_pass)

    def getSortedYarns(self):
        return sorted(self.layeredKnit.yarns.keys())

    def addTexturePasses(self):
        for row_index in self.layeredKnit.rows:
            if row_index > 0:
                self._addRow(row_index)

    def _addRow(self, row_index: int):
        loopsToLayer: Dict[Loop, Layer_Position] = {}
        yarnIdToLoops: Dict[int, List[Loop]] = {yarnId: [] for yarnId in range(0, self.layeredKnit.layer_count)}

        textures_RtoL = self.layeredKnit.textures
        layers_RtoL = self.layeredKnit.layering
        if row_index % 2 == 0:  # loop order Left to right
            texture_index = 2
            textures = reversed(textures_RtoL)
            layers = reversed(layers_RtoL)
        else:
            texture_index = 0
            textures = textures_RtoL
            layers = layers_RtoL
        for baseTexture, layer_coloring in zip(textures, layers):
            baseRow = baseTexture.rows[row_index]
            # if row_index % 2 == 0:
            #     baseRow = reversed(baseRow)
            layerRow = layer_coloring.layer[row_index]
            if row_index % 2 == 0:
                layerRow = reversed(layerRow)
            for baseLoop, layerPos in zip(baseRow, layerRow):
                realLoop = self.layeredKnit.getLoopFromBaseTexture(texture_index, baseLoop)
                yarnIdToLoops[realLoop.yarnId].append(realLoop)
                loopsToLayer[realLoop] = layerPos
            if row_index % 2 == 0:
                texture_index -= 1
            else:
                texture_index += 1

        def getNeedle(l: Loop) -> Needle:
            n = self.machineState.getHoldingNeedle(l)
            if n is None:
                n = self.machineState.needleByParent(l, self.layeredKnit)
            assert n is not None, f"No Needle found related to loop {l}"
            return n

        # Do initial positioning in preparation for stitching the layer
        for layer in [Layer_Position.Bot, Layer_Position.Mid]:
            xfers = []
            for loop, loop_layer in loopsToLayer.items():
                needle = getNeedle(loop)
                if loop_layer == layer and needle.bed != Bed.Back:  # move to back if not already there
                    xfers.append(XferInstruction(needle, needle.toBack()))
            self.addPass(self.machineState.xferPass(xfers))

        sorted_yarns = self.getSortedYarns()
        yarnSide = self.machineState.yarnIdsToSides[sorted_yarns[0]]
        if yarnSide == MachineSide.Left:  # starting from left side,
            sorted_yarns = reversed(sorted_yarns)
        for yarn_index, yarnId in enumerate(sorted_yarns):
            assert yarnSide == self.machineState.yarnIdsToSides[yarnId], "Yarns should all start on same side"
            direction = yarnSide.nextDirection()
            carrier = self._carrier(yarnId)
            outer_xfers = []
            inner_xfers = []
            stitch_xfers = []
            undo_stitch_xfers = []
            undo_inner_xfers = []
            undo_outer_xfers = []
            stitches: Dict[StitchInstruction, Loop] = {}
            for loop in yarnIdToLoops[yarnId]:
                loop_layer = loopsToLayer[loop]
                needle = getNeedle(loop)
                other_loops = self.layeredKnit.loopsToLoopLayer[loop].otherLoops(loop)
                for otherLoop in other_loops:
                    otherNeedle = getNeedle(otherLoop)
                    other_layer = loopsToLayer[otherLoop]
                    front_xfer = XferInstruction(otherNeedle.toBack(), otherNeedle.toFront())
                    back_xfer = XferInstruction(otherNeedle.toFront(), otherNeedle.toBack())
                    if other_layer < loop_layer:  # this triplet loop should be in front of this loop
                        if otherNeedle.bed != Bed.Front:
                            if other_layer == Layer_Position.Top:
                                outer_xfers.append(front_xfer)
                                undo_outer_xfers.append(back_xfer)
                            else:
                                assert other_layer == Layer_Position.Mid
                                inner_xfers.append(front_xfer)
                                undo_inner_xfers.append(back_xfer)
                    elif other_layer > loop_layer:  # this triplet loop should be behind this loop
                        if otherNeedle.bed != Bed.Back:
                            if other_layer == Layer_Position.Bot:
                                outer_xfers.append(back_xfer)
                                undo_outer_xfers.append(front_xfer)
                            else:
                                assert other_layer == Layer_Position.Mid
                                inner_xfers.append(back_xfer)
                                undo_inner_xfers.append(front_xfer)
                if self.layeredKnit.loopIsBTF(loop.loopId):  # knit on front bed
                    if needle.bed != Bed.Front:  # needs transfer to front, then back after knitting
                        stitch_xfers.append(XferInstruction(needle.toBack(), needle.toFront()))
                        undo_stitch_xfers.append(XferInstruction(needle.toFront(), needle.toBack()))
                    stitches[KnitInstruction(direction, needle.toFront(), carrier)] = loop
                else:  # purl on back bed
                    if needle.bed != Bed.Back:  # needs transfer to back, then front after knitting
                        stitch_xfers.append(XferInstruction(needle.toFront(), needle.toBack()))
                        undo_stitch_xfers.append(XferInstruction(needle.toBack(), needle.toFront()))
                    stitches[PurlInstruction(direction, needle.toBack(), carrier)] = loop
            self.addPass(self.machineState.xferPass(outer_xfers))  # move outer layers to their position
            self.addPass(self.machineState.xferPass(inner_xfers))  # move mid layer based on the stitched loop
            self.addPass(self.machineState.xferPass(stitch_xfers))  # place loops for stitching
            missPosition = 3 - yarn_index
            self.addPass(self.machineState.stitchPass(stitches, yarnId, missPosition=missPosition))  # make those stitches
            self.addPass(self.machineState.xferPass(undo_stitch_xfers))  # undo the stitching moves
            self.addPass(self.machineState.xferPass(undo_inner_xfers))  # undo the mid layer moves
            self.addPass(self.machineState.xferPass(undo_outer_xfers))  # undo the outer layer moves

    def addPass(self, machinePass: Optional[MachinePass]):
        if machinePass is not None and len(machinePass) > 0:
            # if isinstance(machinePass, Xfer_MachinePass):  # may be some xfers to undo
            #     if len(self.machinePasses) > 0 and isinstance(self.machinePasses[-1], Xfer_MachinePass):
            #         lastPass = self.machinePasses[-1]
            #         keepInstructions = machinePass.undoLastPass(lastPass)
            #         # todo use these to undo the duplicate instructions for machine efficiency
            self.machinePasses.append(machinePass)
