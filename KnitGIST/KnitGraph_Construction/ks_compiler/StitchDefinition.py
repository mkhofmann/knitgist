from enum import Enum


def knitDefinition():
    return StitchDefinition()


def purlDefinition():
    return StitchDefinition(direction="ftb", name="p")


def yoDefinition():
    return StitchDefinition(inLoopsCount=0, direction="none", name="yo")


def slipDecreaseDefinition(sDecToken):
    if sDecToken == "skpo":
        return StitchDefinition(2, name=sDecToken, parentIterOrder=[1, 0])
    elif sDecToken == "sk2po":
        return StitchDefinition(3, name=sDecToken, parentIterOrder=[2, 0, 1])
    else:
        assert sDecToken == "s2kpo"
        return StitchDefinition(3, name=sDecToken, parentIterOrder=[2, 1, 0])


def xTogDecreaseDefinition(togToken):
    togNum = int(togToken[1:2])
    if togToken[0:1] == "k":
        return StitchDefinition(inLoopsCount=togNum, name="kxtog")
    else:
        return StitchDefinition(inLoopsCount=togNum, direction="ftb", name="pxtog")


class StitchDefinition(object):
    def __init__(self, inLoopsCount=1, direction="btf", depth=0, name="k", parentOffSet=0, parentIterOrder=None,
                 additionalParameters=None):
        if additionalParameters is None:
            additionalParameters = {}
        self.parentOffSet = parentOffSet
        self.name = name
        self.direction = direction
        self.inLoopsCount = inLoopsCount
        self.depth = depth
        if parentIterOrder is None:
            parentIterOrder = [*range(0, self.inLoopsCount)]  # list of indices
        self.parentIterOrder = parentIterOrder
        self.additionalParameters = additionalParameters.copy()

    def copy(self):
        definition = StitchDefinition(inLoopsCount=self.inLoopsCount, direction=self.direction, depth=self.depth,
                                      name=self.name, parentOffSet=self.parentOffSet,
                                      parentIterOrder=self.parentIterOrder)
        definition.additionalParameters = self.additionalParameters.copy()
        return definition

    def copyOppositeSideRead(self):
        copy = self.copy()
        copy.flipDirection()
        return copy

    def __eq__(self, other):
        instance = isinstance(other, StitchDefinition)
        count = self.inLoopsCount == other.inLoopsCount
        direction = self.direction == other._direction
        depth = self.depth == other.depth
        off_set = self.parentOffSet == other.parentOffSet
        return instance and count and direction and depth and off_set

    def __str__(self):
        return "{}({}-{}{}->1)^{}".format(self.name, self.inLoopsCount, self.direction, self.depth, self.parentOffSet)

    def __repr__(self):
        if len(self.additionalParameters) > 0:
            return str(self) + str(self.additionalParameters)
        else:
            return str(self)

    def weight(self, weightName="weight", defaultWeight=0):
        if weightName in self.additionalParameters:
            return self.additionalParameters[weightName]
        else:
            return defaultWeight

    def isDecrease(self):
        return self.inLoopsCount > 1

    def flipDirection(self):
        if self.direction == "btf":
            self.direction = "ftb"
        else:
            self.direction = "btf"
        if self.name == "k":
            self.name = "p"
        elif self.name == "p":
            self.name = "k"

    def getInLoopCount(self):
        return self.inLoopsCount

    def leans(self):
        if not self.isDecrease():
            return StitchLean.Center
        elif self.inLoopsCount % 2 == 1:
            return StitchLean.Center
        else:
            inOrder = True
            isReversed = True
            last = self.parentIterOrder[0]
            for loopIndex in self.parentIterOrder[1:]:
                if isReversed and loopIndex > last:
                    isReversed = False
                if inOrder and loopIndex < last:
                    inOrder = False
                if not inOrder and not isReversed:
                    break
                last = loopIndex
            assert not (inOrder and isReversed)
            if not inOrder and not isReversed:
                return StitchLean.Center
            elif inOrder:
                return StitchLean.Right
            else:
                return StitchLean.Left


class StitchLean(Enum):
    Left = "left"
    Right = "right"
    Center = "center"
