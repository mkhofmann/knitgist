# Represents a series of loops
# Allows us to do operations on and get data about the parent row above a specified row
class LoopStack(object):
    def __init__(self, knitGraph, rowIndex, isRow=True):
        self.isRow = isRow

        # Gets the row of loops directly above rowIndex
        self.parentLoops = knitGraph.rows[rowIndex - 1].copy()

        # Determines if rowIndex is at an even index
        # QUESTION: why are we doing this?
        self.isWs = rowIndex % 2 == 0

    # Returns the length of the row above rowIndex (the "parent" row)
    def __len__(self):
        return len(self.parentLoops)

    # Returns the last loop in the parent, only if the parent is a row
    def nextLoop(self):
        if not self.isRow:
            assert False, "Round construction not yet Supported"
        else:
            return self.parentLoops[-1]

    # Removes the last loop from the parent, only if the parent is a row
    def pop(self):
        if not self.isRow:
            assert False, "Round construction not yet Supported"
        else:
            return self.parentLoops.pop()
