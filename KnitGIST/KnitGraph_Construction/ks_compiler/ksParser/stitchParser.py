import os

from parglare import Grammar, Parser

from KnitGIST2.KnitGraph_Construction.ks_compiler.ksParser.KnitSpeak2parser import SymbolTable


class StitchParser(object):
    def __init__(self, debugGrammer=False, debugParser=False, debugParserLayout=False):
        dir = os.path.dirname(__file__)
        pg_loc = dir + "\\stitchDef.pg"
        self.grammer = Grammar.from_file(pg_loc, debug=debugGrammer, ignore_case=True)
        self.parser = Parser(self.grammer, debug=debugParser, debug_layout=debugParserLayout)
        self.parser.symbolTable = SymbolTable()

    def parse(self, pattern, patternIsFile=False):
        if patternIsFile:
            result = self.parser.parse_file(pattern)
        else:
            result = self.parser.parse(pattern)
        return result


class NumParser(object):
    def __init__(self, debugGrammer=False, debugParser=False, debugParserLayout=False):
        dir = os.path.dirname(__file__)
        pg_loc = dir + "\\numExpression.pg"
        self.grammer = Grammar.from_file(pg_loc, debug=debugGrammer, ignore_case=True)
        self.parser = Parser(self.grammer, debug=debugParser, debug_layout=debugParserLayout)
        self.parser.symbolTable = SymbolTable()


    def parse(self, pattern):
        result = self.parser.parse(pattern)
        return result
