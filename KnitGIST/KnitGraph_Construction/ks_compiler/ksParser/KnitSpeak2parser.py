import os

from parglare import Grammar, Parser
from typing import List, Dict, Any

from KnitGIST.KnitGraph_Construction.ks_compiler.CableDefinition import cableDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import *


def basicStitchTable(maxTogs=3, maxCables=6):
    table = {"k": knitDefinition(), "p": purlDefinition(), "yo": yoDefinition(), "skpo": slipDecreaseDefinition("skpo"),
             "sk2po": slipDecreaseDefinition("sk2po"), "s2kpo": slipDecreaseDefinition("s2kpo")}
    for i in range(2, maxTogs + 1):
        table["k{}tog".format(i)] = xTogDecreaseDefinition("k{}tog".format(i))
        table["p{}tog".format(i)] = xTogDecreaseDefinition("p{}tog".format(i))
    lr = ["l", "r"]
    for lean in lr:
        phrase = "t2{}".format(lean)
        table[phrase] = cableDefinition(phrase)
        phrase = "t2{}p".format(lean)
        table[phrase] = cableDefinition(phrase)
    fb = ["f", "b"]
    for i in range(2, maxCables + 1):
        for lean in fb:
            phrase = "c{}{}".format(i, lean)
            table[phrase] = cableDefinition(phrase)
            phrase = "c{}{}p".format(i, lean)
            table[phrase] = cableDefinition(phrase)
    return table


class KnitSpeak2Parser(object):
    def __init__(self, debugGrammer: bool=False, debugParser: bool=False, debugParserLayout:bool=False, numTable=None,
                 stitchDefTable=None):
        directory = os.path.dirname(__file__)
        pg_loc = directory + "\\knitspeak.pg"
        self._grammer = Grammar.from_file(pg_loc, debug=debugGrammer, ignore_case=True)
        self.parser = Parser(self._grammer, debug=debugParser, debug_layout=debugParserLayout)
        self.parser.symbolTable = SymbolTable(numTable, stitchDefTable)

    def parse(self, pattern: str, patternIsFile: bool = False) -> List[Dict[str, Any]]:
        if patternIsFile:
            result = self.parser.parse_file(pattern)
        else:
            result = self.parser.parse(pattern)
        return result


class SymbolTable(object):
    def __init__(self, numTable=None, stitchDefTable=None, keepBasicStitches=True):
        if stitchDefTable is None:
            stitchDefTable = {}
        if keepBasicStitches:
            basics = basicStitchTable()
            basics.update(stitchDefTable)
            stitchDefTable = basics
        if numTable is None:
            numTable = {}
        for key in numTable:
            for stitchKey in numTable:
                assert key != stitchKey, "Shadowed stitch={} and numerical identfiers".format(key)
        self.numTable = numTable
        self.stitchDefTable = stitchDefTable
        self.symbolTable = {}
        for key in self.numTable:
            self.symbolTable[key] = self.numTable[key]
        for key in self.stitchDefTable:
            self.stitchDefTable[key] = self.stitchDefTable[key]

    def hasSymbol(self, symbol):
        return symbol in self.symbolTable

    def hasNumSymbol(self, symbol):
        return symbol in self.numTable

    def hasStitchSymbol(self, symbol):
        return symbol in self.stitchDefTable

    def addNumSymbol(self, symbol, data):
        self.symbolTable[symbol] = data
        if self.hasStitchSymbol(symbol):
            del self.stitchDefTable[symbol]
        self.numTable[symbol] = data

    def addStitchSymbol(self, symbol, data):
        self.symbolTable[symbol] = data
        if self.hasNumSymbol(symbol):
            del self.numTable[symbol]
        self.stitchDefTable[symbol] = data
