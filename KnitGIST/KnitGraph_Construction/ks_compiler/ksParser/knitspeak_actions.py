from enum import Enum

from parglare import get_collector

from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import StitchDefinition, knitDefinition, purlDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.ksParser.numExpression_actions import stripToDigits

action = get_collector()


@action
def courseIdDefinitions(_, nodes):
    assert "ow" in nodes[1], "Only rows are accepted now"
    return nodes[0]


@action
def courseStatement(_, nodes):
    if nodes[0] is not None:
        newStitchDefs = flipStitchList(nodes[2])
        return {"courseIds": nodes[1], "stitch-operations": newStitchDefs}
    return {"courseIds": nodes[1], "stitch-operations": nodes[2]}


def flipStitchList(opTupleList):
    newStitchDefs = []
    for operation in opTupleList:
        stDef = operation[0]
        if type(stDef) is list:
            newStitchDefs.append((flipStitchList(stDef), operation[1]))
        else:
            stDef = stDef.copy()
            stDef.flipDirection()
            newStitchDefs.append((stDef, operation[1]))
    return newStitchDefs


@action
def side(_, nodes):
    sideToLower = nodes
    if sideToLower[0:1] == "(":
        sideToLower = sideToLower[1:]
    if sideToLower[-1:] == ")":
        sideToLower = sideToLower[:-1]
    return sideToLower.lower()


@action
def courseIDList(_, nodes):
    if len(nodes) == 1:
        if type(nodes[0]) is int:
            courseIdlist = [nodes[0]]
        else:
            courseIdlist = nodes[0]
    else:
        courseIdlist = nodes[0]
        courseIdlist.append(nodes[2])
        topList = []
        for cId in courseIdlist:
            if type(cId) is int:
                topList.append(cId)
            else:
                for subId in cId:
                    topList.append(subId)
        courseIdlist = topList
    courseSet = set()
    uniqueSortedCIds = []
    for cId in sorted(courseIdlist):
        if cId not in courseSet:
            uniqueSortedCIds.append(cId)
            courseSet.add(cId)
    return uniqueSortedCIds


@action
def stitch_statement_List(_, nodes):
    if len(nodes) == 1:
        if type(nodes[0]) is list:
            return nodes[0]
        else:
            return [nodes[0]]
    else:
        stitchList = nodes[0]
        if type(nodes[2]) is list:
            stitchList.extend(nodes[2])
        else:
            stitchList.append(nodes[2])
        topList = []
        for stitch in stitchList:
            if type(stitch) is list:
                for subStitch in stitch:
                    topList.append(subStitch)
            else:
                topList.append(stitch)
        return stitchList


@action
def repeated_Stitch(_, nodes):
    if nodes[1] is None:
        nodes[1] = 1
    return nodes[0], nodes[1]


@action
def repeated_stitch_group(_, nodes):
    if nodes[1] is None:
        nodes[1] = 1
    return nodes[0], nodes[1]


@action
def static_stitch_group(_, nodes):
    if nodes[3] is None:
        nodes[3] = 1
    return nodes[1], nodes[3]


@action
def conditional_stitch_group(_, nodes):
    return nodes[1], nodes[3]


@action
def between_courses(_, nodes):
    courseIds = [*range(nodes[2], nodes[4] + 1)]
    if nodes[1] is not None:
        sideIds = []
        if nodes[1] == "ws":
            modifier = 0
        else:
            modifier = 1
        for cId in courseIds:
            if cId % 2 == modifier:
                sideIds.append(cId)
        courseIds = sideIds
    return courseIds


class RepCondition(Enum):
    End = "to end",
    Last = "to last",
    LastN = "to last N"

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


@action
def rep_condition(_, nodes):
    if len(nodes) == 2:
        assert nodes[1] == "end"
        return RepCondition.End, 0
    elif len(nodes) == 3:
        assert nodes[2] == "st"
        return RepCondition.Last, 1
    else:
        assert len(nodes) == 4 and nodes[3] == "sts"
        return RepCondition.LastN, nodes[2]


@action
def rowOrRound(_, nodes):
    if "round" in nodes:
        assert False, "Rounds not yet supported in KS2.0"
    return nodes


# @action
# def cableAssignment(context, nodes):
#     stitchDefs = []
#     cableName = nodes[0]
#     for i, defDict in enumerate(nodes[2]):
#         newDef = StitchDefinition(inLoopsCount=defDict["inLoops"], direction=defDict["direction"],
#                                   depth=defDict["depth"], name="{}_{}".format(cableName, i),
#                                   parentOffSet=defDict["shift"])
#         stitchDefs.append((newDef, 1))
#     currentSymbolTable = context.parser.symbolTable
#     currentSymbolTable.addStitchSymbol(cableName, stitchDefs)
#     return stitchDefs


@action
def stitchId(context, nodes):
    currentSymbolTable = context.parser.symbolTable
    assert currentSymbolTable.hasStitchSymbol(nodes[0]), "No stitch defined ID={}".format(nodes[0])
    return currentSymbolTable.stitchDefTable[nodes[0]]


# @action
# def stitchAssignment(context, nodes):
#     symbol = nodes[0]
#     data = StitchDefinition(inLoopsCount=nodes[2]["inLoops"], direction=nodes[2]["direction"], depth=nodes[2]["depth"],
#                             name=nodes[0], parentOffSet=nodes[2]["shift"])
#     currentSymbolTable = context.parser.symbolTable
#     currentSymbolTable.addStitchSymbol(symbol, data)
#     return data


@action
def loopOrder(_, nodes):
    if len(nodes) == 1:
        return nodes
    else:
        order = nodes[0]
        assert len(order) < 4, "Can only order up to 4 loops"
        nextLoopIter = nodes[2]
        if type(nextLoopIter) is list:  # iterations:
            order.extend(nextLoopIter)
        else:
            order.append(nextLoopIter)
        found = set()
        for loop in order:
            if loop in found:
                assert "{} can't be connected more than once".format(loop)
            found.add(loop)
        for i in range(1, len(order)):
            assert i in found, "loop {} was not found".format(i)
        return order, len(order)


# @action
# def cableDef(_, nodes):
#     return nodes[2]


@action
def loopIter(_, nodes):
    if nodes[0] < nodes[2]:
        increment = 1
    else:
        increment = -1
    value = [*range(nodes[0], nodes[2] + increment, increment)]
    return value


# @action
# def stitchDef(_, nodes):
#     if type(nodes[0]) is list and len(nodes[0]) == 1:
#         nodes[0] = nodes[0][0]
#     if nodes[2] is None:
#         nodes[2] = 0
#     if nodes[3] is None:
#         nodes[3] = 0
#     return {"inLoops": nodes[0], "direction": nodes[1], "depth": nodes[2], "shift": nodes[3]}


@action
def depth(_, nodes):
    if nodes[1] is None:
        return nodes[2]
    else:
        return -1 * nodes[2]


# nums
@action
def numDefinition(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addNumSymbol(symbol, data)
    return data


@action
def numId(context, nodes):
    symbol = nodes[0]
    currentSymbolTable = context.parser.symbolTable
    assert currentSymbolTable.hasNumSymbol(symbol), "No numerical expression is associated with the ID={}".format(
        symbol)
    return currentSymbolTable.numTable[symbol]


@action
def numericalExpression(_, nodes):
    assert nodes[0] >= 0, "Non Negative Numbers:{}".format(nodes[0])
    return nodes[0]


@action
def numOperation(_, nodes):
    firstIndex = 0
    secondIndex = 2
    opIndex = 1
    if nodes[0] == "(":
        firstIndex += 1
        secondIndex += 1
        opIndex += 1
    assert type(nodes[firstIndex]) is int, "{} does not evaluate to number".format(nodes[firstIndex])
    assert type(nodes[secondIndex]) is int, "{} does not evaluate to number".format(nodes[opIndex])
    if nodes[opIndex] == "+":
        return nodes[firstIndex] + nodes[secondIndex]
    elif nodes[opIndex] == "-":
        return nodes[firstIndex] - nodes[secondIndex]
    elif nodes[opIndex] == "*":
        return nodes[firstIndex] * nodes[secondIndex]
    elif nodes[opIndex] == "/":
        return nodes[firstIndex] / nodes[secondIndex]


@action
def integer(_, node):
    string = stripToDigits(node)
    i = int(string)
    return i


@action
def opId(context, nodes):
    currentSymbolTable = context.parser.symbolTable
    assert currentSymbolTable.hasStitchSymbol(nodes[0]), "No stitch defined ID={}".format(nodes[0])
    return currentSymbolTable.stitchDefTable[nodes[0]]


@action
def cableAssignment(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    for stDef in data["cable"]:
        stDef.name += "_in_" + symbol
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addStitchSymbol(symbol, data)
    return data


@action
def stitchAssignment(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    data.name = symbol
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addStitchSymbol(symbol, data)
    return data


@action
def stitchDef(_, nodes):
    opList = nodes[2]
    curLoop = 0
    slipStack = []
    passedStack = []
    orienation = None
    for operation in opList:
        if orienation is None and operation["dir"] != "slip":
            orienation = operation["dir"]
        elif operation["dir"] != "slip":
            assert orienation == operation["dir"], "Orientation across stitch does not match"
        maxOpLoop = -1
        for opLoop in operation["order"]:
            actualLoop = curLoop + opLoop
            if operation["dir"] != "slip":
                passedStack.append(actualLoop)
            else:
                slipStack.append(actualLoop)
            maxOpLoop = max(opLoop, maxOpLoop)
        curLoop = maxOpLoop + curLoop + 1
    while len(slipStack) > 0:
        passedStack.append(slipStack.pop())
    if orienation is None:
        orienation = "btf"
    assert curLoop <= 4, "Decreases greater than 4 are not allowed"
    definition = StitchDefinition(inLoopsCount=curLoop, direction=orienation, name="customDec",
                                  parentIterOrder=passedStack)
    return definition


@action
def op(_, nodes):
    return nodes[0]


@action
def slipOp(_, nodes):
    if nodes[1] is None:
        cnt = 1
    else:
        cnt = nodes[1]
    return {"dir": "slip", "order": makeOrderList(cnt, True)}


@action
def knitOp(_, nodes):
    if nodes[1] is None:
        cnt = 1
    else:
        cnt = nodes[1]
    return {"dir": "btf", "order": makeOrderList(cnt)}


@action
def purlOp(_, nodes):
    if nodes[1] is None:
        cnt = 1
    else:
        cnt = nodes[1]
    return {"dir": "ftb", "order": makeOrderList(cnt)}


def makeOrderList(cnt, forward=False):
    if forward:
        return list(range(0, cnt, 1))
    else:
        return list(range(cnt - 1, -1, -1))


@action
def count(_, nodes):
    return nodes[1]


@action
def cableStitch(_, nodes):
    if "k" in nodes[0]:
        return knitDefinition()
    else:
        return purlDefinition()


@action
def cableDef(_, nodes):
    mainNeedle = []
    mainNeedleCount = 0
    frontCount = 0
    backCount = 0
    for operation in nodes[2]:
        if "count" in operation:
            if operation["needle"] == "front":
                frontCount += operation["count"]
            elif operation["needle"] == "back":
                backCount += operation["count"]
        elif operation["needle"] is None:
            for stDef in operation["stitches"]:
                mainNeedleCount += stDef.inLoopsCount
                copyDef = stDef.copy()
                copyDef.parentOffSet = (frontCount + backCount)
                mainNeedle.append(copyDef)
        else:
            addedMainCount = 0
            inserts = []
            for stDef in operation["stitches"]:
                copyDef = stDef.copy()
                if operation["needle"] == "front":
                    copyDef.depth = 1
                elif operation["needle"] == "back":
                    copyDef.depth = -1
                copyDef.parentOffSet = -1 * mainNeedleCount
                inserts.append(copyDef)
                addedMainCount += stDef.inLoopsCount
            if operation["needle"] == "front":
                assert addedMainCount <= frontCount, "Not enough loops heald to front"
                frontCount -= addedMainCount
            elif operation["needle"] == "back":
                assert addedMainCount <= backCount, "Not enough loops heald to back"
                backCount -= addedMainCount
            mainNeedleCount += addedMainCount
            for insertOp in reversed(inserts):
                mainNeedle.insert(0, insertOp)

    return {"cable": list(reversed(mainNeedle)), "size": mainNeedleCount}


@action
def holdOp(_, nodes):
    return {"count": nodes[1], "needle": nodes[2]}
    pass


@action
def runOp(_, nodes):
    stitchList = nodes[0]
    return {"stitches": stitchList, "needle": nodes[1]}


@action
def fromNeedle(_, nodes):
    return nodes[1]
