from parglare import get_collector
action = get_collector()


@action
def lessThanFour(_, nodes):
    assert nodes[0] < 4, "{} > 4: Cannot shift loops this far".format(nodes[0])
    return nodes[0]


@action
def numDefinition(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addNumSymbol(symbol, data)
    return data


@action
def numId(context, nodes):
    symbol = nodes[0]
    currentSymbolTable = context.parser.symbolTable
    assert currentSymbolTable.hasNumSymbol(symbol), "No numerical expression is associated with the ID={}".format(
        symbol)
    return currentSymbolTable.numTable[symbol]


@action
def numericalExpression(_, nodes):
    if len(nodes) == 3:  # calculation:
        assert type(nodes[0]) is int, "{} does not evaluate to number".format(nodes[0])
        assert type(nodes[2]) is int, "{} does not evaluate to number".format(nodes[2])
        if nodes[1] == "+":
            return nodes[0] + nodes[2]
        elif nodes[1] == "-":
            return nodes[0] - nodes[2]
        elif nodes[1] == "*":
            return nodes[0] * nodes[2]
        elif nodes[1] == "/":
            return nodes[0] / nodes[2]
    else:
        return nodes[0]


@action
def integer(_, node):
    string = stripToDigits(node)
    i = int(string)
    return i


def stripToDigits(text):
    digits = ""
    for c in text:
        if c.isdigit():
            digits += c
    return digits
