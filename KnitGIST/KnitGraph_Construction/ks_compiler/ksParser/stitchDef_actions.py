from parglare import get_collector

from KnitGIST.KnitGraph_Construction.ks_compiler.ksParser.knitspeak_actions import StitchDefinition

action = get_collector()


@action
def opId(context, nodes):
    currentSymbolTable = context.parser.symbolTable
    assert currentSymbolTable.hasStitchSymbol(nodes[0]), "No stitch defined ID={}".format(nodes[0])
    return currentSymbolTable.stitchDefTable[nodes[0]]


@action
def cableAssignment(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    for stDef in data:
        stDef.name += "_in_" + symbol
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addStitchSymbol(symbol, data)
    return data


@action
def stitchAssignment(context, nodes):
    symbol = nodes[0]
    data = nodes[2]
    data.name = symbol
    currentSymbolTable = context.parser.symbolTable
    currentSymbolTable.addStitchSymbol(symbol, data)
    return data


@action
def stitchDef(_, nodes):
    opList = nodes[2]
    curLoop = 0
    slipStack = []
    passedStack = []
    orienation = None
    for op in opList:
        if orienation is None and op["dir"] != "slip":
            orienation = op["dir"]
        elif op["dir"] != "slip":
            assert orienation == op["dir"], "Orientation across stitch does not match"
        maxOpLoop = -1
        for opLoop in op["order"]:
            actualLoop = curLoop + opLoop
            if op["dir"] != "slip":
                passedStack.append(actualLoop)
            else:
                slipStack.append(actualLoop)
            maxOpLoop = max(opLoop, maxOpLoop)
        curLoop = maxOpLoop + curLoop + 1
    while len(slipStack) > 0:
        passedStack.append(slipStack.pop())
    if orienation is None:
        orienation = "btf"
    assert curLoop <= 4, "Decreases greater than 4 are not allowed"
    definition = StitchDefinition(inLoopsCount=curLoop, direction=orienation, name="customDec",
                                  parentIterOrder=passedStack)
    return definition


@action
def op(_, nodes):
    return nodes[0]


@action
def slipOp(_, nodes):
    if nodes[1] is None:
        count = 1
    else:
        count = nodes[1]
    return {"dir": "slip", "order": makeOrderList(count, True)}


@action
def knitOp(_, nodes):
    if nodes[1] is None:
        count = 1
    else:
        count = nodes[1]
    return {"dir": "btf", "order": makeOrderList(count)}


@action
def purlOp(_, nodes):
    if nodes[1] is None:
        count = 1
    else:
        count = nodes[1]
    return {"dir": "ftb", "order": makeOrderList(count)}


def makeOrderList(count, forward=False):
    if forward:
        return list(range(0, count, 1))
    else:
        return list(range(count - 1, -1, -1))


@action
def count(_, nodes):
    return nodes[1]


@action
def numericalExpression(_, nodes):
    return int(nodes[0])


@action
def cableDef(_, nodes):
    mainNeedle = []
    mainNeedleCount = 0
    frontCount = 0
    backCount = 0
    for op in nodes[2]:
        if "count" in op:
            if op["needle"] == "front":
                frontCount += op["count"]
            elif op["needle"] == "back":
                backCount += op["count"]
        elif op["needle"] is None:
            for stDef in op["stitches"]:
                mainNeedleCount += stDef.inLoopsCount
                copyDef = stDef.copy()
                copyDef.parentOffSet = -1 * (frontCount + backCount)
                mainNeedle.append(copyDef)
        else:
            addedMainCount = 0
            inserts = []
            for stDef in op["stitches"]:
                copyDef = stDef.copy()
                if op["needle"] == "front":
                    copyDef.depth = 1
                elif op["needle"] == "back":
                    copyDef.depth = -1
                copyDef.parentOffSet = mainNeedleCount
                inserts.append(copyDef)
                addedMainCount += stDef.inLoopsCount
            if op["needle"] == "front":
                assert addedMainCount <= frontCount, "Not enough loops heald to front"
                frontCount -= addedMainCount
            elif op["needle"] == "back":
                assert addedMainCount <= backCount, "Not enough loops heald to back"
                backCount -= addedMainCount
            mainNeedleCount += addedMainCount
            for insertOp in reversed(inserts):
                mainNeedle.insert(0, insertOp)
    return mainNeedle


@action
def holdOp(_, nodes):
    return {"count": nodes[1], "needle": nodes[2]}
    pass


@action
def runOp(_, nodes):
    stitchList = nodes[0]
    return {"stitches": stitchList, "needle": nodes[1]}


@action
def fromNeedle(_, nodes):
    return nodes[1]
