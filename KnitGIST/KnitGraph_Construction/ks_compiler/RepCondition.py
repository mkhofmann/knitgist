from typing import Optional

from KnitGIST.KnitGraph_Construction.ks_compiler.ksParser.knitspeak_actions import RepCondition


class RepConditionResult:

    def __init__(self, reps: int, condition: Optional[RepCondition] = None):
        self.reps: int = reps
        self.condition: Optional[RepCondition] = condition
