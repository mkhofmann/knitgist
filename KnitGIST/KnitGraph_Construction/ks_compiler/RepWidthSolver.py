from typing import Union, Optional, Any, List, Tuple

from pulp import LpProblem, LpVariable, lpSum, LpMinimize

from KnitGIST.KnitGraph_Construction.ks_compiler.CableDefinition import CableDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.RepCondition import RepConditionResult
from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import StitchDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.ksParser.knitspeak_actions import RepCondition


def convertOpsToConditional(ops: list, closingReps: Optional[list] = None) -> List[Tuple[Any, RepConditionResult]]:
    if closingReps is None:
        for op in ops:
            if type(op[1]) is tuple:  # conditional
                return ops  # with no changes
        return [(ops, RepConditionResult(0, RepCondition.End))]
    else:
        assert type(closingReps) is list
        closingWidth = operationWidth(closingReps)
        newOpsList = []
        hasConditionals = False
        for i, op in enumerate(ops):
            repCondition: RepConditionResult = op[1]
            if isinstance(op, StitchDefinition) or isinstance(op, CableDefinition):
                op = (op, 1)
                ops[i] = op
            if repCondition.condition is not None:  # conditional
                newOp = op[0], RepConditionResult(repCondition.reps + closingWidth, RepCondition.LastN)
                hasConditionals = True
            else:
                newOp = op
            newOpsList.append(newOp)
        if not hasConditionals:
            newOpsList = [(newOpsList, RepConditionResult(closingWidth, RepCondition.LastN))]
        newOpsList.extend(closingReps)
        return newOpsList


def operationWidth(operation: Union[list, tuple, CableDefinition, dict, StitchDefinition]) -> int:
    if type(operation) is list:
        width = 0
        for op in operation:
            width += operationWidth(op)
        return width
    elif type(operation) is tuple:
        width = operationWidth(operation[0])
        staticReps = operation[1]
        if staticReps.condition is not None:  # conditional rep
            staticReps = 1
        else:
            staticReps = staticReps.reps
        return width * staticReps
    elif isinstance(operation, CableDefinition):
        return operation.size
    elif type(operation) is dict and "cable" in operation:
        return operation["size"]
    else:
        assert isinstance(operation, StitchDefinition)
        return operation.inLoopsCount


def repWidthSolver(operations: List[Tuple[Any, RepConditionResult]], maxWidth: Optional[int] = None, minWidth: Optional[int] = None, target: int = LpMinimize):
    repIds = []
    opWidths = {}
    staticOps = set()
    conditionalOps = set()
    repIdsToZeros = {}

    def maskRepWeights(*repIdentifiers, **kwargs):
        copyDict = repIdsToZeros.copy()
        for repIdentifier in repIdentifiers:
            if "widthWeights" in kwargs:
                weight = opWidths[repIdentifier]
            else:
                weight = 1
            copyDict[repIdentifier] = weight
        return copyDict

    for i, op in enumerate(operations):
        repId = str(i)
        repIds.append(repId)
        repIdsToZeros[repId] = 0
        opWidths[repId] = operationWidth(op)
        assert type(op) is tuple
        if op[1].condition is not None:
            conditionalOps.add(repId)
        else:
            staticOps.add(repId)

    if len(conditionalOps) == 0:
        return repWidthSolver(convertOpsToConditional(operations), maxWidth, minWidth, target)  # add a to end operation

    problem = LpProblem("repWidthSolver", target)
    repVars = LpVariable.dicts("Rep", repIds, lowBound=1.0,
                               cat="Integer")  # reps must be 0 or greater integers on each operation
    problem += lpSum([opWidths[repStr] * repVars[repStr] for repStr in repIds])

    if maxWidth is not None:
        problem += lpSum([opWidths[repStr] * repVars[repStr] for repStr in repIds]) <= maxWidth, "MaxWidthCondition"
    if minWidth is not None:
        problem += lpSum([opWidths[repStr] * repVars[repStr] for repStr in repIds]) >= minWidth, "MinWidthCondition"

    # static op constraints
    conditionalWeights = maskRepWeights(*staticOps)  # static op weights set to 1
    problem += lpSum([conditionalWeights[repStr] * repVars[repStr] for repStr in repIds]) == len(
        staticOps), "Singular_Static_Reps"
    # conditional Rep constraints
    trailingOpIds = []
    for i, op in enumerate(reversed(operations)):
        opIndex = (len(operations) - 1) - i
        repId = str(opIndex)
        if repId in conditionalOps:
            remainder = op[1].reps
            weights = maskRepWeights(*trailingOpIds, widthWeights=True)
            problem += lpSum(
                [weights[repStr] * repVars[repStr] for repStr in repIds]) == remainder, "EndCondition_{}".format(repId)
        trailingOpIds.insert(0, repId)

    problem.solve()
    # print("Status:", LpStatus[problem.status])
    if problem.status == -1:
        return -1
    finalWidth = 0
    # noinspection PyProtectedMember
    for v in problem._variables:
        repId = v.name.split("_")[1]
        finalWidth += v.varValue * opWidths[repId]
        # print("{}->{}".format(v.name, v.varValue))
    return finalWidth
