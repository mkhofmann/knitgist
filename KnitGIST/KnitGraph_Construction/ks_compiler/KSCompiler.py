from pulp import LpMaximize
from sortedcontainers import SortedDict
from typing import List, Tuple, Union

from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph
from KnitGIST.KnitGraph_Construction.ks_compiler.RepCondition import RepConditionResult
from KnitGIST.KnitGraph_Construction.ks_compiler.RepWidthSolver import repWidthSolver
from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import StitchDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.ksParser.KnitSpeak2parser import KnitSpeak2Parser


def deepCopyOperations(operations: List[Tuple[Union[StitchDefinition, list], Union[int, tuple]]]) -> List[Tuple[StitchDefinition, RepConditionResult]]:
    copy = []
    for opTuple in operations:
        op = opTuple[0]
        if type(op) is list:
            copyOp = deepCopyOperations(op)
        else:
            copyOp = op.copy()
        rep = opTuple[1]
        if type(rep) == int:
            updatedRep = RepConditionResult(reps=rep)
        else:
            assert type(rep) == tuple
            updatedRep = RepConditionResult(reps=rep[1], condition=rep[0])
        copy.append((copyOp, updatedRep))
    return copy


def buildGraph(courseIdsToInstructions: SortedDict, targetWidth=None, height=None):
    if targetWidth is None:
        firstWidth = repWidthSolver(courseIdsToInstructions[1])
    elif type(targetWidth) is tuple:
        firstWidth = repWidthSolver(courseIdsToInstructions[1], targetWidth[0], targetWidth[1])
    else:
        firstWidth = repWidthSolver(courseIdsToInstructions[1], targetWidth, target=LpMaximize)
    firstWidth = int(firstWidth)
    if height is None:
        height = len(courseIdsToInstructions)
    knitGraph = KnitGraph()
    knitGraph.castOnRow(firstWidth)
    lastWidth = firstWidth
    if len(courseIdsToInstructions.keys()) == 1:
        maxId = [*courseIdsToInstructions.keys()][0]
    else:
        maxId = max(*courseIdsToInstructions.keys())
    while knitGraph.maxRow < height:
        for i in range(1, maxId + 1):
            operations = courseIdsToInstructions[i]
            nextWidth = repWidthSolver(operations, lastWidth, lastWidth)
            assert nextWidth == lastWidth, "Cannot match row width at row {}".format(i)
            lastWidth = knitGraph.addRowByKs2Instructions(operations)
            if knitGraph.maxRow == height:
                break
    return knitGraph


class KSCompiler:

    def __init__(self, pattern: str, patternIsFile: bool = False, numTable=None, stitchTable=None):
        self.patternIsFile: bool = patternIsFile
        self.pattern: str = pattern
        self.parser: KnitSpeak2Parser = KnitSpeak2Parser(debugGrammer=False, debugParser=False, numTable=numTable,
                                                         stitchDefTable=stitchTable)
        self._parseResult = self.parser.parse(self.pattern, self.patternIsFile)
        self._courseIdsToInstructions: SortedDict = SortedDict()
        for courseDef in self._parseResult:
            courseIds = courseDef["courseIds"]
            operations = courseDef["stitch-operations"]
            for cId in courseIds:
                assert cId not in self._courseIdsToInstructions, "Course {} is defined more than once".format(cId)
                self._courseIdsToInstructions[cId] = deepCopyOperations(operations)
        courseKeys = [*self._courseIdsToInstructions.keys()]
        if len(courseKeys) == 1:
            maxId = 1
        else:
            maxId = max(*courseKeys)
        for i in range(1, maxId + 1):
            assert i in self._courseIdsToInstructions, "course {} is not defined".format(i)

    def buildGraph(self, width=None, height=None):
        return buildGraph(self._courseIdsToInstructions, width, height)

    def stitchGrid(self, widthInStitches, heightInStitches):
        stitchGrid = {}
        gridRow = 1
        while gridRow <= heightInStitches:
            for patternRow in self._courseIdsToInstructions:
                stitches = []
                while len(stitches) < widthInStitches:
                    for operation in self._courseIdsToInstructions[patternRow]:
                        assert type(operation) is tuple
                        op = operation[0]
                        reps = operation[1]
                        if type(reps) is tuple:  # conditionalEnding
                            while widthInStitches - len(stitches) > reps[1]:
                                stitches = self.addOperationToStitchGrid(stitches, op, 1, ws=gridRow % 2 == 0)
                        else:
                            stitches = self.addOperationToStitchGrid(stitches, op, reps, ws=gridRow % 2 == 0)
                stitchesAsDict = {}
                for i, stitch in enumerate(stitches):
                    stitchesAsDict[i] = stitch
                stitchGrid[gridRow] = stitchesAsDict
                gridRow += 1
        return stitchGrid

    def addOperationToStitchGrid(self, stitches, operation, reps, ws=False):
        if isinstance(operation, StitchDefinition):
            for i in range(0, reps):
                if ws:
                    stitches.insert(0, operation)
                else:
                    stitches.append(operation)
            return stitches
        elif type(operation) is list:
            for i in range(0, reps):
                for op in operation:
                    stitches = self.addOperationToStitchGrid(stitches, op[0], op[1], ws)
            return stitches
        else:
            assert False
