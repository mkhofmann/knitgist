from typing import List

from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import StitchDefinition


class CableDefinition:
    def __init__(self, size: int, isPurl: bool, isFront: bool):
        self.size: int = size
        self._isPurl: bool = isPurl
        self._isFront: bool = isFront
        self._front: int = int(self.size / 2) + int(self.size) % 2  # majority in front for clean looking cables
        self._back: int = self.size - self._front
        if self._isFront:
            self._hold: int = self._front  # hold the front set
            self._run: int = self._back  # hold the back set
        else:
            self._hold: int = self._back  # hold the back set
            self._run: int = self._front  # run the front set

    def copy(self):
        newCable = CableDefinition(self.size, self._isPurl, self._isFront)
        return newCable

    def __str__(self):
        return "cable({} to front={}, purl={})".format(self.size, self._isFront, self._isPurl)

    def __repr__(self):
        return str(self)

    def getInLoopCount(self) -> int:
        return self.size

    def convertToStitchDefinitions(self) -> List[StitchDefinition]:
        stitchDefs = []
        for i in range(0, self._run):
            if self._isFront:
                if self._isPurl:
                    newDef = StitchDefinition(name="P-in-cable", direction="ftb", parentOffSet=self._hold)
                else:
                    newDef = StitchDefinition(name="K-in-cable", parentOffSet=self._hold)
            else:
                newDef = StitchDefinition(name="K-in-cable", parentOffSet=self._hold)
            stitchDefs.append(newDef)
        for i in range(0, self._hold):
            if self._isFront:  # working back stitches in purl
                newDef = StitchDefinition(name="K-in-cable", depth=1, parentOffSet=-1 * self._run)
            else:  # isBack
                if self._isPurl:
                    newDef = StitchDefinition(name="P-in-cable", direction="ftb", depth=-1, parentOffSet=-1 * self._run)
                else:
                    newDef = StitchDefinition(name="K-in-cable", depth=-1, parentOffSet=-1 * self._run)
            stitchDefs.append(newDef)

        return stitchDefs


def cableDefinition(cableToken: str) -> CableDefinition:
    isPurl = cableToken[-1:] == "p"
    if cableToken[0:2] == "t2":
        isFront = cableToken[2:3] == "l"
        size = 2
    else:
        isFront = cableToken[2:3] == "f"
        size = int(cableToken[1:2])
    return CableDefinition(size=size, isPurl=isPurl, isFront=isFront)
