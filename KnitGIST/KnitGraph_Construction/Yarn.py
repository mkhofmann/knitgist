import networkx as networkx
from typing import Optional


class Yarn:
    def __init__(self, yarnId: int, lastLoop: Optional[int] = None):
        # Create a directed graph representing this Yarn
        self.yarnGraph: networkx.DiGraph = networkx.DiGraph()

        # Integer representing the loopId of the lastLoop in this Yarn
        self.lastLoop: int = lastLoop
        self.yarnId: int = yarnId

    def addLoopToEnd(self, loopId: int = None) -> int:
        """
        Adds the loop at the end of the yarn
        :param loopId: the id of the new loop, if the loopId is none, it defaults to 1 more than last put on this yarn
        :return: the loopId added to the yarn
        """
        if loopId is None:
            if self.lastLoop is None:
                loopId = 0
            else:
                loopId = self.lastLoop + 1
        self.yarnGraph.add_node(loopId)
        if self.lastLoop is not None:
            self.yarnGraph.add_edge(self.lastLoop, loopId)
        self.lastLoop = loopId
        return loopId

    def getLoops(self):
        return [*self.yarnGraph.nodes]
