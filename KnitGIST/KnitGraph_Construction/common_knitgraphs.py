from KnitGIST.KnitGraph_Construction.ks_compiler.KSCompiler import KSCompiler


def buildGraphFunction(pattern, width=1, height=1, patternIsFile=False):
    compiler = KSCompiler(pattern, patternIsFile=patternIsFile)
    return compiler.buildGraph(width, height)


def buildStSt(width=10, height=10):
    return buildGraphFunction(ststPattern(), width, height)


def ststPattern():
    return r'''1st row k.
                2nd row p.'''


def buildRib1(width=10, height=10):
    return buildGraphFunction(ribPattern(), width, height)


def ribPattern(count=1):
    return r'''1st row k count={},p count.
                2nd row k count,p count.'''.format(count)


def buildRib2(width=10, height=10):
    return buildGraphFunction(ribPattern(2), width, height)


def buildSeed(width=10, height=10):
    return buildGraphFunction(seedPattern(), width, height)


def seedPattern():
    return r'''1st row k,p.
                2nd row p,k.'''


def buildMoss(width=12, height=12):
    return buildGraphFunction(mossPattern(), width, height)


def mossPattern():
    return r'''1st row k 2, p 2.
               2nd row k 2, p 2.
               3rd row p 2, k 2.
               4th row p 2, k 2.'''


def buildReverseStSt(width=10, height=10):
    return buildGraphFunction(reverseStstPattern(), width, height)


def reverseStstPattern():
    return r'''1st row p.
               2nd row k.'''


def buildGarter(width=10, height=10):
    return buildGraphFunction(garterPattern(), width, height)


def garterPattern():
    return r'''1st row k.'''


def buildWelt2(width=10, height=10):
    return buildGraphFunction(welt2Pattern(), width, height)


def welt2Pattern():
    return r'''1st and 4th rows k.
               2nd and 3rd rows p.'''


def buildBasicLace(width=10, height=10):
    return buildGraphFunction(basicLacePattern(), width, height)


def basicLacePattern():
    return r'''1st row k, [k 3] to last st, k.
               from ws 2nd to 6th rows p.
               3rd row k, [skpo, yo, k] to last st, k.
               5th row k, [k, yo, k2tog] to last st, k.'''


def buildBasicCable(width=10, height=12):
    return buildGraphFunction(basicCablePattern(), width, height)


def basicCablePattern():
    return r'''1st row k,[k 3] to last st, k.
               from ws 2nd to 6th rows p.
               3rd row k, [c3b] to last st, k.
               5th row k, [c3f] to last st, k.'''
