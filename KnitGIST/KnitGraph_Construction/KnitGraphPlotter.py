import matplotlib.pyplot as plt
import networkx as nx

from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph


def plotKnitGraph(knitgraph, ouputFileName, show=False):
    plotter = KnitGraphPlotter(knitgraph)
    plotter.drawGraph(ouputFileName, show)
    plt.clf()


class KnitGraphPlotter(object):
    def __init__(self, knitgraph: KnitGraph):
        self.knitgraph: KnitGraph = knitgraph
        self.minWidth, minRow, maxRow, self.maxWidth = self.knitgraph.minMaxWidth()
        self.totalRows = len(self.knitgraph.rows)

    def getPositions(self):
        pos = {}
        for node in self.knitgraph.graph.nodes:
            loop = self.knitgraph.loopSet[node]
            x_pos = self.getRowIndexPosition(loop.index_in_row, loop.row)
            pos[node] = (x_pos, self.getRowPosition(loop.row))
        return pos

    def getEdgeLabels(self):
        labels = {}
        for edge in self.knitgraph.graph.edges:
            edgeName = self.knitgraph.graph.edges[edge]["stitchName"]
            labels[edge] = edgeName
        return labels

    def getRowIndexPosition(self, rowIndex, rowNum):
        portion = float(rowIndex) / float(self.maxWidth - 1)
        if rowNum == -1 or rowNum == 0:
            return portion
        elif rowNum % 2 == 1:  # ws
            return 1.0 - portion
        return portion

    def getRowPosition(self, rowNum):
        coAdjustedRowNum = rowNum + 1  # co is at -1 but we want to show it so we add 1 to everything
        portion = float(coAdjustedRowNum) / float(self.totalRows)
        return portion

    def drawGraph(self, filename, show=False):
        positions = self.getPositions()
        plt.figure(figsize=(20, 20))
        nx.draw_networkx_edge_labels(self.knitgraph.graph, pos=positions, edge_labels=self.getEdgeLabels(),
                                     label_pos=.5, font_size=20)
        nx.draw_networkx(self.knitgraph.graph, pos=positions, node_size=1000, font_size=20)
        nx.draw_networkx(self.knitgraph.yarns[0].yarnGraph, pos=positions, edge_color="b", node_size=1000, font_size=20)

        plt.savefig(filename)  # save as png
        if show:
            plt.show()  # display
