from typing import Dict

from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph
from KnitGIST.KnitGraph_Construction.Loop import Loop


class DoubleKnitGraph(KnitGraph):
    def __init__(self, templateKnitGraph: KnitGraph):

        # single sided KnitGraph as a template for the two sides of this DoubleKnitGraph
        super().__init__()
        self.maxRow = templateKnitGraph.maxRow
        self.templateKnitGraph: KnitGraph = templateKnitGraph

        # dictionaries from the template loops to the DoubleKnitGraph loops
        self.originalToFront: Dict[Loop, Loop] = {}
        self.originalToBack: Dict[Loop, Loop] = {}
        self.generate_double_graph()

    def generate_double_graph(self):
        # iterate through all the loops in the template graph
        # make two loops in the graph being generating to represent
        # the front_side (identical to the original graph)
        # and the back_side (the reversed graph with the added color)
        self._cast_on()

        newRow = []
        rowIndex = 1
        for loop in self.templateKnitGraph.loopsInRowIter():
            if loop.row < rowIndex:  # cast on row:
                continue
            elif loop.row > rowIndex:
                self.rows[rowIndex] = newRow
                newRow = []
                rowIndex = loop.row
            frontIndex = (loop.index_in_row * 2)
            backIndex = frontIndex + 1
            if loop.row % 2 == 0:  # WS row
                rowMax = (len(self.templateKnitGraph.rows[loop.row]) * 2) - 1
                oldFront = frontIndex
                frontIndex = rowMax - backIndex
                backIndex = rowMax - oldFront

                self.changeColor(1)
                back_parent = self._addLoop(loop.row, backIndex)
                self.originalToBack[loop] = back_parent
                newRow.append(back_parent)

                self.changeColor(0)
                front_parent = self._addLoop(loop.row, frontIndex)
                self.originalToFront[loop] = front_parent
                newRow.append(front_parent)
            else: # switch which we start knitting on reverse of direction
                self.changeColor(0)
                front_parent = self._addLoop(loop.row, frontIndex)
                self.originalToFront[loop] = front_parent
                newRow.append(front_parent)

                self.changeColor(1)
                back_parent = self._addLoop(loop.row, backIndex)
                self.originalToBack[loop] = back_parent
                newRow.append(back_parent)

        self.rows[rowIndex] = newRow

        # look at all of the edges of the template KnitGraph
        # recreate those edges between
        # the new loops on the front side
        # and the opposite (flip the direction parameter) edges for the back_side
        for loop in self.templateKnitGraph.loopsInRowIter():

            # getInEdgesByParents returns a list of a child's in edges
            # with the list index being the parentId and the list contents being the in edge.
            inEdgesList = self.templateKnitGraph.getInEdgesByParents(loop.loopId)
            for parentId, parentData in inEdgesList.items():
                parent = self.templateKnitGraph.getLoop(parentId)
                # add edge to front side of the graph
                stitchName = parentData["stitchName"]
                stitchDir = parentData['direction']
                stitchDepth = parentData['depth']
                stitchParentOrder = parentData['parentOrder']
                self.connectLoops(parent=self.originalToFront[parent], child=self.originalToFront[loop], stitchName=stitchName,
                                  direction=stitchDir, depth=stitchDepth, parentOrder=stitchParentOrder)
                if stitchDir == "btf":
                    stitchDir = "ftb"
                else:
                    stitchDir = "btf"
                stitchDepth = -1 * stitchDepth
                stitchParentOrder = -1 * stitchParentOrder
                self.connectLoops(self.originalToBack[parent], self.originalToBack[loop], stitchName=stitchName, direction=stitchDir, depth=stitchDepth,
                                  parentOrder=stitchParentOrder)

    def _cast_on(self):
        pRow = []
        cRow = []
        for co_parent, co_child in zip(self.templateKnitGraph.rows[-1], self.templateKnitGraph.rows[0]):
            self.changeColor(1)
            backIndex = (co_parent.index_in_row * 2) + 1
            back_parent = self._addLoop(co_parent.row, backIndex)
            self.originalToBack[co_parent] = back_parent
            pRow.append(back_parent)
            back_child = self._addLoop(co_child.row, backIndex)
            self.originalToBack[co_child] = back_child
            cRow.append(back_child)

            self.changeColor(0)
            frontIndex = (co_parent.index_in_row * 2)
            front_parent = self._addLoop(co_parent.row, frontIndex)
            self.originalToFront[co_parent] = front_parent
            pRow.append(front_parent)
            front_child = self._addLoop(co_child.row, frontIndex)
            self.originalToFront[co_child] = front_child
            cRow.append(front_child)

        self.rows[-1] = pRow
        self.rows[0] = cRow

    def swapStitchColor(self, templateLoopId: str):
        originalLoop = self.templateKnitGraph.getLoop(templateLoopId)
        frontLoop = self.originalToFront[originalLoop]
        backLoop = self.originalToBack[originalLoop]
        frontYarn = frontLoop.yarnId
        frontLoop.yarnId = backLoop.yarnId
        backLoop.yarnId = frontYarn
        pass
