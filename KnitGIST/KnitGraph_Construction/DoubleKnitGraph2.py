from typing import Dict

from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph
from KnitGIST.KnitGraph_Construction.Loop import Loop


class DoubleKnitGraph2(KnitGraph):
    def __init__(self, knitGraph1: KnitGraph, knitGraph2: KnitGraph):

        # single sided KnitGraph as a template for the two sides of this DoubleKnitGraph
        super().__init__()

        # check that knitGraph1 and knitGraph2 have the same dimensions
        assert (len(knitGraph1.rows) == len(knitGraph2.rows))
        for num in knitGraph1.rows:
            assert (len(knitGraph1.rows[num]) == len(knitGraph2.rows[num]))

        self.knitGraph1: KnitGraph = knitGraph1
        self.knitGraph2: KnitGraph = knitGraph2

        # dictionaries from the template loops to the DoubleKnitGraph loops
        self.original1ToFront: Dict[Loop, Loop] = {}
        self.original2ToBack: Dict[Loop, Loop] = {}
        self.generate_double_graph()

    @property
    def maxRow(self) -> int:
        return self.knitGraph1.maxRow  # (same as knitGraph2.maxRow)

    def generate_double_graph(self):
        # iterate through all the loops in the template graph
        # make two loops in the graph being generating to represent
        # the front_side (identical to the original graph)
        # and the back_side (the reversed graph with the added color)
        self._cast_on()

        newRow = []
        rowIndex = 1
        for loop1, loop2 in zip(self.knitGraph1.loopsInRowIter(), self.knitGraph2.loopsInRowIter()):
            if loop1.row < rowIndex:  # check if cast on row
                continue
            elif loop1.row > rowIndex:
                self.rows[rowIndex] = newRow
                newRow = []
                rowIndex = loop1.row
            frontIndex = (loop1.index_in_row * 2)
            backIndex = frontIndex + 1
            if loop1.row % 2 == 0:  # WS row
                rowMax = (len(self.knitGraph1.rows[loop1.row]) * 2) - 1
                oldFront = frontIndex
                frontIndex = rowMax - backIndex
                backIndex = rowMax - oldFront

                self.changeColor(1)
                back_parent = self._addLoop(loop2.row, backIndex)
                self.original2ToBack[loop2] = back_parent
                newRow.append(back_parent)

                self.changeColor(0)
                front_parent = self._addLoop(loop1.row, frontIndex)
                self.original1ToFront[loop1] = front_parent
                newRow.append(front_parent)
            else:  # switch which we start knitting on reverse of direction
                self.changeColor(0)
                front_parent = self._addLoop(loop1.row, frontIndex)
                self.original1ToFront[loop1] = front_parent
                newRow.append(front_parent)

                self.changeColor(1)
                back_parent = self._addLoop(loop2.row, backIndex)
                self.original2ToBack[loop2] = back_parent
                newRow.append(back_parent)

        self.rows[rowIndex] = newRow

        # look at all of the edges of the template KnitGraph
        # recreate those edges between
        # the new loops on the front side
        # and the opposite (flip the direction parameter) edges for the back_side
        for loop1, loop2 in zip(self.knitGraph1.loopsInRowIter(), self.knitGraph2.loopsInRowIter()):

            # getInEdgesByParents returns a list of a child's in edges
            # with the list index being the parentId and the list contents being the in edge.
            inEdgesList1 = self.knitGraph1.getInEdgesByParents(loop1.loopId)
            inEdgesList2 = self.knitGraph2.getInEdgesByParents(loop2.loopId)
            for parentId1, parentData1, parentId2, parentData2 in zip(inEdgesList1.items(), inEdgesList2.items()):
                parent1 = self.knitGraph1.getLoop(parentId1)
                parent2 = self.knitGraph2.getLoop(parentId2)
                # add edge to front side of the graph
                stitchName1 = parentData1["stitchName"]
                stitchDir1 = parentData1['direction']
                stitchDepth1 = parentData1['depth']
                stitchParentOrder1 = parentData1['parentOrder']

                stitchName2 = parentData2["stitchName"]
                stitchDir2 = parentData2['direction']
                stitchDepth2 = parentData2['depth']
                stitchParentOrder2 = parentData2['parentOrder']

                self.connectLoops(parent=self.original1ToFront[parent1], child=self.original1ToFront[loop1],
                                  stitchName=stitchName1, direction=stitchDir1, depth=stitchDepth1,
                                  parentOrder=stitchParentOrder1)

                self.connectLoops(self.original2ToBack[parent2], self.original2ToBack[loop2], stitchName=stitchName2,
                                  direction=stitchDir2, depth=stitchDepth2, parentOrder=stitchParentOrder2)

    def _cast_on(self):
        pRow = []
        cRow = []

        self.changeColor(1)
        for co_parent, co_child in zip(self.knitGraph1.rows[-1], self.knitGraph1.rows[0]):
            backIndex = (co_parent.index_in_row * 2) + 1
            back_parent = self._addLoop(co_parent.row, backIndex)
            self.originalToBack[co_parent] = back_parent
            pRow.append(back_parent)
            back_child = self._addLoop(co_child.row, backIndex)
            self.originalToBack[co_child] = back_child
            cRow.append(back_child)

        self.changeColor(0)
        for co_parent, co_child in zip(self.knitGraph2.rows[-1], self.knitGraph2.rows[0]):
            frontIndex = (co_parent.index_in_row * 2)
            front_parent = self._addLoop(co_parent.row, frontIndex)
            self.originalToFront[co_parent] = front_parent
            pRow.append(front_parent)
            front_child = self._addLoop(co_child.row, frontIndex)
            self.originalToFront[co_child] = front_child
            cRow.append(front_child)

        self.rows[-1] = pRow
        self.rows[0] = cRow

    def swapStitchColor(self, switchLoopId: str):
        originalLoop1 = self.knitGraph1.getLoop(switchLoopId)
        originalLoop2 = self.knitGraph2.getLoop(switchLoopId)
        loop1 = self.original1ToFront[originalLoop1]
        loop2 = self.original2ToBack[originalLoop2]
        temp = loop1.yarnId
        loop1.yarnId = loop2.yarnId
        loop2.yarnId = temp
        pass
