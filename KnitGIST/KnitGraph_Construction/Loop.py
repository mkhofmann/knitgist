class Loop:
    def __init__(self, loopId: int, row: int, index_in_row: int, yarnId: int):
        """
        :param loopId: id of loop
        :param row: the row number loop is in
        :param index_in_row: the index of the loop in that row
        """
        self.index_in_row: int = index_in_row
        self.row: int = row
        self._loopId: int = loopId
        self.yarnId: int = yarnId
        self.planeDeformationByChildren = None
        self.planeDeformationByParents = None
        self.curvature = None
        self.waleDeformation = None
        self.waleCompression = None
        self.maxWaleDepth = None
        self.loopParams = {}

    @property
    def loopId(self):
        return f"{self._loopId}"

    def __hash__(self):
        return self._loopId

    def __eq__(self, other):
        return isinstance(other, Loop) and self._loopId == other._loopId and self.yarnId == other.yarnId

    def __lt__(self, other):
        assert isinstance(other, Loop)
        return self._loopId < other._loopId

    def __gt__(self, other):
        assert isinstance(other, Loop)
        return self._loopId > other._loopId

    def __str__(self):
        return f"{self._loopId}=r{self.row},y{self.yarnId}"

    def __repr__(self):
        return str(self)
