from KnitGIST.KnitGraph_Construction.DoubleKnitGraph import DoubleKnitGraph
from KnitGIST.KnitGraph_Construction.Iterator import Iterator
from KnitGIST.KnitGraph_Construction.common_knitgraphs import buildGraphFunction


class BitmapParser:
    # Returns a DoubleKnitGraph with stitch type specified by textureStr
    # whose colors are swapped based on the file corresponding to bitmapFileName.
    # (1 indicates swap color in the bitmap file)
    def bitmapSwapColors(self, bitmapFileName, textureStr):
        bitmapFile = open(bitmapFileName, 'r')
        try:
            allLines = bitmapFile.readlines()
            firstLine = allLines[0]
            fileWidth = len(firstLine.split())
            fileHeight = len(allLines)

            knitGraph = buildGraphFunction(textureStr, fileWidth, fileHeight)
            doubleKnitGraph = DoubleKnitGraph(knitGraph)

            itr = Iterator()
            row_arr = itr.reverse_2D_arr_dict_individual_rows(knitGraph.rows)
            row_arr_reversed = itr.reverse_2D_arr_dict_row_order(row_arr, fileHeight)

            for rowIndex in range(fileHeight):
                bitArr = allLines[rowIndex].split()
                row = row_arr_reversed[rowIndex]
                for colIndex, loop in enumerate(row):
                    if (bitArr[colIndex] == 1):
                        doubleKnitGraph.swapStitchColor(loop.loopId)

            return doubleKnitGraph

        finally:
            bitmapFile.close()