import sys
from typing import Dict, List, Optional, Tuple, Union, Any

import networkx as networkx

from KnitGIST.KnitGraph_Construction.Loop import Loop
from KnitGIST.KnitGraph_Construction.Yarn import Yarn
from KnitGIST.KnitGraph_Construction.ks_compiler.CableDefinition import CableDefinition
from KnitGIST.KnitGraph_Construction.ks_compiler.LoopStack import LoopStack
from KnitGIST.KnitGraph_Construction.ks_compiler.RepCondition import RepConditionResult
from KnitGIST.KnitGraph_Construction.ks_compiler.StitchDefinition import StitchDefinition


class KnitGraph:
    def __init__(self):
        # Directed graph for this KnitGraph
        self.graph: networkx.DiGraph = networkx.DiGraph()

        # 1D array of unique Loop objects with the ith index being the Loop with loopId i
        self.loopSet: Dict[str, Loop] = {}
        self._lastLoopId: int = -1

        # A list of Yarns, or Loop LinkedLists
        self.yarns: Dict[int, Yarn] = {}
        self._addYarn(Yarn(0))
        self.currYarnId: int = 0
        self.lastYarnId: int = 0

        # 2D array layout of all the Loops for this KnitGraph
        self.rows: Dict[int, List[Loop]] = {}

        # Index of the last row
        self.maxRow: int = 0

    def loopsInRowIter(self):
        return self.loopSet.values()

    # QUESTION: why aren't we updating the graph according to our changes in rows?
    # ans: we are, it happens in the connectLoops function
    def castOnRow(self, castOnCount: int):
        """
        creates a cast on row of length cast on count.
        :param castOnCount: length of cast on row
        """
        assert len(self.rows) == 0, f"Cannot cast on for KnitGraph of height {len(self.rows)}"
        pRow = []
        cRow = []
        for i in range(castOnCount - 1, -1, -1):
            # Appends a loop to this Yarn. The loop exists at the last row of this KnitGraph
            newParent: Loop = self._addLoop(row=-1, rowIndex=i)
            pRow.append(newParent)

            # Appends a loop to this Yarn. The loop exists at the first row of this KnitGraph
            newChild: Loop = self._addLoop(row=0, rowIndex=i)
            cRow.append(newChild)

            # Connects the two loops we just created by adding an edge between them
            self.connectLoops(newParent, newChild, "co")

        # Sets the first, but less than 0th, row of this KnitGist with the series of parent loops we just created
        self.rows[-1] = pRow

        # Sets the first row of this KnitGist with the series of child loops we just created.
        self.rows[0] = cRow

        # QUESTION: why are we doing this? Shouldn't maxRow = 1 in this case because we have 2 rows now?
        # Ans: this is based on a convenient convention that the cast on row is at the -1 position
        self.maxRow = 0

    def addRowByKs2Instructions(self, rowInstructions: List[Tuple[Union[StitchDefinition, list], RepConditionResult]],
                                rowNum: Optional[int] = None) -> int:
        """
        Adds a row instruction set to the graph in the form of new loops and stitch-edge connections
        :param rowInstructions: The instruction set for the row
        :param rowNum: the row index to add them at. If none this will default to the top row
        :return: the loop count of loops on this row
        """
        if rowNum is None:
            rowNum = self.maxRow + 1
            self.rows[rowNum] = []
            self.maxRow = rowNum
        parentRowLoops: LoopStack = LoopStack(self, rowNum, True)
        rowIndex: int = 0
        while len(parentRowLoops) > 0:
            for operationTuple in rowInstructions:
                operation = operationTuple[0]
                repCondition = operationTuple[1]
                if repCondition.condition is not None:  # conditional ending
                    while len(parentRowLoops) > repCondition.reps:
                        operationList, rowIndex = self._addStaticOperations(operation, parentRowLoops,
                                                                            RepConditionResult(1), rowIndex, rowNum)
                else:
                    operationList, rowIndex = self._addStaticOperations(operation, parentRowLoops, repCondition,
                                                                        rowIndex, rowNum)
        return len(self.rows[rowNum])

    def _addStaticOperations(self, operation: Union[StitchDefinition, CableDefinition, dict, list],
                             parentRowLoops: LoopStack, repCondition: RepConditionResult, rowIndex: int, rowNum: int) -> \
    Tuple[List[Union[StitchDefinition, CableDefinition]], int]:
        """
        Adds a series of stitches at rowNum and rowIndex based on the operation passed in. Parses the operation into its individual stitch components
        :param operation: The operation to be converted to static operations
        :param parentRowLoops: The stack of loops available to be consumed by these operations
        :param repCondition: the rep condition for repeating this result
        :param rowIndex: the index in the row that these are being added to, will be modified
        :param rowNum: the index of the row these are being added at
        :return: the list of stitch operations made static (with no reps) and the updated in-row-index after these operations are applied
        """
        operationList = KnitGraph.createStitchDefList(operation, repCondition)
        for stitchDef in operationList:
            rowIndex = self._addStitchDefinition(stitchDef, parentRowLoops, rowIndex, rowNum)
        return operationList, rowIndex

    # Add a stitch or a series of stitches to the rowNum at rowIndex based on the type of definition
    # Example: if definition is a Cable definition, then we will append a cable to the row at rowNum
    def _addStitchDefinition(self, definition: Union[StitchDefinition, CableDefinition, dict],
                             parentRowLoops: LoopStack, rowIndex: int, rowNum: int) -> int:
        """
        Add a stitch or a series of stitches to the rowNum at rowIndex based on the type of definition.
        Example: if definition is a Cable definition, then we will append a cable to the row at rowNum
        :param definition: The definition of the stitch to be added
        :param parentRowLoops: the loops that can be consumed to make this stitch
        :param rowIndex: the in-row index of the stitch being added
        :param rowNum: the row index where the stitch is added
        :return: the updated in-row index after the stitch is added
        """
        # if isinstance(definition, KPFbDefinition):
        #     c1, c2 = self.addKPFBIncrease(definition, parentRowLoops, rowNum, rowIndex)
        #     rowIndex += 2
        #     self.rows[rowNum].append(c1)
        #     self.rows[rowNum].append(c2)
        if isinstance(definition, CableDefinition):
            # children is a List[Loop]
            children = self._addCableByCableDefinition(definition, parentRowLoops, rowNum, rowIndex)
            rowIndex += definition.size
            self.rows[rowNum].extend(children)
        elif type(definition) == dict and "cable" in definition:
            # children is a List[Loop]
            children = self._addCableByCableDefinition(definition, parentRowLoops, rowNum, rowIndex)
            rowIndex += definition["size"]
            self.rows[rowNum].extend(children)
        else:
            # child is a single Loop
            child = self._addStitchByStitchDefinition(definition, parentRowLoops, rowNum, rowIndex)
            rowIndex += 1
            self.rows[rowNum].append(child)
        return rowIndex

    # TODO, if code is working without this then delete
    # def addRow(self, rowInstructions, rowNum: int = None):
    #     if rowNum is None:
    #         # Set rowNum equal to the index of the last row in rows
    #         rowNum = self.maxRow + 1
    #         self.rows[rowNum] = []
    #         self.maxRow = rowNum
    #
    #     # Get the parent row (row directly above rowNum); an array of Loops
    #     parentRowLoops = self.rows[rowNum - 1].copy()
    #     rowIndex: int = 0
    #     parentRowLoops, rowIndex = self.addStitchList(rowInstructions.startBorderList, parentRowLoops, rowNum, rowIndex)
    #     while len(parentRowLoops) - rowInstructions.endBorderLoopCount > 0:
    #         parentRowLoops, rowIndex = self.addStitchList(rowInstructions.repetitionList, parentRowLoops, rowNum,
    #                                                       rowIndex)
    #     parentRowLoops, rowIndex = self.addStitchList(rowInstructions.endBorderList, parentRowLoops, rowNum, rowIndex)
    #     assert len(parentRowLoops) == 0

    def _addStitchList(self, definitionList: List[Union[CableDefinition, StitchDefinition, list]],
                       parentRowLoops: LoopStack, rowNum: int, rowIndex: int) -> Tuple[LoopStack, int]:
        """
        Adds a series of stitches at rowIndex at rowNum based on the type of stitches in definitionList.
        :param definitionList: the list of stitch definitions to add in
        :param parentRowLoops: the available loops to make these stitches
        :param rowNum: the index of the row being added to
        :param rowIndex: the index in the rwo being added to
        :return: A tuple of the un-consumed parent loops and the index in the row after the added stitches
        """
        for definition in definitionList:
            if type(definition) is list:
                parentRowLoops, rowIndex = self._addStitchList(definition, parentRowLoops, rowNum, rowIndex)
            # elif isinstance(definition, KPFbDefinition):
            #     c1, c2 = self.addKPFBIncrease(definition, parentRowLoops, rowNum, rowIndex)
            #     rowIndex += 2
            #     self.rows[rowNum].append(c1)
            #     self.rows[rowNum].append(c2)
            elif isinstance(definition, CableDefinition):
                children = self._addCableByCableDefinition(definition, parentRowLoops, rowNum, rowIndex)
                rowIndex += definition.size
                self.rows[rowNum].extend(children)
            else:
                child = self._addStitchByStitchDefinition(definition, parentRowLoops, rowNum, rowIndex)
                rowIndex += 1
                self.rows[rowNum].append(child)
        return parentRowLoops, rowIndex

    # Returns a series of stitches to create a cable
    def _addCableByCableDefinition(self, cableDef: Union[CableDefinition, dict], consumableLoops: LoopStack, row: int,
                                   startIndex: int) -> List[Loop]:
        """
        Adds a cable to the graph at the specified location
        :param cableDef: definition of the cable
        :param consumableLoops: the loops available to make the cable from
        :param row: the row where the cable is added
        :param startIndex: the start index of the cable in the row
        :return: the list of child loops of the cable
        """
        parents = []
        if isinstance(cableDef, CableDefinition):
            cableStitchDefs = cableDef.convertToStitchDefinitions()
            cableSize = cableDef.size
        else:
            cableStitchDefs = cableDef["cable"]
            cableSize = cableDef["size"]
        for i in range(0, cableSize):
            parents.append(consumableLoops.pop())
        children = []
        for parentIndex, cableStitchDef in enumerate(cableStitchDefs):
            child = self._addLoop(row, startIndex + parentIndex)
            parent = parents[parentIndex + cableStitchDef.parentOffSet]
            self.connectLoops(parent, child, stitchName=cableStitchDef.name, direction=cableStitchDef._direction,
                              depth=cableStitchDef.depth)
            children.append(child)
        return children

    # Todo if code is working, delete
    # def _addKPFBIncrease(self, fbDefinition, consumableLoops, row, rowIndex):
    #     base, yo = fbDefinition.convertToStitchDefinitions()
    #     c1 = self._addStitchByStitchDefinition(base, consumableLoops, row, rowIndex)
    #     c2 = self._addStitchByStitchDefinition(yo, consumableLoops, row, rowIndex + 1)
    #     return c1, c2

    def _addStitchByStitchDefinition(self, stitchDef: StitchDefinition, consumableLoops: LoopStack, row: int,
                                     rowIndex: int) -> Loop:
        """
        Adds a stitch to the graph by the given definition and location data
        :param stitchDef: the definition of the stitch to be added
        :param consumableLoops: the available loops to consume
        :param row: the row being added at
        :param rowIndex: the index in the row being added at
        :return: the child loop of the new stitch
        """
        onFront = row % 2 == 1
        if not onFront and (
                stitchDef.direction == "btf" or stitchDef.direction == "ftb"):  # ws flip knit and purls todo: handle cases of knits in other stitch groups like kfb pfb, cables, etc
            if stitchDef.direction == "btf":
                name = "p"
                direction = "ftb"
            else:  # stitchDef.name == "purl":
                name = "k"
                direction = "btf"
        else:
            direction = stitchDef.direction
            name = stitchDef.name
        if stitchDef.name == "slip":
            return consumableLoops.pop()
        child = self._addLoop(row, rowIndex)
        if stitchDef.isDecrease():
            parents = []
            assert len(consumableLoops) >= stitchDef.inLoopsCount, "not enough loops"
            for i in range(0, stitchDef.inLoopsCount):
                parents.append(consumableLoops.pop())
            for i, parent in zip(stitchDef.parentIterOrder, parents):
                self.connectLoops(parent, child, stitchName=name, direction=direction, parentOrder=i)
        elif "yo" in stitchDef.name:
            pass  # no edges to connect
        else:  # knits and purls
            assert len(consumableLoops) > 0, "not enough loops"
            p = consumableLoops.pop()
            self.connectLoops(p, child, stitchName=name, direction=direction)
        return child

    # Adds a loop to the end of the Yarn at location (row,rowIndex)
    # This new loop is documented in this KnitGraph's loopSet and graph
    def _addLoop(self, row: int, rowIndex: int) -> Loop:
        """
        Adds a loop to the end of the Yarn at location (row,rowIndex). This new loop is documented in this KnitGraph's loopSet and graph
        :param row: the row the loop belongs to
        :param rowIndex: the index of this loop in that row
        :return: the loop created
        """
        loopId: int = self._lastLoopId + 1
        self._lastLoopId = loopId
        loopId = self.yarns[self.currYarnId].addLoopToEnd(loopId)
        loop: Loop = Loop(loopId, row, rowIndex, self.currYarnId)
        self.loopSet[loop.loopId] = loop
        self.graph.add_node(loop.loopId)
        return loop

    # Create a new Yarn with the new loop
    # Specify a Yarn (by default a new Yarn)
    # When addLoop is called
    def changeColor(self, yarnId: Optional[int] = None) -> int:
        """
        Changes which yarn is currently being added to
        Create a new Yarn with the new loop
        Specify a Yarn (by default a new Yarn)
        When addLoop is called
        :param yarnId: the id of an existing yarn
        :return the yarnId of the yarn swapped to
        """
        if yarnId is None or yarnId == self.lastYarnId + 1:
            self.lastYarnId += 1
            newYarnId = self._addYarn(Yarn(self.lastYarnId, self.yarns[self.currYarnId].lastLoop))
            self.yarns[newYarnId] = Yarn(self.lastYarnId, self.yarns[self.currYarnId].lastLoop)
            self.currYarnId = self.lastYarnId
            return newYarnId
        else:  # a yarnId was specified
            globalLastLoop = self.yarns[self.currYarnId].lastLoop
            self.currYarnId = yarnId
            if self.currYarnId not in self.yarns:
                self.yarns[self.currYarnId] = Yarn(self.currYarnId)
            self.yarns[self.currYarnId].lastLoop = globalLastLoop
            return yarnId

    def connectLoops(self, parent: Loop, child: Loop, stitchName: str, direction: str = "btf", depth: int = 0,
                     parentOrder: int = 0):
        """
        Connects the parent loop to the child loop by connecting the corresponding nodes in the graph by an edge
        :param parent: Loop to be pulled through
        :param child: Loop being pulled through
        :param stitchName: name of the stitch represented by this edge
        :param direction: Direction "btf" or "ftb" the loop is pulled through
        :param depth: cabling depth of this edge, 0 for standard stitches
        :param parentOrder: the stacking order of this loop in a decrease
        """
        assert child.loopId in self.loopSet
        assert parent.loopId in self.loopSet
        self.graph.add_edge(parent.loopId, child.loopId)
        self.graph[parent.loopId][child.loopId]['direction'] = direction
        self.graph[parent.loopId][child.loopId]['depth'] = depth
        self.graph[parent.loopId][child.loopId]['stitchName'] = stitchName
        self.graph[parent.loopId][child.loopId]["parentOrder"] = parentOrder

    def getLoop(self, loopId: str) -> Optional[Loop]:
        """
        :param loopId: id of loop to return
        :return: the Loop with loop id equal to loopId
        """
        if loopId is None:
            return None
        return self.loopSet[loopId]

    def bottomWidth(self) -> int:
        """
        :return: the length of the first row of this KnitGraph
        """
        return len(self.rows[0])

    def topWidth(self) -> int:
        """
        :return: the length of the last row of this KnitGraph
        """
        return len(self.rows[-1])

    def minMaxWidth(self) -> Tuple[int, int, int, int]:
        """
        :return: the size and index of the row with the smallest width and the row with the largest width
        """
        minWidth: int = sys.maxsize
        maxWidth: int = -1
        minRow: int = -1
        maxRow: int = 1
        rowCounter: int = 0
        for rowNum in self.rows:
            row = self.rows[rowNum]
            if len(row) < minWidth:
                minWidth = len(row)
                minRow = rowCounter
            if len(row) > maxWidth:
                maxWidth = len(row)
                maxRow = rowCounter
            rowCounter += 1
        return minWidth, minRow, maxRow, maxWidth

    def height(self) -> int:
        """
        :return: the number of rows
        """
        return len(self.rows)

    # todo now that this examines YarnGraph, we need a function for indexing nodes next to each other but on different yarns
    def getLoopYarnWiseNeighbors(self, loopId: str, stayInRow: bool = True) -> Tuple[Optional[str], Optional[str]]:
        """
        :param loopId:
        :param stayInRow: if neighbor loops are outside current loop return
        :return: the (previous and next) neighbors of the Loop with id loopId in the context of the Loop's Yarn
        """
        row: int = self.getLoop(loopId).row
        nextNeighbors = self.yarns[self.currYarnId].yarnGraph.successors(loopId)
        nextNeighbor = None
        for n in nextNeighbors:  # deiterate
            nextNeighbor = n
        prevNeighbors = self.yarns[self.currYarnId].yarnGraph.predecessors(loopId)
        prevNeighbor = None
        for p in prevNeighbors:  # deiterate
            prevNeighbor = p
        if stayInRow:
            if row == 0:
                if prevNeighbor is not None and self.getLoop(
                        prevNeighbor).row == -1:  # if this is a cast on go forward to next child
                    prevNeighbor, _ = self.getLoopYarnWiseNeighbors(prevNeighbor, False)
                if nextNeighbor is not None and self.getLoop(
                        nextNeighbor).row == -1:  # if this is a cast on go forward to next child
                    _, nextNeighbor = self.getLoopYarnWiseNeighbors(nextNeighbor, False)
            prevLoop = self.getLoop(prevNeighbor)
            if prevLoop is not None and prevLoop.row != row:
                prevNeighbor = None
            nextLoop = self.getLoop(nextNeighbor)
            if nextLoop is not None and nextLoop.row != row:
                nextNeighbor = None

        return prevNeighbor, nextNeighbor

    def getOutEdgesByChildren(self, parentId: str):
        """
        :param parentId: the id of the loop with outgoing edges
        :return: The children of the node corresponding to parentId in this KnitGraph
        """
        if parentId is None:
            return None
        return self.graph.adj[parentId]

    def getInEdgesByParents(self, childId: str) -> Dict[str, Dict[str, Any]]:
        """
        :param childId:
        :return: a list of a child's in edges with the list index being the parentId and the list contents being the in edge.
        """
        if childId is None:
            return {}
        edges = self.graph.in_edges(childId)
        adjView = {}
        for edgeTuple in edges:
            parentId = edgeTuple[0]
            parentOutEdges = self.getOutEdgesByChildren(parentId)
            adjView[parentId] = parentOutEdges[childId]
        return adjView

    def loopIsBTF(self, childId: str) -> bool:
        inEdges = self.getInEdgesByParents(childId)
        for edge_data in inEdges.values():
            return edge_data["direction"] == "btf"
        return False

    def loopIsFTB(self, childId: str) -> bool:
        inEdges = self.getInEdgesByParents(childId)
        for edge_data in inEdges.values():
            return edge_data["direction"] == "ftb"
        return False

    def _getEdgeNeighbors(self, childId: str, neighborParentId: str, neighborChildId: str):
        """

        :param childId: loop id of current loop
        :param neighborParentId: loop id of parent loop in neighboring edge
        :param neighborChildId:  loop id of child loop in neighboring edge
        :return: returns the parentEdge, neighbor child id, and the adjacency matrix between them of an edge neighoring the declared edge
        """
        if neighborChildId is None and neighborParentId is None:  # there are no yarn wise neighbors, both parent and child are on edge
            return None
        elif neighborChildId is None:  # the child is on the edge, but the parent may have multiple outgoing edges so there is still a neighboring parent
            # todo: is there any case that this occurs where the answer is not just the child itself
            # todo: is there another edge case where parent is None but child isnt. If all increases are yo, then no?
            neighborParentOutEdges = self.getOutEdgesByChildren(neighborParentId)
            closestConnectedChild = None
            closestDistance = sys.maxsize
            for connectedChild in neighborParentOutEdges:
                distance = abs(connectedChild - childId)
                if distance < closestDistance:
                    closestDistance = distance
                    closestConnectedChild = connectedChild
                    if closestDistance == 0:
                        break
            return neighborParentId, closestConnectedChild, neighborParentOutEdges[closestConnectedChild]
        if self.graph.has_edge(neighborParentId, neighborChildId):  # neighbor child is connected to neighboring parent
            return neighborParentId, neighborChildId, self.graph.adj[neighborParentId][neighborChildId]
        else:  # cable: neighbor parent and child are not connected, but both are present so this must be a cable.
            assert len(self.graph.adj[neighborParentId]) == 1  # no increases or decreases in cables
            assert len(self.graph.pred[neighborChildId]) == 1
            cEdgeParent = list(self.graph.predecessors(neighborChildId))[0]
            pEdgeChild = list(self.graph.successors(neighborParentId))[0]
            if self.graph.adj[cEdgeParent][neighborChildId]["depth"] > self.graph.adj[neighborParentId][pEdgeChild][
                "depth"]:
                return cEdgeParent, neighborChildId, self.graph.adj[cEdgeParent][neighborChildId]
            else:
                return neighborParentId, pEdgeChild, self.graph.adj[neighborParentId][pEdgeChild]

    def _getWeftWiseEdgeNeighbors(self, parentId: str, childId: str):
        """
        :param parentId: parent loop id of edge
        :param childId: child loop id of edge
        :return: the previous edge and next edge horizontally from specified edge
        """
        nextP, preP = self.getLoopYarnWiseNeighbors(
            parentId)  # note these are flipped because its previous and next relative to the child
        preC, nextC = self.getLoopYarnWiseNeighbors(childId)
        while preC is not None and len(list(self.graph.predecessors(preC))) == 0:  # skip yarn over neighbors
            preC, _ = self.getLoopYarnWiseNeighbors(preC)
        while nextC is not None and len(list(self.graph.predecessors(nextC))) == 0:  # skip yarn over neighbors
            _, nextC = self.getLoopYarnWiseNeighbors(nextC)
        preEdge = self._getEdgeNeighbors(childId, preP, preC)
        nextEdge = self._getEdgeNeighbors(childId, nextP, nextC)
        return preEdge, nextEdge

    def _waleWisePlaneDeformation(self, loopId: str, forceWeight: float = 1.0, store: bool = True,
                                  check: bool = False) -> float:
        """
        :param loopId: the id of the loop being checked for deformation
        :param forceWeight: additional weight applied
        :param store: if true, caches value in loop for ready
        :param check: if true, ignores cached value
        :return:
        """
        # todo should predecessor edges be considered?
        if not check and self.getLoop(loopId).waleDeformation is not None:
            return self.getLoop(loopId).waleDeformation
        nextEdges = self.graph.adj[loopId]
        prevEdges = self.graph.pred[loopId]
        assert len(nextEdges) <= 1  # yarn overs are only increase structure
        if len(prevEdges) == 0:
            deformation = 0
        else:
            deformation = 0
            prevLoopId = list(prevEdges.keys())[0]
            prevEdgeData = self.graph.adj[prevLoopId][loopId]
            prevEdgeDirection = prevEdgeData["direction"]
            if prevEdgeData["stitchName"] == "co":
                forceWeight = 0  # ignore cast on deformations since they aren't part of the texture
            cableDeformation = prevEdgeData["depth"]

            def difference_weight(e1Direction, e2Direction):
                return forceWeight * int(e1Direction != e2Direction)

            def pressureByEdges(e1Direction, e2Direction):
                return difference_weight(e1Direction, e2Direction) * (
                        -1 * int(e1Direction == 'ftb') + int(
                    e1Direction == "btf"))  # -1 (if ftb/ purl) + 1 (if btf/ knit). So always 0 plus a value

            for nextLoopId in nextEdges:
                nextEdgeData = nextEdges[nextLoopId]
                cableDeformation += nextEdgeData["depth"] * forceWeight
                deformation += pressureByEdges(prevEdgeDirection, nextEdgeData["direction"])
            deformation += cableDeformation
            if deformation > 1:
                deformation = 1
            elif deformation < -1:
                deformation = -1
        if store:
            self.getLoop(loopId).waleDeformation = deformation
        return deformation

    def _waleWiseCompressionOfWale(self, baseLoopId: str, forceWeight: float = 1.0, store: bool = True,
                                   check: bool = False) -> Tuple[float, int]:
        """
        :param baseLoopId: id of loop at base of wale
        :param forceWeight: modifier on force
        :param store: if true, caches result in loops
        :param check: if true, ignores cached values
        :return: the sum of the deformation across the loop from the base loop and the depth of the wale fromt hat point
        """
        loop = self.getLoop(baseLoopId)
        if not check and loop.waleCompression is not None:
            return loop.waleCompression, loop.maxWaleDepth
        sumDeformation = abs(self._waleWisePlaneDeformation(baseLoopId, forceWeight, store, check))
        maxDepth = -1
        for childLoop in self.graph.adj[baseLoopId]:
            waleCompression, depth = self._waleWiseCompressionOfWale(childLoop, forceWeight, store, check)
            maxDepth = max(maxDepth, depth)
            sumDeformation += abs(waleCompression)
        waleDepth = 1 + maxDepth
        if store:
            loop.waleCompression = sumDeformation
            loop.maxWaleDepth = waleDepth
        return sumDeformation, waleDepth

    def waleWiseCompression(self, forceWeight: float = 1.0) -> float:
        """
        :param forceWeight: a modifier on the weight
        :return: the horizontal compression of the whole graph
        """
        totalDeformation = 0
        for loop in self.rows[0]:
            deformation, waleDepth = self._waleWiseCompressionOfWale(loop.loopId, forceWeight)
            normalizedDeformation = deformation / waleDepth
            totalDeformation += normalizedDeformation
        return totalDeformation / float(len(self.rows[0]))

    def widthStretch(self, forceWeight: float = 1.0) -> float:
        """
        :param forceWeight: a modifier on the weight
        :return: the amount this texture stretches width wise
        """
        return max(0.0, self.horizontalCompression(forceWeight) - self.waleWiseCompression(forceWeight))

    def heightStretch(self, forceWeight: float = 1.0) -> float:
        """
        :param forceWeight: a modifier on the weight
        :return: the amount the texture stretches height wise
        """
        return max(0.0, self.waleWiseCompression(forceWeight) - self.horizontalCompression(forceWeight))

    def _weftWisePlaneDeformationByEdge(self, parentId: str, childId: str, forceWeight: float = 1.0, store: bool = True,
                                        check: bool = False) -> float:
        """
        positive plane is pressure of purls into knits, negative plane is pressure of knits into purls. 0 plane is
        position of like stitches because yarn can't be stretched infinitely plane can only be shifted between -1 and 1
        by immediate neighbor
        :param parentId: id of the parent loop
        :param childId: id of child loop
        :param forceWeight: modifier on weights
        :param store: if true, caches result
        :param check: if true, ignores cache
        :return: the deformation of an edge horizontally
        """
        if not check and "yarn-wise-deformation" in self.graph.adj[parentId][childId]:
            return self.graph.adj[parentId][childId]["yarn-wise-deformation"]
        edge = self.graph.get_edge_data(parentId, childId)
        if edge["depth"] < 0:  # cables are forced out of plane
            return -1
        elif edge["depth"] > 0:
            return 1
        assert edge is not None
        edgeDirection = edge['direction']

        def difference_weight(e1Direction, e2Direction):
            return forceWeight * int(e1Direction != e2Direction)

        def pressureByEdges(e1Direction, e2Direction):
            return difference_weight(e1Direction, e2Direction) * (
                    -1 * int(e1Direction == 'ftb') + int(
                e1Direction == "btf"))  # -1 (if ftb/ purl) + 1 (if btf/ knit). So always 0 plus a value

        def direction(edgeNeighborData):
            return edgeNeighborData[2]["direction"]

        preEdge, nextEdge = self._getWeftWiseEdgeNeighbors(parentId, childId)
        if preEdge is None:
            prePlaneValue = 0
        else:
            prePlaneValue = pressureByEdges(edgeDirection, direction(preEdge))
        if nextEdge is None:
            nextPlaneValue = 0
        else:
            nextPlaneValue = pressureByEdges(edgeDirection, direction(nextEdge))
        planeValue = prePlaneValue + nextPlaneValue
        if planeValue < -1 * forceWeight:
            planeValue = -1 * forceWeight
        elif planeValue > 1 * forceWeight:
            planeValue = 1 * forceWeight
        if store:
            self.graph.adj[parentId][childId]["yarn-wise-deformation"] = planeValue
        return planeValue

    def _weftWisePlaneDeformationOfLoop(self, loopId: str, isParent: bool = True, forceWeight: float = 1.0,
                                        store: bool = True, check: bool = False) -> float:
        """
        :param loopId: the id of the loop being calculated
        :param isParent: true if the loop is parent to another loop
        :param forceWeight: a modifier on weights
        :param store: if true, caches the calculation
        :param check: if true, ignores cached result
        :return: the horizontal deformation of a loop
        """
        if isParent:
            if not check and self.loopSet[loopId].planeDeformationByParents is not None:
                return self.loopSet[loopId].planeDeformationByParents
            children = self.graph.adj[loopId]
            planeValueSum = 0
            for childId in children:
                planeValueSum += self._weftWisePlaneDeformationByEdge(loopId, childId, forceWeight, store, check)
            if planeValueSum < -1 * forceWeight:
                planeValueSum = -1 * forceWeight
            elif planeValueSum > 1 * forceWeight:
                planeValueSum = 1 * forceWeight
            if store:
                self.loopSet[loopId].planeDeformationByParents = planeValueSum
            return planeValueSum
        else:
            if not check and self.loopSet[loopId].planeDeformationByChildren is not None:
                return self.loopSet[loopId].planeDeformationByChildren
            parents = self.graph.pred[loopId]
            planeValueSum = 0
            for parentId in parents:
                planeValueSum += self._weftWisePlaneDeformationByEdge(parentId, loopId, forceWeight, store, check)
            if planeValueSum < -1 * forceWeight:
                planeValueSum = -1 * forceWeight
            elif planeValueSum > 1 * forceWeight:
                planeValueSum = 1 * forceWeight
            if store:
                self.loopSet[loopId].planeDeformationByChildren = planeValueSum
            return planeValueSum

    def horizontalCompressionByRow(self, rowNum: int, forceWeight: float = 1.0):
        """
        :param rowNum: the row compression is calcualted at
        :param forceWeight: modifier
        :return: the total plane deformation of the row with index rowNum and the length of the row at index rowNum
        """
        sumDeformation: int = 0
        for loop in self.rows[rowNum]:
            sumDeformation += abs(self._weftWisePlaneDeformationOfLoop(loop.loopId, forceWeight=forceWeight))
        return sumDeformation, len(self.rows[rowNum])

    def horizontalCompression(self, forceWeight: float = 1.0) -> float:
        """
        :param forceWeight: modifier
        :return: the horizontal compression of the texture
        """
        sumDeformation = 0
        for i in range(0, self.maxRow):
            rowCompression, rowLength = self.horizontalCompressionByRow(i, forceWeight)
            normalizedRowCompression = rowCompression / rowLength
            sumDeformation += normalizedRowCompression
        return sumDeformation / float(self.maxRow)

    def _getLocalCurvature(self, loopId: int, decayRate: float = 2.0, subCurveCutOff: float = .5) -> float:
        """
        :param loopId: the id of the loop being curved
        :param decayRate: the decay rate of curve effects of decendents
        :param subCurveCutOff: a cut off for calculated local curvature
        :return: the curvature of this loop including its decendents
        """
        edgeAdj = self.graph.adj[loopId]
        if len(edgeAdj) == 0:
            return 0.0
        else:  # recurse
            sumPaths = 0
            localCurve = 0
            for childId in edgeAdj:
                if edgeAdj[childId]["direction"] == "btf":
                    localCurve += 1  # add knit curve forward
                elif edgeAdj[childId]["direction"] == "ftb":
                    localCurve -= 1  # subtract purl curve
                subCurvature = self._getLocalCurvature(childId, decayRate) / decayRate
                sumPaths += subCurvature
            average = sumPaths / len(edgeAdj)
            if abs(average) <= subCurveCutOff:
                average = 0
            curvature = average + localCurve
            return curvature

    def _getLoopCurvature(self, loopId: str, store: bool = True, check: bool = False) -> float:
        """
        :param loopId: id of loop being considered
        :param store: if true, cache the result
        :param check: if true, ignore cache
        :return: the curvature of the loop with loopId by considering the directions of the adjacent loops
        """
        if not check and self.loopSet[loopId].curvature is not None:
            return self.loopSet[loopId].curvature
        edgeAdj = self.graph.adj[loopId]
        if len(edgeAdj) == 0:
            curvature = 0
        else:  # recurse
            sumPaths = 0
            localCurve = 0
            for childId in edgeAdj:
                if edgeAdj[childId]["direction"] == "btf":
                    localCurve += 1  # add knit curve forward
                elif edgeAdj[childId]["direction"] == "ftb":
                    localCurve -= 1  # subtract purl curve
                subCurvature = self._getLoopCurvature(childId, store, check)
                sumPaths += subCurvature
            average = sumPaths / len(edgeAdj)
            curvature = average + localCurve
        if store:
            self.loopSet[loopId].curvature = curvature
        return curvature

    # Returns average curvature of a loop in the row with index rowNum
    def _getRowCurvature(self, rowNum: int) -> float:
        """
        :param rowNum: index of row being considered
        :return: average curvature of row
        """
        curveSum = 0
        for loop in self.rows[rowNum]:
            loop_curvature = self._getLoopCurvature(loop.loopId)
            curveSum += loop_curvature
        return curveSum / float(len(self.rows[rowNum]))

    # todo: if code working, delete
    # def getLocalRowCurvature(self, rowNum: int, decayRate=2, subCurveCutOff=.5):
    #     curveSum = 0
    #     for loop in self.rows[rowNum]:
    #         loopCurvature = self._getLocalCurvature(loop.loopId, decayRate, subCurveCutOff)
    #         curveSum += loopCurvature
    #     return curveSum / len(self.rows[rowNum])

    def getBaseCurvature(self) -> float:
        """
        :return: the curvature of the first row
        """
        return self._getRowCurvature(0)

    @staticmethod
    def convertStitchDefOperationToList(stitchDef: Union[StitchDefinition, CableDefinition, dict],
                                        repCondition: RepConditionResult) -> List[StitchDefinition]:
        """
        :param stitchDef: the stitch definition being repeated
        :param repCondition: the repetition condition
        :return: a list of reps number of stitchDef operations
            Example:
            --> stitchDef = Cable, reps = 3
            --> output = [Cable, Cable, Cable]
        """
        operationList = [stitchDef] * repCondition.reps
        return operationList

    @staticmethod
    def convertOperationBlockToList(operationBlock: list, repCondition: RepConditionResult) -> List[
        Union[StitchDefinition, CableDefinition]]:
        """
        Extends each operation in operationBlock by the number of reps specified in the operationBlock
        Extends the entire series of operations by reps
        :param operationBlock: the list of operations to duplicate
        :param repCondition: the repetition of this operation
        :return: A list of operations
            Example:
                --> operationBlock = [(Stitch, 2), (Cable, 3)], reps = 2
                --> output = [Stitch, Stitch, Cable, Cable, Cable, Stitch, Stitch, Cable, Cable, Cable]
        """
        operationList = []
        for operationTuple in operationBlock:
            operation = operationTuple[0]
            operationReps = operationTuple[1]
            if isinstance(operation, StitchDefinition) or isinstance(operation, CableDefinition):
                operationList.extend(KnitGraph.convertStitchDefOperationToList(operation, operationReps))
            else:  # operation is an operationBlock, we want to recursively process it
                operationList.extend(KnitGraph.convertOperationBlockToList(operationReps))
        operationList = operationList * repCondition.reps
        return operationList

    @staticmethod
    def createStitchDefList(operation: Union[StitchDefinition, CableDefinition, dict, list],
                            repCondition: RepConditionResult) -> List[Union[CableDefinition, StitchDefinition]]:
        """
        :param operation: the operation to consider
        :param repCondition: the repetition condition
        :return: the list of operations
        """
        if isinstance(operation, StitchDefinition) or isinstance(operation, CableDefinition) or (
                type(operation) is dict and "cable" in operation):
            return KnitGraph.convertStitchDefOperationToList(operation, repCondition)
        else:  # operation is an operationBlock, so we want to do multiple duplicating operations
            return KnitGraph.convertOperationBlockToList(operation, repCondition)

    def _addYarn(self, yarn: Yarn):
        self.yarns[yarn.yarnId] = yarn
        return yarn.yarnId
