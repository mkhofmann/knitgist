from typing import Dict, List, Optional, Tuple, Union, Any

from KnitGIST.KnitGraph_Construction.Loop import Loop

class Iterator:

    # Reverses the order of the rows in the rows dictionary passed in.
    # (i.e. if rows has a height of H, row 0 is swapped with row H-1, row 1 is swapped with row H-2, etc.)
    # Assumes the contents of the individual rows have already been reversed.
    def reverse_2D_arr_dict_row_order(self, rows, fileHeight):
        arr_dict: Dict[int, List[Loop]] = {}
        forwardIndex = 0
        for rowIndex in range(fileHeight - 1, -1, -1):
            arr_dict[forwardIndex] = rows[rowIndex].copy()
            forwardIndex += 1
        return arr_dict

    # Reverses the order of the contents of every odd indexed row in the rows dictionary passed in.
    # (i.e. reverses contents of row at index 1, index 3, etc.)
    def reverse_2D_arr_dict_individual_rows(self, rows):
        arr_dict: Dict[int, List[Loop]] = {}
        for rowIndex in rows:
            if rowIndex % 2 == 0:
                arr_dict[rowIndex] = rows[rowIndex].copy()
            else:
                arr_dict[rowIndex] = self.reverse_list(rows[rowIndex]).copy()
        return arr_dict

    # Reverses a list.
    def reverse_list(self, list):
        reversed_list = []
        for i in range(len(list) - 1, -1, -1):
            reversed_list.append(list[i])
        return reversed_list