import random

from KnitGIST2.KnitGraph_Construction.ks_compiler.KSCompiler import deepCopyOperations
from KnitGIST2.synthesis.Row_synthesis import generateStitchBlock, unWindBlock


def generateTiles(popSize, width, height, stitchGenerator, rowRepChance=.5, stitchRepChance=.5):
    tiles = []
    for i in range(0, popSize):
        tiles.append(generateTile(width, height, stitchGenerator, rowRepChance, stitchRepChance))
    return tiles


def generateTile(width, height, stitchGenerator, rowRepChance=.5, stitchRepChance=.5):
    filledIndices = []
    rowDefs = {}
    for i in range(1, height + 1):
        makeRep = len(rowDefs) > 0 and random.random() < rowRepChance
        if makeRep:
            rowToRep = filledIndices[random.randint(0, len(filledIndices) - 1)]
            rowOperationsToRep = deepCopyOperations(rowDefs[rowToRep])
            rowDefs[i] = rowOperationsToRep
        else:
            rowDefs[i], width = generateStitchBlock(stitchGenerator, width, stitchRepChance)
        filledIndices.append(i)
    return rowDefs


def unWindTile(tile):
    newTile = {}
    for index in tile:
        block = tile[index]
        newBlock = unWindBlock(block, newTile, index)
        newTile[index] = newBlock
    return newTile
