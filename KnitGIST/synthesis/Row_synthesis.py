import random

from KnitGIST2.synthesis.PatternHelperFunctions import getRowWidth, instructionWidth
from KnitGIST2.KnitGraph_Construction.ks_compiler.StitchDefinition import knitDefinition, purlDefinition, \
    StitchDefinition


def factorInteger(x):
    factorPairs = []
    for i in range(2, x):
        if x % i == 0:
            factorPairs.append((i, int(x / i)))
    return factorPairs


def chooseNonPrimeNumber(minValue, maxValue):
    choice = random.randint(minValue, maxValue)
    firstChoice = choice
    while choice >= minValue:
        choiceFactors = factorInteger(choice)
        if len(choiceFactors) > 0:
            return choice, choiceFactors
        choice -= 1
    choice = firstChoice + 1
    while choice <= maxValue:
        choiceFactors = factorInteger(choice)
        if len(choiceFactors) > 0:
            return choice, choiceFactors
        choice += 1
    return None, []


def generateStitchBlock(stitchGenerator, blocksize, repChance=.5):
    block = []
    synthBlockSize = 0
    remainingBlockSize = lambda: blocksize - synthBlockSize
    outLoopSize = 0

    def addStitch():
        stitchData = stitchGenerator()
        stitch = stitchData["definition"]
        stitchDef = stitch[0]
        stitchReps = stitch[1]
        inLoops = stitchData["inLoops"]
        outLoops = stitchData["outLoops"]
        if len(block) >= 1:
            lastStitch = block[-1][0]
            lastReps = block[-1][1]
            if isinstance(lastStitch, StitchDefinition) and lastStitch.name == stitchDef.name:
                block[-1] = lastStitch, lastReps + stitchReps
            else:
                block.append(stitch)
        else:
            block.append(stitch)
        return inLoops, outLoops

    while synthBlockSize < blocksize:
        remainder = remainingBlockSize()
        makeRep = synthBlockSize > 0 and remainder > 2 and random.random() < repChance
        if makeRep:
            repeatedBlockSize, repFactors = chooseNonPrimeNumber(2, remainder)
            if repeatedBlockSize is None:
                inloops, outloops = addStitch()
                synthBlockSize += inloops
                outLoopSize += outloops
            else:
                factorIndex = random.randint(0, len(repFactors) - 1)
                firstFactorIsBlockSize = random.random() < .5  # coin flip
                subBlockSize = repFactors[factorIndex][int(firstFactorIsBlockSize)]
                reps = repFactors[factorIndex][int(not firstFactorIsBlockSize)]
                subBlock, subBlockOutLoops = generateStitchBlock(stitchGenerator, subBlockSize, repChance)
                if len(subBlock) == 1:  # repetition of one stitch type
                    if subBlock[0][1] != subBlockSize:
                        assert subBlock[0][1] == subBlockSize
                    subBlock = subBlock[0]
                    subBlockReps = subBlock[1] * reps
                    block.append((subBlock[0], subBlockReps))
                else:
                    block.append((subBlock, reps))
                synthBlockSize += subBlockSize * reps
                outLoopSize += subBlockOutLoops * reps
        else:
            inloops, outloops = addStitch()
            synthBlockSize += inloops
            outLoopSize += outloops
    return block, outLoopSize


def generateRandomKorP(**kwargs):
    if "knitOdds" in kwargs:
        knitOdds = kwargs["knitOdds"]
    else:
        knitOdds = .5
    flip = random.random()
    if flip < knitOdds:
        return {"definition": (knitDefinition(), 1), "inLoops": 1, "outLoops": 1}
    else:
        return {"definition": (purlDefinition(), 1), "inLoops": 1, "outLoops": 1}


def unWindBlock(block, tile=None, index=None):
    newBlock = []
    for operationTuple in block:
        if not type(operationTuple) is tuple:
            operationTuple = (operationTuple, 1)
        operation = operationTuple[0]
        operationReps = operationTuple[1]
        if type(operationReps) == tuple:  # conditional Ending
            lastRowWidth = getRowWidth(tile, index - 1, False)
            curWidth = instructionWidth(newBlock, True)
            remainingWidth = lastRowWidth - curWidth - operationReps[1]
            if isinstance(operation, StitchDefinition):
                repWidth = operation.inLoopsCount
            else:
                repWidth = instructionWidth(operation, True)
            operationReps = int(remainingWidth / repWidth)
            assert remainingWidth % repWidth == 0, "This operation will not finish with the correct number of loops"
        if isinstance(operation, StitchDefinition):
            newBlock.extend([(operation, 1)] * operationReps)
        else:
            newBlock.extend(unWindBlock(operation, tile, index) * operationReps)
    return newBlock
