from KnitGIST2.KnitGraph_Construction.ks_compiler.CableDefinition import CableDefinition
from KnitGIST2.KnitGraph_Construction.ks_compiler.StitchDefinition import knitDefinition, purlDefinition


def getRowWidth(kspattern, rowIndex, botWidth=True):
    instructions = kspattern[rowIndex]
    rowWidth = instructionWidth(instructions, botWidth)
    return rowWidth


def addCOKnits(kspattern):
    firstRowWidth = getRowWidth(kspattern, 1)
    newPattern = {}
    newPattern[1] = [(knitDefinition(), firstRowWidth)]
    newPattern[2] = [(purlDefinition(), firstRowWidth)]
    for r in kspattern:
        newPattern[r + 2] = kspattern[r]
    return newPattern


def instructionWidth(instructions, botWidth=True):
    rowWidth = 0
    for defTuple in instructions:
        if not type(defTuple) is tuple:
            defTuple = (defTuple, 1)
        if type(defTuple[0]) is list:
            rowWidth += instructionWidth(defTuple[0], botWidth) * defTuple[1]
        if isinstance(defTuple[0], CableDefinition):
            rowWidth += defTuple[0].size
        else:
            if botWidth:
                stitchWidth = defTuple[0].inLoopsCount
            else:
                stitchWidth = 1
            rowWidth += stitchWidth * defTuple[1]
    return rowWidth


def flipDefSide(repDefs):
    wsRepDefs = []
    for instruction in repDefs:
        read = instruction[0].copyOppositeSideRead()
        wsRepDefs.append((read, instruction[1]))
    return wsRepDefs
