from typing import List, Dict, Optional

from KnitGIST.ColorWork.LayerPosition import Layer_Coloring, Layer_Position
from KnitGIST.KnitGraph_Construction.KnitGraph import KnitGraph
from KnitGIST.KnitGraph_Construction.Loop import Loop
from KnitGIST.KnitGraph_Construction.Yarn import Yarn


class LayeredLoopSet:
    def __init__(self, row_index: int, in_row_index: int, loops: List[Loop]):
        self.in_row_index = in_row_index
        self.row_index = row_index
        self.loops: List[Loop] = loops

    def otherLoops(self, loop: Loop):
        assert loop in self.loops, f"Loop {loop} is not in {self.loops}"
        others = []
        for other in self.loops:
            if other != loop:
                others.append(other)
        return others


class LayeredKnit(KnitGraph):
    def __init__(self, textures: List[KnitGraph], layering: Optional[List[Layer_Coloring]] = None):
        super().__init__()
        # create knitgraph
        assert 2 <= len(textures) <= 3, "Layers must be either 2 or 3"
        self.textures: List[KnitGraph] = textures
        self.indexedYarnIds: List[int] = [-1 for _ in
                                          range(0, len(self.textures))]  # yarnIDs in order of texture indices
        self.yarnsToTextures: Dict[Yarn, KnitGraph] = {}
        assert self.textures[0].maxRow == self.textures[1].maxRow, "Layer Row Counts Must match"
        if self.threeLayers():
            assert self.textures[1].maxRow == self.textures[2].maxRow, "Layer Row Counts Must match"

        for row_0, row_1 in zip(self.textures[0].rows.values(), self.textures[1].rows.values()):
            assert len(row_0) == len(row_1), "Row Widths do not match"
        if self.threeLayers():
            for row_1, row_2 in zip(self.textures[1].rows.values(), self.textures[2].rows.values()):
                assert len(row_1) == len(row_2), "Row Widths do not match"

        self.layerLoopsToGraphLoops: Dict[int, Dict[Loop, Loop]] = {i: {} for i in range(0, len(self.textures))}
        self.loopsToLoopLayer: Dict[Loop, LayeredLoopSet] = {}
        self._generateGraph()
        self.maxRow = max(self.rows.keys())

        # manage layering data
        if layering is None:
            layering = [Layer_Coloring(self.layerWidth, self.height, defaultLayer=pos)
                        for pos in [Layer_Position.Top, Layer_Position.Mid, Layer_Position.Bot]]  # make default layers
            if len(self.textures) == 2:
                layering = [layering[0], layering[2]]  # remove mid layer

        if len(self.textures) == 3 == len(layering) + 1:  # need to add in the implied third layer
            impliedLayer = layering[0].impliedLayer(layering[1])
            layering.append(impliedLayer)
        assert len(self.textures) == len(layering), "Layering information must "
        self.layering: List[Layer_Coloring] = layering

    def getLoopFromBaseTexture(self, texture_index: int, texture_loop: Loop) -> Loop:
        return self.layerLoopsToGraphLoops[texture_index][texture_loop]

    @property
    def layerWidth(self) -> int:
        """
        :return: the perceived width of the object, once all layers have collapse on each other
        """
        return self.textures[0].bottomWidth()

    @property
    def needleWidth(self) -> int:
        """
        :return: the number of needles wide needed to knit this object
        """
        return self.bottomWidth()

    @property
    def height(self) -> int:
        return self.maxRow

    @property
    def layer_count(self) -> int:
        return len(self.layering)

    def _cast_on(self):
        pRow = []
        cRow = []
        row_index = 0
        castOnParents: Dict[int, List[Loop]] = {i: layer.rows[-1] for i, layer in enumerate(self.textures)}
        castOnChildren: Dict[int, List[Loop]] = {i: layer.rows[0] for i, layer in enumerate(self.textures)}
        for i in range(0, len(castOnParents[0])):
            for texture_index in range(len(self.textures) - 1, -1, -1):
                texture_parents = castOnParents[texture_index]
                texture_children = castOnChildren[texture_index]
                yarnId = self.changeColor(texture_index)  # set yarn color
                self.indexedYarnIds[texture_index] = yarnId
                yarn = self.yarns[yarnId]
                self.yarnsToTextures[yarn] = self.textures[texture_index]
                texture_parent = texture_parents[i]  # get equivalent loop
                parent = self._addLoop(-1, row_index)  # make loop
                self.layerLoopsToGraphLoops[texture_index][texture_parent] = parent  # mark loop relationships
                pRow.append(parent)
                layer_child = texture_children[i]
                child = self._addLoop(0, row_index)
                self.layerLoopsToGraphLoops[texture_index][layer_child] = child
                cRow.append(child)
                row_index += 1
        self.rows[-1] = pRow
        self.rows[0] = cRow

    def _generateGraph(self):
        self._cast_on()

        for row_index in range(1, self.textures[0].maxRow + 1):
            newRow: List[Loop] = []
            texturesInTripletOrder = self.textures
            # if row_index % 2 == 0:
            #     texturesInTripletOrder = reversed(texturesInTripletOrder)
            textures_rows = [texture.rows[row_index] for texture in texturesInTripletOrder]
            in_row_index = 0
            for base_inRow_index in range(0, len(textures_rows[0])):
                layerLoops = []
                for layer_index, layer_row in enumerate(textures_rows):
                    if row_index % 2 == 0:  # start with last yarn on ws rows
                        layer_index = 2 - layer_index
                    # make the loop for this color
                    self.changeColor(layer_index)
                    loop = self._addLoop(row_index, in_row_index)
                    layerLoops.append(loop)
                    layer_loop = layer_row[base_inRow_index]
                    self.layerLoopsToGraphLoops[layer_index][layer_loop] = loop
                    newRow.append(loop)
                    in_row_index += 1

                    # connect loop to parent loops to form stitches
                    layer = self.textures[layer_index]
                    inEdgeList = layer.getInEdgesByParents(layer_loop.loopId)
                    for parentId, parentData in inEdgeList.items():
                        layer_parent = layer.getLoop(parentId)
                        cur_parent = self.layerLoopsToGraphLoops[layer_index][layer_parent]
                        stitchName = parentData["stitchName"]
                        stitchDir = parentData["direction"]
                        stitchDepth = parentData["depth"]
                        stitchParentOrder = parentData["parentOrder"]
                        self.connectLoops(parent=cur_parent, child=loop, stitchName=stitchName, direction=stitchDir,
                                          depth=stitchDepth, parentOrder=stitchParentOrder)
                layerSet = LayeredLoopSet(row_index=row_index, in_row_index=base_inRow_index, loops=layerLoops)
                for loop in layerLoops:
                    self.loopsToLoopLayer[loop] = layerSet

            self.rows[row_index] = newRow

    def threeLayers(self) -> bool:
        return len(self.textures) == 3
