from enum import Enum
from typing import List, Tuple, Optional


class Layer_Position(Enum):
    Top = 0
    Mid = 1
    Bot = 2

    def compatible(self, otherLayer_1, otherLayer_2):
        return self != otherLayer_1 and self != otherLayer_2 and otherLayer_1 != otherLayer_2

    @staticmethod
    def getPosition(val: int):
        if val == 0:
            return Layer_Position.Top
        elif val == 1:
            return Layer_Position.Mid
        elif val == 2:
            return Layer_Position.Bot
        else:
            assert False, f"Cannot support {val} layers"

    def impliedPos(self, other):
        if self == Layer_Position.Top:
            if other == Layer_Position.Mid:
                return Layer_Position.Bot
            else:
                return Layer_Position.Mid
        elif self == Layer_Position.Mid:
            if other == Layer_Position.Top:
                return Layer_Position.Bot
            else:
                return Layer_Position.Top
        else:  # self == Layer_Position.Bot
            if other == Layer_Position.Top:
                return Layer_Position.Mid
            else:
                return Layer_Position.Top

    def __lt__(self, other):
        assert isinstance(other, Layer_Position)
        return self.value < other.value

    def __str__(self):
        return str(self.value)


class Layer_Coloring:
    def __init__(self, actualWidth: int, height: int, filename: Optional[str] = None,
                 defaultLayer: Layer_Position = Layer_Position.Top):
        self.height = height
        self.width = actualWidth
        self.layer: List[List[Layer_Position]] = [[defaultLayer for _ in range(0, self.width)]
                                                  for __ in range(0, self.height)]
        if filename is not None:
            self.readFromFile(filename)

    def _checkRowAndStitch(self, key: Tuple[int, int]):
        row = key[0]  # rows are between 0 and self.height
        assert 0 <= row <= self.height, f"row {row} out of bounds"
        stitch = key[1]  # 0 indicates leftmost stitch
        assert 0 <= stitch < self.width, f"stitch {stitch} out of bounds"
        return row, stitch

    def __setitem__(self, key: Tuple[int, int], value: Layer_Position):
        row, stitch = self._checkRowAndStitch(key)
        self.layer[row][stitch] = value

    def __getitem__(self, item: Tuple[int, int]) -> Layer_Position:
        row, stitch = self._checkRowAndStitch(item)
        return self.layer[row][stitch]

    def readFromFile(self, filename: str):
        with open(filename, 'r') as layer_data:
            cur_row = self.height - 1
            for line in layer_data:
                values = line.split()
                assert len(values) == self.width, \
                    f"Number of values ({len(values)} does not match width of {self.width})"
                cur_stitch = self.width - 1
                for value in values:
                    num_val = int(value)
                    self[(cur_row, cur_stitch)] = Layer_Position.getPosition(num_val)
                    cur_stitch -= 1
                cur_row -= 1

    def checkCompatibility(self, otherLayer_0, otherLayer_1):
        for row, otherRow0, otherRow1 in zip(self.layer, otherLayer_0.layer, otherLayer_1.layer):
            for pos, otherPos0, otherPos1 in zip(row, otherRow0, otherRow1):
                if not pos.compatible(otherPos0, otherPos1):
                    print(f"Incompatible positions [{pos}, {otherPos0}, {otherPos1}]")
                    return False
        return True

    def impliedLayer(self, other):
        newLayer = Layer_Coloring(self.width, self.height)
        row_index = 0
        for row, otherRow in zip(self.layer, other.layer):
            stitch_index = 0
            for pos, otherPos in zip(row, otherRow):
                newLayer[(row_index, stitch_index)] = pos.impliedPos(otherPos)
                stitch_index += 1
            row_index += 1
        return newLayer

    def __eq__(self, other):
        if not isinstance(other, Layer_Coloring):
            return False
        for row, otherRow in zip(self.layer, other.layer):
            for pos, otherPos in zip(row, otherRow):
                if pos != otherPos:
                    return False
        return True


def allOneColorLayer(pos: Layer_Position, actualWidth: int, height: int, filename: str) -> Layer_Coloring:
    with open(filename, "w") as layer:
        lines = []
        for _ in range(0, height):
            line = ""
            for _ in range(0, actualWidth):
                line += f"{pos} "
            line += "\n"
            lines.append(line)
        layer.writelines(lines)

    return Layer_Coloring(actualWidth, height, filename, pos)
